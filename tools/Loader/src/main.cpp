//============================================================================
// Name        : Loader.cpp
// Author      : Luigi Venuso
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdio.h>
#include "UartComm.h"
#include "Loader.h"


using namespace std;

static void usage(void)
{
	cout <<"Usage: Loader [file] [option]		load memory and boot core"<<endl;
	cout <<"Options:"<<endl;
	cout <<"	-p <port name of serial device (example: /dev/ttyUSB1)>."<<endl;
	cout <<"   	-b <baud_rate> (tha same you synthesized)."<<endl;
	cout <<"	-t <number of activated thread> (default 1)."<<endl;
	cout <<"	-n <number of bytes to read> (4 | 8 | 16 | 32 | 64)\n."<<endl;
}

int main(int argc, char **argv)
{


	string port = "/dev/ttyUSB1";
	string file_name = argv[1];
	int byte_num = 64;
	int baud_rate = 9600;//460800;
	int thread_number = 1;
	int thread_activation_mask = 0;
	int option = 0;
	string line;


	cout << "Bootloader nuplus" << endl;
	if (argc <= 1)
	{
		cerr << "Specify hex file name!\n";
		return 1;
	} else if ((strcmp(argv[1], "help") == 0))
	{
		usage();
		return 0;
	} else if (file_name.substr(file_name.find_last_of(".") + 1) != "hex")
	{
		cerr << "Specify hex file name!\n";
		return 1;
	}

	while ((option = getopt(argc, argv, "p:b:t:n:")) != -1)
	{
		switch (option)
		{
		case 'p':
			port = optarg;
			break;
		case 'b':
			baud_rate = atoi(optarg);
			break;
		case 't':
			thread_number = atoi(optarg);
			break;
		case 'n':
			byte_num = atoi(optarg);
			break;
		default:
			break;
		}
	};

	Loader *loader = new Loader(file_name, port, baud_rate);
	loader->connect();
	string bufRead[loader->getlineNumb()];

	loader->openFile(bufRead);

	//Load Memory
	loader->loadMemory(0x00,(uint8_t)0x40,bufRead);


/*****************************************************************************************
 * 								DUMP MEMORY WRITED										 *
 ****************************************************************************************/

//   for (int t = 0; t < loader->getlineNumb(); t++)
//		{
//			loader->readMemoryLine(0x00000000 + t * 64, data_read);
//			sleep(0.5);
//			for (int k = 64 - byte_num; k < 64; k++)
//			{
//				printf("%02x ", *(data_read + k));//fprintf(f,"%02x ", *(data_read + k));
//			}
//			printf("\n");
//		}



	//Boot Core
	sleep(1);
	for (int t = 0; t < thread_number; t++)
	{
		loader->bootThread(0, (uint8_t) t);
		thread_activation_mask |= loader->one_hot_encoder(t);
	}


	sleep(1);
	loader->enableThread(thread_activation_mask);

	cout << "End of Program" << endl;

	//Read Memory
	uint8_t data_read[byte_num];
	for (int t = 0; t < thread_number; t++)
	{
		loader->readMemoryLine(0x01000000 + t * 64, data_read);
		cout << "Result: ";
		for (int k = 64 - byte_num; k < 64; k++)
		{
			printf("%02x ", *(data_read + k));
		}
	}
	cout << endl;

	for (int i = 0; i<byte_num; i++){
		data_read[i] = 0;
	}

	for (int t = 0; t < thread_number; t++)
	{
		loader->readMemoryLine(0x00001804 + t * 64, data_read);
		cout << "Result: ";
		for (int k = 64 - byte_num; k < 64; k++)
		{
			printf("%02x ", *(data_read + k));
		}
	}
	cout << endl;

	loader->close();
	return 0;
}



