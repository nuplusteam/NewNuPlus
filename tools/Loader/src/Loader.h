/*
 * Loader.h
 *
 *  Created on: 05 lug 2017
 *      Author: luigi
 */

#ifndef LOADER_H_
#define LOADER_H_

#include "UartComm.h"
#include "defines.h"
#include "vector"
#include <fstream>
#include <sstream>

namespace std
{

class Loader
{
public:
	//Loader();
	Loader(string inputFilePath, string port, int baud);
	virtual ~Loader();
	int getlineNumb();


	void connect();
	void close();
	void flushuart();
	void openFile(string *buffRead);
	bool sendAddress(unsigned int address);
	bool loadMemoryLine(unsigned int address, string line);
	void loadMemory(unsigned int address, uint8_t increment, string *bufRead);
	bool readMemoryLine(unsigned int address,  uint8_t *linemem);
	int swapAddress(int address);
	bool sendCommandAndCheck(uint8_t comm, uint8_t resp);
	bool sendInt(int data, uint8_t *serialResp);
	bool sendInitializeCommand(uint8_t comm, uint8_t resp);
	bool sendString(string data);

	//Common
	unsigned char HexChar (char c);
	int HexToBin (const char* s, unsigned char * buff, int length);
	void hexStringToByteArray(string s, uint8_t *data);
	void print_progress_bar(unsigned int current, unsigned int total);

	bool sendPc(unsigned int pc);
	bool bootThread(int pc, uint8_t thread_id);
	bool enableThread(uint8_t thread_mask);
	int getThreadnum();

	uint8_t one_hot_encoder(int thread_id);
private:
	UartComm *serialComm;
	string fileName;
	string serialPort;
	int baudRate;
	int lineNumb;


};

} /* namespace std */

#endif /* LOADER_H_ */
