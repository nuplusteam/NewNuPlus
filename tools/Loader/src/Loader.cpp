/*
 * Loader.cpp
 *
 *  Created on: 05 lug 2017
 *      Author: luigi
 */

#include "Loader.h"

namespace std
{

Loader::Loader(string inputFilePath, string port, int baud)
{

	serialComm = new UartComm(port, baud);
	serialPort = port;
	baudRate = baud;
	fileName = inputFilePath;
	ifstream inputFile(fileName);
	string line;
	if(inputFilePath.substr(inputFilePath.find_last_of(".") + 1) == "hex"){
		while (getline(inputFile, line)){
			lineNumb++;
		}
	}
	inputFile.close();
}

int Loader::getlineNumb(){
	return lineNumb;
}
Loader::~Loader()
{
	baudRate = 0;

	delete serialComm;

}

} /* namespace std */

void Loader::connect(){
	if (serialPort != "" && baudRate != 0) {
				serialComm->connection();
				cout<<"Serial port "<< serialPort<< " connected!"<<endl;
			} else
				cerr<<"Connection Failed!"<<endl;
}

void Loader::close() {
		serialComm->closeConnection();

	}
void Loader::flushuart(){
	serialComm->flush();
}

void Loader::openFile(string *bufRead)
{

	ifstream inputFile(fileName);

	unsigned int i = 0;
	if (fileName.substr(fileName.find_last_of(".") + 1) == "hex")
	{
		if (inputFile.is_open())
		{
			string line;
			while(!inputFile.eof()){
				getline(inputFile, line);
				bufRead[i] = line;
				i++;
			}
			cout << "File " << fileName << " opened!" << endl;
		} else
		{
			cerr << "File " << fileName << " can not be opened!" << endl;
		}
		inputFile.close();
	}
}

int Loader::swapAddress(int address) {
		int address_swp;
		int a = (address >> 24) & 0xFF;
		int b = (address >> 16) & 0xFF;
		int c = (address >> 8) & 0xFF;
		int d = (address >> 0) & 0xFF;

		address_swp = (a << 0) | (b << 8) | (c << 16) | (d << 24);
		return address_swp;
	}

bool Loader::sendCommandAndCheck(uint8_t comm, uint8_t resp) {
		bool res = false;
		uint8_t serialResp = 0;

		serialComm->sendByte(&comm);
		serialComm->readByte(1500, &serialResp);

		if (serialResp == resp)
			res = true;

		return res;
	}

 bool Loader::sendInt(int data, uint8_t *serialResp) {
		bool res = false;
		uint8_t byte[4];

		for (int i = 0; i < 4; i++) {
			byte[i] = (uint8_t)(data >> i*8);
		}

		cout<<endl;

		for (int i = 0; i < 4; i++) {
			serialComm->sendByte(&byte[i]);
			serialComm->readByte(1500,serialResp);
		}

		return res;
	}


bool Loader::sendAddress(unsigned int address)
{
	bool resp = false;
	uint8_t serialResp;

	resp = sendCommandAndCheck(SEND_ADDRESS_COMMAND, SEND_ADDRESS_COMMAND);
	if(!resp){
		cout<<"SA:Address command Error!"<<endl;
	}else {
		resp = true;
	}
	sendInt(address, &serialResp);

	return resp;

}

bool Loader::sendInitializeCommand(uint8_t comm, uint8_t resp) {
		bool res = false;
		uint8_t serialResp = 0;
		uint8_t tmp = SEND_COMMAND_CORE;

		serialComm->sendByte(&tmp);
		serialComm->readByte(1500,&serialResp);

		if (serialResp == SEND_COMMAND_CORE)
			res = true;

		serialComm->sendByte(&comm);
		serialComm->readByte(1500,&serialResp);

		if (serialResp == resp)
			res = true;
		return res;
	}

bool Loader::loadMemoryLine(unsigned int address, string line)
{
	bool correct = false;
	bool result = false;

	correct = sendInitializeCommand(WRITE_MEMORY_COMMAND, COMMAND_RECEIVED);

	if (correct){
		result = true;
	}else
		cout<<"\nLM: Command not received!"<<endl;

	//Send Address
	sendAddress(address);

	correct = sendCommandAndCheck(WRITE_MEMORY_COMMAND, WRITE_MEMORY_ACK);

	if (correct){
		result = true;
	}else
		cout<<"\nLM: Memory not available!"<<endl;

	sendString(line);
	return result;
}

void Loader::loadMemory(unsigned int address, uint8_t increment, string *bufRead){

		int i = 0;
		while(i<lineNumb){
			print_progress_bar(i,lineNumb-1);
			loadMemoryLine(address, bufRead[i]);
			address += increment;
			i++;
		}

	}

void Loader::hexStringToByteArray(string s, uint8_t *data) {
		int len = s.length();
		uint8_t i_str;
		stringstream sstr;

		for (int i = 0; i < len; i += 2){
			sstr.str("");
			sstr << s[i] << s[i + 1];

			i_str = (uint8_t) strtoul(sstr.str().c_str(), nullptr, 16);
			data[i / 2] = i_str;
		}

}


bool Loader::sendString(string data) {
		bool result = false;
		uint8_t serialResp = 0;
		uint8_t bytes[data.length()/2];

		hexStringToByteArray(data, bytes);

		for (unsigned int i = 0; i < data.length()/2; i++) {
			serialComm->sendByte(&bytes[i]);
			serialComm->readByte(1500, &serialResp);
		}
		if (serialResp == DATA_RECEIVED_ACK){
			result = true;
		}else
			cout<<"\nLM_SS: Memory not available!"<<endl;

		return result;
	}

bool Loader::readMemoryLine(unsigned int address, uint8_t *linemem) {
		bool correct = false;
		bool result = true;
		uint8_t serialResp;

		correct = sendInitializeCommand(READ_MEMORY_COMMAND, COMMAND_RECEIVED);
		if (!correct)
			result = false;

		sendAddress(address);

		correct = sendCommandAndCheck(READ_MEMORY_COMMAND, READING_MEMORY);

		if (!correct)
			result = false;


		for (int i = 0; i < 64; i++) {
			serialComm->readByte(1500, linemem+i);
		}

		serialComm->readByte(1500, &serialResp);
		if(!(serialResp == DATA_RECEIVED))
			cout<<"RM: Data not available!"<<endl;

		return result;
}



bool Loader::sendPc(unsigned int pc){
	uint8_t serialResp = 0;
	bool result = false;
	result = sendInt(pc, &serialResp);
	return result;
}

bool Loader::bootThread(int pc, uint8_t thread_id){
	bool correct = false;
	bool result = true;
	uint8_t serialResp = 0;

	correct = sendInitializeCommand(BOOT_COMMAND, COMMAND_RECEIVED);
	if (!correct)
		result = false;

	correct = sendCommandAndCheck(BOOT_COMMAND, BOOT_COMMAND);
	if (!correct)
		result = false;

	sendPc(pc);

	serialComm->sendByte(&thread_id);
	serialComm->readByte(1500, &serialResp);

	if(serialResp == BOOT_ACK){
		cout<<"BC: Core booted"<<endl;
	}else{
		cout<<"BC: Core not booted"<<endl;
	}

	return result;
}

bool Loader::enableThread(uint8_t thread_mask){
	bool correct = false;
	bool result = true;
	uint8_t serialResp = 0;

	correct = sendInitializeCommand(ENABLE_THREAD_COMMAND, COMMAND_RECEIVED);
	if (!correct)
		result = false;

	correct = sendCommandAndCheck(ENABLE_THREAD_COMMAND, ENABLE_THREAD_COMMAND);
	if (!correct)
		result = false;

	serialComm->sendByte(&thread_mask);
	while(serialResp != THREAD_EN){
		cout<<"Waiting job execution.."<<endl;
		serialComm->readByte(1500,&serialResp);
	}

	cout<<"Job executed"<<endl;
	return result;
}

uint8_t Loader::one_hot_encoder(int thread_id){
	uint8_t mask = 0;
	mask = (uint8_t) (mask^(1 << thread_id));
	return mask;
}

void Loader::print_progress_bar(unsigned int current, unsigned int total)
{
    unsigned int num_ticks = current * PROGRESS_BAR_WIDTH / total;
    unsigned int i;

    printf("\r_loading %d bytes [",64*lineNumb);
    for (i = 0; i < num_ticks; i++)
        printf("=");

    for (; i < PROGRESS_BAR_WIDTH; i++)
        printf(" ");

    printf("] (%u%%)", current * 100 / total);
    std::system("clear");
}
