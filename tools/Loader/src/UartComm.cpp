/*
 * UartComm.cpp
 *
 *  Created on: 04 lug 2017
 *      Author: luigi
 */

#include "UartComm.h"

namespace std
{

UartComm::UartComm()
{
	portName = nullptr;
	baudRate = 0;
	serial_fd = 0;

}

UartComm::UartComm(string port, int baud)
{
	portName = port;
	baudRate = baud;
	serial_fd = 0;
}

UartComm::~UartComm()
{
	// TODO Auto-generated destructor stub
}

int UartComm::getSerialFd(){
	return serial_fd;
}

int UartComm::connection()
{
	struct termios serialopts;

	serial_fd = open(portName.c_str(), O_RDWR | O_NOCTTY);

	if (serial_fd < 0)
	{
		perror("couldn't open serial port");
		return -1;
	}

	memset(&serialopts, 0, sizeof(serialopts));
	serialopts.c_cflag = CS8 | CLOCAL | CREAD;
	cfsetspeed(&serialopts, baudRate);

	if (tcsetattr(serial_fd, TCSANOW, &serialopts) != 0)
	{
		perror("Unable to initialize serial port");
		return -1;
	}

	tcflush(serial_fd, TCIOFLUSH);

	return serial_fd;
}

void UartComm::closeConnection(){
	close(serial_fd);
}

void UartComm::flush(){
	tcflush(serial_fd, TCIOFLUSH);
}



int UartComm::sendByte(uint8_t *b){

	if (write(serial_fd, b, 1) != 1)
	    {
	        perror("write");
	        return 0;
	    }

	//printf("%02x\n", *b);
	    return 1;
}

int UartComm::writeSerialLong(unsigned int value){

	uint8_t *temp = (uint8_t *)&value;
	for (unsigned i = 0; i < 4; i++)
	{
		if (!sendByte(temp + i))
		{
			perror("write");
			return 0;
		}
	}


	    return 1;
}

int UartComm::readByte(int timeout_ms, uint8_t *result){

		fd_set set;
	    struct timeval tv;
	    int ready_fds;

	    FD_ZERO(&set);
	    FD_SET(serial_fd, &set);

	    tv.tv_sec = timeout_ms / 1000;
	    tv.tv_usec = (timeout_ms % 1000) * 1000;

	    do
	    {
	        ready_fds = select(FD_SETSIZE, &set, NULL, NULL, &tv);
	    }
	    while (ready_fds < 0 && errno == EINTR);

	    if (ready_fds == 0)
	        return 0;

	    if (read(serial_fd, result, 1) != 1)
	    {
	        perror("read");
	        return 0;
	    }


	    return 1;
}


int UartComm::readSerialLong(int timeout_ms, uint8_t *out){
	    int i;

	    for (i = 0; i < 4; i++)
	    {
//	        if (!readByte(timeout_ms, ((uint8_t *)out)+i))
//	            return 0;
	    	  if (!readByte(timeout_ms, (out)+i))
	    		            return 0;

	    }

	    return 1;
}


} /* namespace std */
