/*
 * UartComm.h
 *
 *  Created on: 04 lug 2017
 *      Author: luigi
 */

#ifndef UARTCOMM_H_
#define UARTCOMM_H_

#include"defines.h"

namespace std
{

class UartComm
{
public:
	UartComm();
	UartComm(string port, int baud);
	virtual ~UartComm();

	string getPortName();
	int getBaudRate();
	int getSerialFd();

	int connection();
	int sendByte(uint8_t *b);
	int writeSerialLong(unsigned int value);
	int readByte(int timeout_ms, uint8_t *result);
	int readSerialLong(int timeout_ms, uint8_t *out);
	//int readSerialLong(int timeout_ms, unsigned int *out);
	void closeConnection();
	void flush();

private:
	string portName;
	int baudRate;
	int serial_fd;
};



} /* namespace std */

#endif /* UARTCOMM_H_ */
