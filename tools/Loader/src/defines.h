/*
 * defines.h
 *
 *  Created on: 04 lug 2017
 *      Author: luigi
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#include <iostream>
#include <bitset>
#include <cstring>
#include <termios.h>
#include <sys/fcntl.h>
#include <stdio.h>
#include <unistd.h>

using namespace std;


typedef bitset<8> byte_t;
#define PROGRESS_BAR_WIDTH 		 40

//COMMAND
#define SEND_ADDRESS_COMMAND 	 0x01
#define	SEND_COMMAND_CORE        0x02
#define WRITE_MEMORY_COMMAND 	 0x03
#define READ_MEMORY_COMMAND      0x04
#define BOOT_COMMAND		 	 0x05
#define ENABLE_THREAD_COMMAND	 0x06

//ACK FROM CORE
#define THREAD_EN				 0x22
#define BOOT_ACK				 0x23
//#define MEM_NOT_AVAILABLE		 0x24
//#define END_MEM_LINE			 0x25
#define COMMAND_RECEIVED		 0x26

//ACK FROM UART
#define WRITE_MEMORY_ACK 		 0x31
#define DATA_RECEIVED            0x32
//#define SEND_ADDRESS_ACK		 0x33
//#define CORE_NOT_AVAILABLE       0x34
#define READING_MEMORY			 0x35
//#define DATA_WRITED				 0x36
//#define ADDRESS_RECEIVED 		 0x37
//#define NBYTE_RECEIVED 		     0x38
#define DATA_RECEIVED_ACK		 0x39

//#define CORE_BOOTED				 0x40
//#define CORE_NOT_BOOTED			 0x41


#endif /* DEFINES_H_ */
