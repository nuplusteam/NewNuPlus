import sys,re,struct,ctypes

filePath = sys.argv[1]
fromLine = int(sys.argv[2])
toLine = int(sys.argv[3])
isFloat = sys.argv[4] == "f"

F = open(filePath,"r")

ListFloat = []
ListMem = []
ListInt = []

for i in range(0, toLine):
    line = F.readline()
    tmp = line.rstrip('\n')
    if i >= (fromLine - 1):
        tmp = re.split('\t', tmp)
        parsed_line = tmp[1]
        parsed_line = parsed_line[1:]
        mem_list = []
        float_list = []
        int_list = []

        for j in range(0, len(parsed_line), 8):
            z = (parsed_line[j: j + 8])
            mem_list = mem_list + [z]
        ListMem.append(mem_list)

        for k in range(0, 16):
            float_val = mem_list[k][6:8] + mem_list[k][4:6] + mem_list[k][2:4] + mem_list[k][0:2]
            float_val = struct.unpack('!f', float_val.decode('hex'))[0]
            float_list = float_list + [float_val] 
            int_val = int(mem_list[k][6:8] + mem_list[k][4:6] + mem_list[k][2:4] + mem_list[k][0:2], 16)
            if int_val > 0x7FFFFFFF:
	    		int_val -= 0x100000000
            int_list = int_list + [int_val] 
        ListFloat.append(float_list)
        ListInt.append(int_list)
        
        
print 'Memory Image: '
for j in range(0, len(ListMem)):
    print ListMem[j]

print '\n\nTraslation: '
for j in range(0, len(ListInt)):
    if isFloat == 1:
	print ["%8.4f" % v for v in reversed(ListFloat[j])]
    else:
    	print ["%8d" % v for v in reversed(ListInt[j])]

F.close()
