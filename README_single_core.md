TOOLCHAIN

In order to install the toolchain, follow the instruction in the README file located in the NuPlusLLVM folder.

VIVADO PROJECT

After compiled the toolchain, you can create your Vivado project.

you have to import the ip cores about MIG and Clock Converter using the command:

    import_ip -files /your/vivado/home/ip/*.xci

Before to launch the simulation, you must modify two paths in the "user_define.sv" file: PROJECT_PATH and MEMORY_PATH, that contain the path related to the main project and the .hex simulation memory file, respectively.
In the user_define.sv, you can also modify the output log path.


EMULATOR

In order to use the nu+ emulator (Emuplus), you must move in the folder Emuplus/Release and execute the following commands:

    make
    ./emuplus -help

Each emuplus run produces four file:
    MEM0.txt: initial memory state;
    MEM.txt: final memory state;
    SPM.txt: scratchpad memory final state;
    Thread_1.txt: executed instruction log and the scalar/vector register state for each instruction.

If you want to set a breakpoint, you have to open the under test .hex file and insert an '@' before instruction.

Example:
........
........
00000000
@34002000 	Breakpoint setted.
03002800
........
........
