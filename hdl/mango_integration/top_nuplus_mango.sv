`include "user_define.sv"
`include "nuplus_define.sv"
`include "synchronization_defines.sv"

module top_nuplus_mango #(
		parameter ID               = 0,   // TILE ID (maybe is not needed)
		parameter TLID_w           = 10,  // ID width (maybe is not needed)
		parameter NODE_ID_w        = 10,  // Node ID width (node id width, internal to NUPLUS)
		parameter MEM_ADDR_w       = 32,  // Memory address width (in bits)
		parameter MEM_DATA_BLOCK_w = 512, // Memory data block width (in bits)
		parameter ITEM_w           = 32 )
	(
		input                             clk,
		input                             reset,
		input                             enable,

		// interface MANGO_TILEREG <-> NUPLUS
		input  [ITEM_w - 1 : 0]           item_data_i,      // Input: items from outside
		input                             item_valid_i,     // Input: valid signal associated with item_data_i port
		output                            item_avail_o,     // Output: avail signal to input port item_data_i
		output [ITEM_w - 1 : 0]           item_data_o,      // Output: items to outside
		output                            item_valid_o,     // Output: valid signal associated with item_data_o port
		input                             item_avail_i,     // Input: avail signal to ouput port item_data_o

		// interface MC
		output [7 : 0]                    nu_leds_out,      // XXX: Debug signal!!
		output [3 : 0]                    nu_thread_en,

		output [MEM_ADDR_w - 1 : 0]       mc_address_o,     // output: Address to MC
		output [MEM_DATA_BLOCK_w - 1 : 0] mc_block_o,       // output: Data block to MC
		output                            mc_avail_o,       // output: available bit from UNIT
		output [NODE_ID_w - 1 : 0]        mc_sender_o,      // output: sender to MC
		output                            mc_read_o,        // output: read request to MC
		output                            mc_write_o,       // output: write request to MC
		input  [MEM_ADDR_w - 1 : 0]       mc_address_i,     // input: Address from MC
		input  [MEM_DATA_BLOCK_w - 1 : 0] mc_block_i,       // input: Data block from MC
		input  [NODE_ID_w - 1 : 0]        mc_dst_i,         // input: destination from MC
		input  [NODE_ID_w - 1 : 0]        mc_sender_i,      // input: Sender from MC
		input                             mc_read_avail_i,  // input: read available signal from MC
		input                             mc_write_avail_i, // input: write available signal from MC
		input                             mc_valid_i,       // input: valid bit from MC
		input                             mc_request_i      // input: Read/Write request from MC
	);

    localparam TILE_ID = 0;
    localparam CORE_ID = 0;

	logic                                        hi_job_valid;
	logic                                        hi_job_pc;
	logic                                        hi_job_thread_id;
	logic                 [`THREAD_NUMB - 1 : 0] thread_en;
	logic                                        m2n_request_available;
	logic                                        hi_read_cr_valid;
	scal_reg_size_t                              hi_read_cr_request;
	scal_reg_size_t                              cr_response;

	// Synchronization Signals
	logic                                        job_terminated;
	logic                                        account_available;
	sync_account_t                               ni_account_mess;
	logic                                        ni_account_mess_valid;
	logic                                        ss1_account_consumed;
	sync_release_t                               ss3_release_mess;
	logic                                        ss3_release_valid;
	logic                                        ni_available;
	service_c2n_message_t                        n2c_release_message;
	logic                                        n2c_release_valid;
	logic                                        c2n_account_valid;
	service_c2n_message_t                        c2n_account_message;

	assign mc_sender_o                                          = 0;

	nuplus_tilereg_interface #(
		.ID              ( ID               ),
		.TLID_w          ( TLID_w           ),
		.NODE_ID_w       ( NODE_ID_w        ),
		.MEM_ADDR_w      ( MEM_ADDR_w       ),
		.MEM_DATA_BLOCK_w( MEM_DATA_BLOCK_w ),
		.ITEM_w          ( ITEM_w           )
	)
	u_nuplus_tilereg_interface (
		.clk                ( clk                ),
		.reset              ( reset              ), //Input: positive reset signal
		//To Nuplus
		.hi_thread_en       ( thread_en          ),
		.hi_job_valid       ( hi_job_valid       ),
		.hi_job_pc          ( hi_job_pc          ),
		.hi_job_thread_id   ( hi_job_thread_id   ),
		.hi_read_cr_valid   ( hi_read_cr_valid   ),
		.hi_read_cr_request ( hi_read_cr_request ),
		.cr_response        ( cr_response        ),
		//Debug board
		.leds_out           ( leds_out           ),
		//interface MANGO_TILEREG <-> NUPLUS
		.item_data_i        ( item_data_i        ), //Input: items from outside
		.item_valid_i       ( item_valid_i       ), //Input: valid signal associated with item_data_i port
		.item_avail_o       ( item_avail_o       ), //Output: avail signal to input port item_data_i
		.item_data_o        ( item_data_o        ), //Output: items to outside
		.item_valid_o       ( item_valid_o       ), //Output: valid signal associated with item_data_o port
		.item_avail_i       ( item_avail_i       )  //Input: avail signal to ouput port item_data_o
	);

	assign nu_leds_out                                          = leds_out;
	assign nu_thread_en                                         = thread_en;
	assign m2n_request_available                                = mc_write_avail_i & mc_read_avail_i;

	nuplus_single_core #(
		.TILE_ID( TILE_ID ),
		.CORE_ID( CORE_ID )
	)
	u_nuplus_single_core (
		.clk                      ( clk                   ),
		.reset                    ( reset                 ),
		.enable                   ( enable                ),
		.thread_en                ( thread_en             ),
		//Host Interface
		.hi_job_valid             ( hi_job_valid          ),
		.hi_job_pc                ( hi_job_pc             ),
		.hi_job_thread_id         ( hi_job_thread_id      ),
		.hi_read_cr_valid         ( hi_read_cr_valid      ),
		.hi_read_cr_request       ( hi_read_cr_request    ),
		.cr_response              ( cr_response           ),
		.n2m_request_address      ( mc_address_o          ),
		.n2m_request_data         ( mc_block_o            ),
		.n2m_request_read         ( mc_read_o             ),
		.n2m_request_write        ( mc_write_o            ),
		.mc_avail_o               ( mc_avail_o            ),
		.m2n_request_available    ( m2n_request_available ),
		.m2n_response_valid       ( mc_valid_i            ),
		.m2n_response_address     ( mc_address_i          ),
		.m2n_response_data        ( mc_block_i            ),
		//Synchronization Interface
		.bc2n_account_valid       ( c2n_account_valid     ),
		.bc2n_account_message     ( c2n_account_message   ),
		.n2bc_network_available   ( account_available     ),
		.n2bc_release_message     ( n2c_release_message   ),
		.n2bc_release_valid       ( n2c_release_valid     ),
		.n2bc_mes_service_consumed(                       )
	);

	assign ni_available                                         = 1'b1;
	assign n2c_release_message.data[$bits( barrier_t ) + 1 : 2] = ss3_release_mess.id_barrier;
	assign n2c_release_valid                                    = ss3_release_valid;
	assign ni_account_mess.id_barrier                           = c2n_account_message.data[$bits( barrier_t ) + $clog2( `TILE_COUNT ) + 1 : $clog2( `TILE_COUNT ) + 2];
	assign ni_account_mess.tile_id_source                       = 0;
	assign ni_account_mess.cnt_setup                            = c2n_account_message.data[$bits( cnt_barrier_t ) + $bits( barrier_t ) + $clog2( `TILE_COUNT ) + 1 : $bits( barrier_t ) + $clog2( `TILE_COUNT ) + 2];
	assign ni_account_mess_valid                                = c2n_account_valid;

	synchronization_core u_synchronization_core (
		.clk                  ( clk                   ),
		.reset                ( reset                 ),
		.job_terminated       ( job_terminated        ),
		.account_available    ( account_available     ),
		//Account
		.ni_account_mess      ( ni_account_mess       ),
		.ni_account_mess_valid( ni_account_mess_valid ),
		.ss1_account_consumed ( ss1_account_consumed  ),
		//Release
		.ss3_release_mess     ( ss3_release_mess      ),
		.ss3_release_valid    ( ss3_release_valid     ),
		.ni_available         ( ni_available          )
	);

endmodule