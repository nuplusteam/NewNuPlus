`include "nuplus_define.sv"
`include "nuplus_boot.sv"

module nuplus_tilereg_interface #(
		parameter ID               = 0,   // TILE ID (maybe is not needed)
		parameter TLID_w           = 10,  // ID width (maybe is not needed)
		parameter NODE_ID_w        = 10,  // Node ID width (node id width, internal to NUPLUS)
		parameter MEM_ADDR_w       = 32,  // Memory address width (in bits)
		parameter MEM_DATA_BLOCK_w = 512, // Memory data block width (in bits)
		parameter ITEM_w           = 32   // Input and output item width (control interface
	)(
		input                                         clk,
		input                                         reset,             // Input: positive reset signal

		// To Nuplus
		output                 [`THREAD_NUMB - 1 : 0] hi_thread_en,
		output logic                                  hi_job_valid,
		output address_t                              hi_job_pc,
		output thread_id_t                            hi_job_thread_id,

		// To NuPlus Control Register
		output logic                                  hi_read_cr_valid,
		output scal_reg_size_t                        hi_read_cr_request,

		input  scal_reg_size_t                        cr_response,

		// Debug board
		output logic           [7 : 0]                leds_out,

		// interface MANGO_TILEREG <-> NUPLUS
		input                  [ITEM_w - 1 : 0]       item_data_i,       // Input: items from outside
		input                                         item_valid_i,      // Input: valid signal associated with item_data_i port
		output                                        item_avail_o,      // Output: avail signal to input port item_data_i
		output                 [ITEM_w - 1 : 0]       item_data_o,       // Output: items to outside
		output                                        item_valid_o,      // Output: valid signal associated with item_data_o port
		input                                         item_avail_i       // Input: avail signal to ouput port item_data_o
	);

	interface_state_t                        state, next_state;
	logic                                    avail_out, valid_out, boot_valid;
	address_t                                boot_pc;
	logic             [`THREAD_NUMB - 1 : 0] thread_en;
	logic                                    update_thread_en, update_boot_thread_id;
	thread_id_t                              boot_thread_id;
	host_messages_t                          message_in;
	logic             [31 : 0]               message_out;


	assign item_avail_o     = avail_out;
	assign item_valid_o     = valid_out;
	assign item_data_o      = message_out;
	assign message_in       = host_messages_t'( item_data_i );
	assign hi_thread_en     = thread_en;
	assign hi_job_thread_id = boot_thread_id;
	assign hi_job_valid     = boot_valid;
	assign hi_job_pc        = boot_pc;

//  -----------------------------------------------------------------------
//  -- Control Unit - Counter and Next State sequential
//  -----------------------------------------------------------------------
	always_ff @ ( posedge clk, posedge reset )
		if ( reset ) begin
			state          <= START;
			thread_en      <= {`THREAD_NUMB{1'b0}};
			boot_thread_id <= 0;
		end else begin
			state          <= next_state; // default is to stay in current state
			if ( update_thread_en )
				thread_en      <= item_data_i[`THREAD_NUMB - 1 : 0];
			if ( update_boot_thread_id )
				boot_thread_id <= thread_id_t'( item_data_i );
		end

//  -----------------------------------------------------------------------
//  -- Control Unit - Next State Block
//  -----------------------------------------------------------------------
	always_comb begin
		next_state <= state;

		unique case ( state )
			START                 : begin
				if ( item_avail_i )
					next_state <= IDLE;
				else
					next_state <= START;
			end

			IDLE                  : begin
				if ( item_valid_i ) begin
					next_state <= read_message_from_host( message_in );
				end else
					next_state <= IDLE;
			end

			BOOT_WAIT_THREAD_ID   : begin
				if ( item_valid_i )
					next_state <= BOOT_WAIT_PC;
				else
					next_state <= BOOT_WAIT_THREAD_ID;
			end

			BOOT_WAIT_PC          : begin
				if ( item_valid_i )
					if ( item_avail_i )
						next_state <= IDLE;
					else
						next_state <= BOOT_SEND_ACK;
				else
					next_state <= BOOT_WAIT_PC;
			end

			BOOT_SEND_ACK         : begin
				if ( item_avail_i )
					next_state <= IDLE;
				else
					next_state <= BOOT_SEND_ACK;
			end

			ENABLE_CORE_WAIT_TM   : begin
				if ( item_valid_i )
					if ( item_avail_i )
						next_state <= IDLE;
					else
						next_state <= ENABLE_CORE_SEND_ACK;
				else
					next_state <= ENABLE_CORE_WAIT_TM;
			end

			ENABLE_CORE_SEND_ACK  : begin
				if ( item_avail_i )
					next_state <= IDLE;
				else
					next_state <= ENABLE_CORE_SEND_ACK;
			end

			READ_STATUS_WAIT_DATA : begin
				if ( item_valid_i )
					next_state <= READ_STATUS_WAIT_CR;
				else
					next_state <= READ_STATUS_WAIT_DATA;
			end

			READ_STATUS_WAIT_CR   : next_state <= IDLE;

		endcase
	end

//  -----------------------------------------------------------------------
//  -- Control Unit - Output Block
//  -----------------------------------------------------------------------
	always_comb begin
		hi_read_cr_valid      <= 1'b0;
		hi_read_cr_request    <= 0;
		avail_out             <= 1'b0;
		valid_out             <= 1'b0;
		update_boot_thread_id <= 1'b0;
		update_thread_en      <= 1'b0;
		boot_valid            <= 1'b0;
		boot_pc               <= 0;
		message_out           <= message_out;
		leds_out              <= 0;

		unique case ( state )
			START                 : begin
				leds_out    <= 8'b1111_0000;
				valid_out   <= 1'b1;
				message_out <= START_MEX;
			end

			IDLE                  : begin
				avail_out   <= 1'b1;
				leds_out    <= 8'b0000_0001;
			end

			ENABLE_CORE_WAIT_TM   : begin
				leds_out    <= 8'b0001_0000;
				avail_out   <= 1'b1;
				if ( item_valid_i ) begin
					update_thread_en      <= 1'b1;
					valid_out             <= 1'b1;
					message_out           <= ENABLE_CORE_ACK;
				end
			end

			ENABLE_CORE_SEND_ACK  : begin
				leds_out    <= 8'b0010_0000;
				valid_out   <= 1'b1;
				message_out <= ENABLE_CORE_ACK;
			end

			BOOT_WAIT_THREAD_ID   : begin
				leds_out    <= 8'b0000_0010;
				avail_out   <= 1'b1;
				if ( item_valid_i )
					update_boot_thread_id <= 1'b1;
			end

			BOOT_WAIT_PC          : begin
				leds_out    <= 8'b0000_0100;
				avail_out   <= 1'b1;
				if ( item_valid_i ) begin
					boot_valid            <= 1'b1;
					boot_pc               <= address_t'( item_data_i );
					valid_out             <= 1'b1;
					message_out           <= BOOT_ACK;
				end
			end

			BOOT_SEND_ACK         : begin
				leds_out    <= 8'b0000_1000;
				valid_out   <= 1'b1;
				message_out <= BOOT_ACK;
			end

			READ_STATUS_WAIT_DATA : begin
				if ( item_valid_i ) begin
					hi_read_cr_valid      <= 1'b1;
					hi_read_cr_request    <= item_data_i;
				end
			end

			READ_STATUS_WAIT_CR   : begin
				valid_out   <= 1'b1;
				message_out <= cr_response;
			end

		endcase
	end

endmodule