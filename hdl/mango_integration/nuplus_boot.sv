`ifndef __NUPLUS_BOOT_SV
`define __NUPLUS_BOOT_SV

// Host - NUPLUS commands
`define HOST_COMMAND_NUM   32
`define HOST_COMMAND_WIDTH ( $clog2( `HOST_COMMAND_NUM ) )

typedef enum logic [`HOST_COMMAND_WIDTH - 1 : 0] {
	BOOT_COMMAND            = 0,
	BOOT_ACK                = 1,
	ENABLE_CORE_COMMAND     = 2,
	ENABLE_CORE_ACK         = 3,
	STOP_CORE_COMMAND       = 4,
	STOP_CORE_ACK           = 5,
	GET_CORE_STATUS         = 6,
	START_MEX               = 7,
	READ_STATUS_COMMAND     = 8
} host_messages_t;

typedef enum {
	START,
	IDLE,
	BOOT_WAIT_THREAD_ID,
	BOOT_WAIT_PC,
	BOOT_SEND_ACK,
	ENABLE_CORE_WAIT_TM,
	ENABLE_CORE_SEND_ACK,
	READ_STATUS_WAIT_DATA,
	READ_STATUS_WAIT_CR
} interface_state_t;

function interface_state_t read_message_from_host ( host_messages_t message_in );
	interface_state_t next_state;
	unique case ( message_in )
		BOOT_COMMAND            : next_state = BOOT_WAIT_THREAD_ID;
		ENABLE_CORE_COMMAND     : next_state = ENABLE_CORE_WAIT_TM;
		STOP_CORE_COMMAND       : next_state = ENABLE_CORE_WAIT_TM;
		READ_STATUS_COMMAND		: next_state = READ_STATUS_WAIT_DATA;
		
		default : next_state                 = IDLE;
	endcase

	return next_state;

endfunction : read_message_from_host

`endif