`include "nuplus_user_defines.sv"
`include "nuplus_defines.sv"
`include "nuplus_coherence_defines.sv"
`include "nuplus_synchronization_defines.sv"

`ifndef SINGLE_CORE
`include "nuplus_message_service_defines.sv"
`endif

module nuplus_core # (
		parameter TILE_ID = 0,
		parameter CORE_ID = 0 )
	(
		input                                                    clk,
		input                                                    reset,
		input                                                    ext_freeze,
		input                                                    resume,
		input                        [`THREAD_NUMB - 1 : 0]      thread_en,

		// From Host Controller
		input  logic                                             hi_read_cr_valid,
		input  register_t                                        hi_read_cr_request,
		input  logic                                             hi_write_cr_valid,
		input  register_t                                        hi_write_cr_data,
		output register_t                                        cr_response,

		// Host Interface
		input  logic                                             hi_job_valid,
		input  address_t                                         hi_job_pc,
		input  thread_id_t                                       hi_job_thread_id,

		//From Host Debug
		input  logic                                             dsu_enable,
		input  logic                                             dsu_single_step,
		input  address_t             [7 : 0]                     dsu_breakpoint,
		input  logic                 [7 : 0]                     dsu_breakpoint_enable,
		input  logic                                             dsu_thread_selection,
		input  thread_id_t                                       dsu_thread_id,
		input  logic                                             dsu_en_vector,
		input  logic                                             dsu_en_scalar,
		input  logic                                             dsu_load_shift_reg,
		input  logic                                             dsu_start_shift,
		input  logic                 [`REGISTER_ADDRESS - 1 : 0] dsu_reg_addr,

		input  logic                                             dsu_write_scalar,
		input  logic                                             dsu_write_vector,
		input  logic                                             dsu_serial_reg_in,

		//To Host Debug
		output address_t             [`THREAD_NUMB - 1 : 0]      dsu_bp_instruction,
		output thread_id_t                                       dsu_bp_thread_id,
		output logic                                             dsu_serial_reg_out,
		output logic                                             dsu_stop_shift,
		output logic                                             dsu_hit_breakpoint,


`ifdef SINGLE_CORE
		output address_t                             n2m_request_address,
		output dcache_line_t                         n2m_request_data,
		output logic                                 n2m_request_read,
		output logic                                 n2m_request_write,
		output logic                                 mc_avail_o,

		input  logic                                 m2n_request_available,
		input  logic                                 m2n_response_valid,
		input  address_t                             m2n_response_address,
		input  dcache_line_t                         m2n_response_data,
`else
		// Network Interface - Instruction Cache
		input                                        mem_instr_request_available,

		output logic                                 tc_instr_request_valid,
		output address_t                             tc_instr_request_address,


		// From Network Interface - Data Cache
		input  logic                                 ni_request_network_available,
		input  logic                                 ni_forward_network_available,
		input  logic                                 ni_response_network_available,
		input  coherence_forwarded_message_t         ni_forwarded_request,
		input  logic                                 ni_forwarded_request_valid,
		input  coherence_response_message_t          ni_response,
		input  logic                                 ni_response_valid,

		// To Network Interface - Data Cache
		output logic                                 l1d_forwarded_request_consumed,
		output logic                                 l1d_response_consumed,

		output logic                                 l1d_request_valid,
		output coherence_request_message_t           l1d_request,
		output logic                                 l1d_request_has_data,
		output tile_address_t                [1 : 0] l1d_request_destinations,
		output logic                         [1 : 0] l1d_request_destinations_valid,

		output logic                                 l1d_response_valid,
		output coherence_response_message_t          l1d_response,
		output logic                                 l1d_response_has_data,
		output tile_address_t                [1 : 0] l1d_response_destinations,
		output logic                         [1 : 0] l1d_response_destinations_valid,

		output logic                                 l1d_forwarded_request_valid,
		output coherence_response_message_t          l1d_forwarded_request,
		output tile_address_t                        l1d_forwarded_request_destination,
`endif

		//Synchronize Barrier
		//From/To Virtual Network
		output                                                   bc2n_account_valid,
		output service_c2n_message_t                             bc2n_account_message,
`ifndef SINGLE_CORE
		output dest_valid_t bc2n_account_destination_valid,
`endif
		input  logic                                             n2bc_network_available,
		//From Network Interface
		input  service_c2n_message_t                             n2bc_release_message,
		input                                                    n2bc_release_valid,
		output logic                                             n2bc_mes_service_consumed
	);

	//  -----------------------------------------------------------------------
	//  -- Signals
	//  -----------------------------------------------------------------------
	// Thread Control Stage - Signals
	thread_id_t                                  tc_job_thread_id;
	address_t                                    tc_job_pc;
	logic                                        tc_job_valid;
	icache_lane_t                                tc_if_data_out;
	address_t                                    tc_if_addr_update_cache;
	logic                                        tc_if_valid_update_cache;
	thread_mask_t                                tc_if_thread_en;
	icache_lane_t                                mem_instr_request_data_in;
	logic                                        mem_instr_request_valid;

	// IF Stage - Signals
	logic                                        if_valid;
	thread_id_t                                  if_thread_selected_id;
	register_t                                   if_pc_scheduled;
	instruction_t                                if_inst_scheduled;
	logic                                        if_cache_miss;
	thread_mask_t                                if_thread_miss;
	address_t                                    if_address_miss;
	address_t             [`THREAD_NUMB - 1 : 0] if_current_pc;

	// Decode Stage - Signals
	logic                                        dec_valid;
	instruction_decoded_t                        dec_instr;

	// Instruction Buffer Stage - Signals
	thread_mask_t                                ib_fifo_full;
	thread_mask_t                                ib_instructions_valid;
	instruction_decoded_t [`THREAD_NUMB - 1 : 0] ib_instructions;

	// Instruction Scheduler Stage - Signals
	logic                                        is_instruction_valid;
	thread_id_t                                  is_thread_id;
	instruction_decoded_t                        is_instruction;
	thread_mask_t                                is_thread_scheduled_mask;
	scoreboard_t                                 is_destination_mask;

	// Operand Fetch Stage - Signals
	logic                                        opf_valid;
	instruction_decoded_t                        opf_inst_scheduled;
	hw_lane_t                                    opf_fetched_op0;
	hw_lane_t                                    opf_fetched_op1;
	hw_lane_mask_t                               opf_hw_lane_mask;
	scoreboard_t                                 opf_destination_bitmap;
	register_t                                   effective_address;
	logic                                        uncoherent_area_hit;

	// INT Pipe Stage - Signals
	logic                                        int_valid;
	instruction_decoded_t                        int_inst_scheduled;
	hw_lane_t                                    int_result;
	hw_lane_mask_t                               int_hw_lane_mask;

	// Control Registers - Signals
	register_t                                   cr_result;
	logic                                        cr_ctrl_cache_wt;

	// Branch Control Stage - Signals
	logic                                        bc_rollback_enable;
	logic                                        bc_rollback_valid;
	address_t                                    bc_rollback_pc;
	thread_id_t                                  bc_rollback_thread_id;
	scoreboard_t                                 bc_scoreboard;

	// FP Pipe Stage - Signals
	logic                                        fpu_valid;
	instruction_decoded_t                        fpu_inst_scheduled;
	hw_lane_mask_t                               fpu_fetched_mask;
	hw_lane_t                                    fpu_result_sp;

	// LDST Pipe Stage - Signals
	logic                                        l1d_valid;
	instruction_decoded_t                        l1d_instruction;
	dcache_line_t                                l1d_result;
	hw_lane_mask_t                               l1d_hw_lane_mask;
	dcache_store_mask_t                          l1d_store_mask;
	dcache_address_t                             l1d_address;
	thread_mask_t                                l1d_almost_full;
	logic                 [`THREAD_NUMB - 1 : 0] no_load_store_pending;
	logic                                        ldst_miss;
	logic                                        l1d_rollback_en;
	register_t                                   l1d_rollback_pc;
	thread_id_t                                  l1d_rollback_thread_id;
	logic                                        ldst_io_valid;
	instruction_decoded_t                        ldst_io_inst_scheduled;
	hw_lane_mask_t                               ldst_io_hw_lane_mask;
	address_t                                    ldst_io_address;
	dcache_line_t                                ldst_io_data;

	// Scratchpad Memory Stage - Signals
	logic                                        spm_valid;
	instruction_decoded_t                        spm_inst_scheduled;
	hw_lane_t                                    spm_result;
	hw_lane_mask_t                               spm_hw_lane_mask;
	logic                                        spm_can_issue;
	logic                                        spm_rollback_en;
	register_t                                   spm_rollback_pc;
	thread_id_t                                  spm_rollback_thread_id;

	// Rollback Handler Stage - Signals
	logic                 [`THREAD_NUMB - 1 : 0] rollback_valid;
	address_t             [`THREAD_NUMB - 1 : 0] rollback_pc_value;
	scoreboard_t          [`THREAD_NUMB - 1 : 0] rollback_clear_bitmap;
	logic                                        rollback_trap_en;
	thread_mask_t                                rollback_thread_id;
	register_t                                   rollback_trap_reason;

	// Writeback Stage - Signals
	logic                                        wb_valid;
	thread_id_t                                  wb_thread_id;
	wb_result_t                                  wb_result;
	logic                 [`NUM_EX_PIPE - 1 : 0] wb_fifo_full;

	logic                 [`THREAD_NUMB - 1 : 0] scoreboard_empty;
	logic                 [`THREAD_NUMB - 1 : 0] release_val;

	// DSU - Signals
	logic                                        freeze;
	logic                                        nfreeze;
	logic                 [`THREAD_NUMB - 1 : 0] dsu_stop_issue;

	assign nfreeze         = ~freeze;

`ifdef SINGLE_CORE
	logic                                     ldst_miss;
	logic                                     ldst_flush;
	logic                                     ldst_evict;

	logic                                     cc_update_ldst_valid;
	dcache_way_idx_t                          cc_update_ldst_way;
	dcache_address_t                          cc_update_ldst_address;
	dcache_privileges_t                       cc_update_ldst_privileges;
	dcache_line_t                             cc_update_ldst_store_value;
	cc_command_t                              cc_update_ldst_command;
	logic                                     cc_memory_busy;

	logic                                     cc_snoop_data_valid;
	dcache_set_t                              cc_snoop_data_set;
	dcache_way_idx_t                          cc_snoop_data_way;
	dcache_line_t                             ldst_snoop_data;

	logic                                     cc_wakeup;
	thread_id_t                               cc_wakeup_thread_id;

	logic                                     cc_snoop_tag_valid;
	dcache_set_t                              cc_snoop_tag_set;

	dcache_privileges_t [`DCACHE_WAY - 1 : 0] ldst_snoop_privileges;
	dcache_tag_t        [`DCACHE_WAY - 1 : 0] ldst_snoop_tag;
	logic                                     ldst_rollback_en;
	scal_reg_size_t                           ldst_rollback_pc;
	thread_id_t                               ldst_rollback_thread_id;

	// Memory interface
	logic                                     mem_instr_request_available;

	logic                                     tc_instr_request_valid;
	address_t                                 tc_instr_request_address;
`endif

`ifdef PERFORMANCE_SYNC
	logic [`THREAD_NUMB - 1 : 0]        counter_sync_send;
	logic [`THREAD_NUMB - 1 : 0]        counter_sync_detect;
	logic [1 : 0][`THREAD_NUMB - 1 : 0] perf_events;
`endif
//-------------------------------------------------------------
//  -- Thread Controller
//  -----------------------------------------------------------------------

	thread_controller u_thread_controller (
		.clk                         ( clk                         ),
		.reset                       ( reset                       ),
		.enable                      ( nfreeze                     ),
		// Host Interface
		.hi_job_valid                ( hi_job_valid                ),
		.hi_job_pc                   ( hi_job_pc                   ),
		.hi_job_thread_id            ( hi_job_thread_id            ),
		//Instruction fetch stage
		.if_cache_miss               ( if_cache_miss               ),
		.if_thread_miss              ( if_thread_miss              ),
		.if_address_miss             ( if_address_miss             ),
		.tc_if_data_out              ( tc_if_data_out              ),
		.tc_if_addr_update_cache     ( tc_if_addr_update_cache     ),
		.tc_if_valid_update_cache    ( tc_if_valid_update_cache    ),
		.tc_if_thread_en             ( tc_if_thread_en             ),
		.thread_en                   ( thread_en                   ), //external signal from user
		.ib_fifo_full                ( ib_fifo_full                ), //from instruction buffer
		.tc_job_pc                   ( tc_job_pc                   ),
		.tc_job_thread_id            ( tc_job_thread_id            ),
		.tc_job_valid                ( tc_job_valid                ),
		.dsu_stop_issue              ( dsu_stop_issue              ),
		//Memory interface
		.mem_instr_request_available ( mem_instr_request_available ),
		.mem_instr_request_data_in   ( mem_instr_request_data_in   ),
		.mem_instr_request_valid     ( mem_instr_request_valid     ),
		.tc_instr_request_address    ( tc_instr_request_address    ),
		.tc_instr_request_valid      ( tc_instr_request_valid      )

	);

//  -----------------------------------------------------------------------
//  -- Instruction Fetch
//  -----------------------------------------------------------------------

	instruction_fetch_stage u_instruction_fetch_stage (
		.clk                   ( clk                      ),
		.reset                 ( reset                    ),
		.enable                ( nfreeze                  ),
		// Rollback stage interface
		.rollback_valid        ( rollback_valid           ),
		.rollback_pc_value     ( rollback_pc_value        ),
		// Instruction fetch stage interface
		.if_valid              ( if_valid                 ),
		.if_thread_selected_id ( if_thread_selected_id    ),
		.if_pc_scheduled       ( if_pc_scheduled          ),
		.if_inst_scheduled     ( if_inst_scheduled        ),
		// To Control Register
		.if_current_pc         ( if_current_pc            ),
		// Thread controller stage interface
		.tc_data_out           ( tc_if_data_out           ),
		.tc_addr_update_cache  ( tc_if_addr_update_cache  ),
		.tc_valid_update_cache ( tc_if_valid_update_cache ),
		.tc_thread_en          ( tc_if_thread_en          ),
		.if_cache_miss         ( if_cache_miss            ),
		.if_thread_miss        ( if_thread_miss           ),
		.if_address_miss       ( if_address_miss          ),
		.tc_job_pc             ( tc_job_pc                ),
		.tc_job_thread_id      ( tc_job_thread_id         ),
		.tc_job_valid          ( tc_job_valid             )
	);

//  -----------------------------------------------------------------------
//  -- Decode
//  -----------------------------------------------------------------------

	decode u_decode (
		.clk                   ( clk                   ),
		.reset                 ( reset                 ),
		.enable                ( nfreeze               ),
		.if_valid              ( if_valid              ),
		.if_thread_selected_id ( if_thread_selected_id ),
		.if_pc_scheduled       ( if_pc_scheduled       ),
		.if_inst_scheduled     ( if_inst_scheduled     ),
		.rollback_valid        ( rollback_valid        ),
		.dec_valid             ( dec_valid             ),
		.dec_instr             ( dec_instr             )
	);

//  -----------------------------------------------------------------------
//  -- Instruction Buffer
//  -----------------------------------------------------------------------

	instruction_buffer # (
		.THREAD_FIFO_LENGTH ( `INSTRUCTION_FIFO_SIZE )
	)
	u_instruction_buffer (
		.clk                      ( clk                      ),
		.reset                    ( reset                    ),
		.enable                   ( nfreeze                  ),
		.dec_valid                ( dec_valid                ),
		.dec_instr                ( dec_instr                ),
		.l1d_full                 ( l1d_almost_full          ),
		.is_thread_scheduled_mask ( is_thread_scheduled_mask ),
		.rb_valid                 ( rollback_valid           ),
		.ib_fifo_full             ( ib_fifo_full             ),
		.ib_instructions_valid    ( ib_instructions_valid    ),
		.ib_instructions          ( ib_instructions          )
	);

//  -----------------------------------------------------------------------
//  -- Instruction Scheduler
//  -----------------------------------------------------------------------

	instruction_scheduler  #( 
		.TILE_ID( TILE_ID )
	)
	u_instruction_scheduler (
		.clk                      ( clk                      ),
		.reset                    ( reset                    ),
		.enable                   ( nfreeze                  ),
		.ib_instructions_valid    ( ib_instructions_valid    ),
		.ib_instructions          ( ib_instructions          ),
		.wb_valid                 ( wb_valid                 ),
		.wb_thread_id             ( wb_thread_id             ),
		.wb_result                ( wb_result                ),
		.wb_fifo_full             ( wb_fifo_full             ),
		.rb_valid                 ( rollback_valid           ),
		.rb_destination_mask      ( rollback_clear_bitmap    ),
		.dsu_stop_issue           ( dsu_stop_issue           ),
		.spm_can_issue            ( spm_can_issue            ),
		.is_instruction_valid     ( is_instruction_valid     ),
		.is_thread_id             ( is_thread_id             ),
		.is_instruction           ( is_instruction           ),
		.is_destination_mask      ( is_destination_mask      ),
		.scoreboard_empty         ( scoreboard_empty         ),
		.release_val              ( release_val              ),
`ifdef PERFORMANCE_SYNC
		.counter_sync_detect ( counter_sync_detect ) ,
`endif
		.is_thread_scheduled_mask ( is_thread_scheduled_mask )
	);

//  -----------------------------------------------------------------------
//  -- Operand Fetch
//  -----------------------------------------------------------------------

	operand_fetch u_operand_fetch (
		.clk                      ( clk                    ),
		.reset                    ( reset                  ),
		.enable                   ( nfreeze                ),
		.issue_valid              ( is_instruction_valid   ),
		.issue_thread_id          ( is_thread_id           ),
		.issue_inst_scheduled     ( is_instruction         ),
		.issue_destination_bitmap ( is_destination_mask    ),
		.rollback_valid           ( rollback_valid         ),
		.wb_valid                 ( wb_valid               ),
		.wb_thread_id             ( wb_thread_id           ),
		.wb_result                ( wb_result              ),
		//To Ex Pipes
		.opf_valid                ( opf_valid              ),
		.opf_inst_scheduled       ( opf_inst_scheduled     ),
		.opf_fetched_op0          ( opf_fetched_op0        ),
		.opf_fetched_op1          ( opf_fetched_op1        ),
		.opf_hw_lane_mask         ( opf_hw_lane_mask       ),
		.opf_destination_bitmap   ( opf_destination_bitmap ),
		// Coherency bit lookup
		.effective_address        ( effective_address      ),
		.uncoherent_area_hit      ( uncoherent_area_hit    )
	);

//  -----------------------------------------------------------------------
//  -- Ex Pipes
//  -----------------------------------------------------------------------

//  -----------------------------------------------------------------------
//  -- INT Pipe
//  -----------------------------------------------------------------------

	int_pipe #( 
		.TILE_ID( TILE_ID )
	)
	u_int_pipe (
		.clk                ( clk                ),
		.reset              ( reset              ),
		.enable             ( nfreeze            ),
		//From Operand Fetch
		.opf_valid          ( opf_valid          ),
		.opf_inst_scheduled ( opf_inst_scheduled ),
		.opf_fetched_op0    ( opf_fetched_op0    ),
		.opf_fetched_op1    ( opf_fetched_op1    ),
		.opf_hw_lane_mask   ( opf_hw_lane_mask   ),
		// From Control Register
		.cr_result          ( cr_result          ),
		//To Writeback
		.int_valid          ( int_valid          ),
		.int_inst_scheduled ( int_inst_scheduled ),
		.int_result         ( int_result         ),
		.int_hw_lane_mask   ( int_hw_lane_mask   )
	);

//  -----------------------------------------------------------------------
//  -- Control Register
//  -----------------------------------------------------------------------

	control_register #(
		.TILE_ID_PAR( TILE_ID ),
		.CORE_ID_PAR( CORE_ID )
	)
	u_control_register (
		.clk                 ( clk                    ),
		.reset               ( reset                  ),
		.enable              ( nfreeze                ),
		// From Host Controller
		.hi_read_cr_valid    ( hi_read_cr_valid       ),
		.hi_read_cr_request  ( hi_read_cr_request     ),
		.hi_write_cr_valid   ( hi_write_cr_valid      ),
		.hi_write_cr_data    ( hi_write_cr_data       ),
		.cr_response         ( cr_response            ),
		// From Instruction Fetch
		.if_current_pc       ( if_current_pc          ),
		// From Barrier Core
		.bc_release_thread   ( release_val            ),
		// From Thread Controller
		.tc_thread_en        ( tc_if_thread_en        ),
		.tc_inst_miss        ( tc_instr_request_valid ),
		// From Operand Fetch
		.opf_valid           ( opf_valid              ),
		.opf_inst_scheduled  ( opf_inst_scheduled     ),
		.opf_fetched_op0     ( opf_fetched_op0        ),
		.opf_fetched_op1     ( opf_fetched_op1        ),
		.effective_address   ( effective_address      ),
		// To Operand Fetch
		.uncoherent_area_hit ( uncoherent_area_hit    ),
		// From LDST
		.ldst_miss           ( ldst_miss              ),
		.ldst_almost_full    ( l1d_almost_full        ),
		// From Rollback Handler
		.rollback_trap_en    ( rollback_trap_en       ),
		.rollback_thread_id  ( rollback_thread_id     ),
		.rollback_trap_reason( rollback_trap_reason   ),
		// To Writeback
		.cr_result           ( cr_result              ),
		// Configuration signals
		.cr_ctrl_cache_wt    ( cr_ctrl_cache_wt       )
	);

//  -----------------------------------------------------------------------
//  -- Branch Control Pipe
//  -----------------------------------------------------------------------

	branch_control u_branch_control (
		//From Operand Fetch
		.opf_valid              ( opf_valid              ),
		.opf_inst_scheduled     ( opf_inst_scheduled     ),
		.opf_fetched_op0        ( opf_fetched_op0        ),
		.opf_fetched_op1        ( opf_fetched_op1        ),
		.opf_destination_bitmap ( opf_destination_bitmap ),

		//To Rollback Handler
		.bc_rollback_enable     ( bc_rollback_enable     ),
		.bc_rollback_valid      ( bc_rollback_valid      ),
		.bc_rollback_pc         ( bc_rollback_pc         ),
		.bc_rollback_thread_id  ( bc_rollback_thread_id  ),
		.bc_scoreboard          ( bc_scoreboard          )
	);

//  -----------------------------------------------------------------------
//  -- FPU Pipe
//  -----------------------------------------------------------------------

	fp_pipe #(
		.ADDER_FP_INST  ( 1 ),
		.MUL_FP_INST    ( 1 ),
		.DIV_FP_INST    ( 1 ),
		.FIX2FP_FP_INST ( 1 ),
		.FP2FIX_FP_INST ( 1 ),
		.ADDER_DP_INST  ( 0 ),
		.MUL_DP_INST    ( 0 ),
		.DIV_DP_INST    ( 0 ),
		.FIX2FP_DP_INST ( 0 ),
		.FP2FIX_DP_INST ( 0 )
	)
	u_fp_pipe (
		.clk               ( clk                ),
		.reset             ( reset              ),
		.enable            ( nfreeze            ),
		// To Writeback
		.fpu_fecthed_mask  ( fpu_fetched_mask   ),
		.fpu_inst_scheduled( fpu_inst_scheduled ),
		.fpu_result_sp     ( fpu_result_sp      ),
		.fpu_valid         ( fpu_valid          ),
		// From Operand Fetch
		.opf_fecthed_mask  ( opf_hw_lane_mask   ),
		.opf_fetched_op0   ( opf_fetched_op0    ),
		.opf_fetched_op1   ( opf_fetched_op1    ),
		.opf_inst_scheduled( opf_inst_scheduled ),
		.opf_valid         ( opf_valid          )
	);

//  -----------------------------------------------------------------------
//  -- SFU Pipe
//  -----------------------------------------------------------------------

//  -----------------------------------------------------------------------
//  -- L1 Data Cache Memory
//  -----------------------------------------------------------------------

	l1d_cache #(
		.TILE_ID( TILE_ID ),
		.CORE_ID( CORE_ID ) )
	u_l1d_cache (
		.clk                             ( clk                             ),
		.reset                           ( reset                           ),
		//From Operand Fetch
		.opf_valid                       ( opf_valid                       ),
		.opf_inst_scheduled              ( opf_inst_scheduled              ),
		.opf_fetched_op0                 ( opf_fetched_op0                 ),
		.opf_fetched_op1                 ( opf_fetched_op1                 ),
		.opf_hw_lane_mask                ( opf_hw_lane_mask                ),
		//From Network Interface
		.ni_request_network_available    ( ni_request_network_available    ),
		.ni_forward_network_available    ( ni_forward_network_available    ),
		.ni_response_network_available   ( ni_response_network_available   ),
		.ni_forwarded_request            ( ni_forwarded_request            ),
		.ni_forwarded_request_valid      ( ni_forwarded_request_valid      ),
		.ni_response                     ( ni_response                     ),
		.ni_response_valid               ( ni_response_valid               ),
		//From Rollback Handler
		.rollback_valid                  ( rollback_valid                  ),
		//To Network Interface
		.l1d_forwarded_request_consumed  ( l1d_forwarded_request_consumed  ),
		.l1d_response_consumed           ( l1d_response_consumed           ),
		.l1d_request_valid               ( l1d_request_valid               ),
		.l1d_request                     ( l1d_request                     ),
		.l1d_request_has_data            ( l1d_request_has_data            ),
		.l1d_request_destinations        ( l1d_request_destinations        ),
		.l1d_request_destinations_valid  ( l1d_request_destinations_valid  ),
		.l1d_response_valid              ( l1d_response_valid              ),
		.l1d_response                    ( l1d_response                    ),
		.l1d_response_has_data           ( l1d_response_has_data           ),
		.l1d_response_destinations       ( l1d_response_destinations       ),
		.l1d_response_destinations_valid ( l1d_response_destinations_valid ),
		.l1d_forwarded_request_valid     ( l1d_forwarded_request_valid     ),
		.l1d_forwarded_request           ( l1d_forwarded_request           ),
		.l1d_forwarded_request_destination ( l1d_forwarded_request_destination ),
		//To Writeback
		.l1d_valid                       ( l1d_valid                       ),
		.l1d_instruction                 ( l1d_instruction                 ),
		.l1d_result                      ( l1d_result                      ),
		.l1d_hw_lane_mask                ( l1d_hw_lane_mask                ),
		.l1d_store_mask                  ( l1d_store_mask                  ),
		.l1d_address                     ( l1d_address                     ),
		//To Instruction Scheduler
		.l1d_almost_full                 ( l1d_almost_full                 ),
		//To Sync Core
		.no_load_store_pending           ( no_load_store_pending           ),
		//To Rollback Handler
		.l1d_rollback_en                 ( l1d_rollback_en                 ),
		.l1d_rollback_pc                 ( l1d_rollback_pc                 ),
		.l1d_rollback_thread_id          ( l1d_rollback_thread_id          ),
		.mem_instr_request_data_in       ( mem_instr_request_data_in       ),
		.mem_instr_request_valid         ( mem_instr_request_valid         ),
		// To Performance Counters
		.ldst_miss                       ( ldst_miss                       ),
		// From Control Register
		.cr_ctrl_cache_wt                ( cr_ctrl_cache_wt                )
	);

//  -----------------------------------------------------------------------
//  -- Scratchpad Memory
//  -----------------------------------------------------------------------

	scratchpad_memory_pipe u_scratchpad_memory_pipe (
		.clk                    ( clk                    ),
		.reset                  ( reset                  ),

		//From Operand Fetch
		.opf_valid              ( opf_valid              ),
		.opf_inst_scheduled     ( opf_inst_scheduled     ),
		.opf_fetched_op0        ( opf_fetched_op0        ),
		.opf_fetched_op1        ( opf_fetched_op1        ),
		.opf_hw_lane_mask       ( opf_hw_lane_mask       ),

		//To Writeback
		.spm_valid              ( spm_valid              ),
		.spm_inst_scheduled     ( spm_inst_scheduled     ),
		.spm_result             ( spm_result             ),
		.spm_hw_lane_mask       ( spm_hw_lane_mask       ),

		//To Dynamic Scheduler
		.spm_can_issue          ( spm_can_issue          ),

		//To RollbackController - Questi segnali si devono attivare in modo combinatoriale nel primo ciclo di clock. Questo è vero per tutte le pipe d'esecuzione.
		.spm_rollback_en        ( spm_rollback_en        ),
		.spm_rollback_pc        ( spm_rollback_pc        ),
		.spm_rollback_thread_id ( spm_rollback_thread_id )
	);

//  -----------------------------------------------------------------------
//  -- Rollback Handler
//  ----------------------------------------------------------------------- 
        
	rollback_handler u_rollback_handler (
		.clk                    ( clk                     ),
		.reset                  ( reset                   ),
		.enable                 ( 1'b1                    ), //( nfreeze                ),
		.is_instruction_valid   ( is_instruction_valid    ),
		.is_thread_id           ( is_thread_id            ),
		.is_destination_mask    ( is_destination_mask     ), //TODO: caso SMT devono essere `THREAD_NUMB
		.bc_scoreboard          ( bc_scoreboard           ),
		.bc_rollback_enable     ( bc_rollback_enable      ),
		.bc_rollback_valid      ( bc_rollback_valid       ),
		.bc_rollback_pc         ( bc_rollback_pc          ),
		.bc_rollback_thread_id  ( bc_rollback_thread_id   ),
		// From SPM
		.spm_rollback_en        ( spm_rollback_en         ),
		.spm_rollback_pc        ( spm_rollback_pc         ),
		.spm_rollback_thread_id ( spm_rollback_thread_id  ),
		// From LDST
		.l1d_rollback_en        ( l1d_rollback_en         ),
		.l1d_rollback_pc        ( l1d_rollback_pc         ),
		.l1d_rollback_thread_id ( l1d_rollback_thread_id  ),
		// To Control Register
		.rollback_trap_en       ( rollback_trap_en        ),
		.rollback_thread_id     ( rollback_thread_id      ),
		.rollback_trap_reason   ( rollback_trap_reason    ),
		.rollback_pc_value      ( rollback_pc_value       ),
		.rollback_valid         ( rollback_valid          ),
		.rollback_clear_bitmap  ( rollback_clear_bitmap   )
	);

//  -----------------------------------------------------------------------
//  -- WriteBack
//  -----------------------------------------------------------------------

	writeback #( 
		.TILE_ID( TILE_ID )
	)
	u_writeback (
		.clk                 ( clk                ),
		.reset               ( reset              ),
		.enable              ( 1'b1               ), //( nfreeze              ),
		//From FP Ex Pipe
		.fp_valid            ( fpu_valid          ),
		.fp_inst_scheduled   ( fpu_inst_scheduled ),
		.fp_result           ( fpu_result_sp      ),
		.fp_mask_reg         ( fpu_fetched_mask   ),

		//From INT Ex Pipe
		.int_valid           ( int_valid          ),
		.int_inst_scheduled  ( int_inst_scheduled ),
		.int_result          ( int_result         ),
		.int_hw_lane_mask    ( int_hw_lane_mask   ),

		//From Scrathpad Memory Pipe
		.spm_valid           ( spm_valid          ),
		.spm_inst_scheduled  ( spm_inst_scheduled ),
		.spm_result          ( spm_result         ),
		.spm_hw_lane_mask    ( spm_hw_lane_mask   ),

		//From Cache L1 Pipe
		.ldst_valid          ( l1d_valid          ),
		.ldst_inst_scheduled ( l1d_instruction    ),
		.ldst_result         ( l1d_result         ),
		.ldst_hw_lane_mask   ( l1d_hw_lane_mask   ),
		.ldst_address        ( l1d_address        ),
		//To Operand Fetch
		.wb_valid            ( wb_valid           ),
		.wb_thread_id        ( wb_thread_id       ),
		.wb_result           ( wb_result          ),

		//TO Dynamic Scheduler
		.wb_fifo_full        ( wb_fifo_full       )
	);

//  -----------------------------------------------------------------------
//  -- Debug Support Unit
//  -----------------------------------------------------------------------

	debug_controller u_debug_controller (
		.clk                   ( clk                   ),
		.reset                 ( reset                 ),
		.resume                ( resume                ),
		.ext_freeze            ( ext_freeze            ),
		.dsu_enable            ( dsu_enable            ),
		.dsu_single_step       ( dsu_single_step       ),
		.dsu_breakpoint        ( dsu_breakpoint        ),
		.dsu_breakpoint_enable ( dsu_breakpoint_enable ),
		.dsu_thread_selection  ( dsu_thread_selection  ),
		.dsu_thread_id         ( dsu_thread_id         ),
		//From Instruction Scheduler
		.is_instruction_valid  ( is_instruction_valid  ),
		.is_instruction        ( is_instruction        ),
		.is_thread_id          ( is_thread_id          ),
		.scoreboard_empty      ( scoreboard_empty      ),
		//From LDST
		.no_load_store_pending ( no_load_store_pending ),
		//From Rollback Handler
		.rollback_valid        ( rollback_valid        ),
		.dsu_bp_instruction    ( dsu_bp_instruction    ),
		.dsu_bp_thread_id      ( dsu_bp_thread_id      ),
		.dsu_hit_breakpoint    ( dsu_hit_breakpoint    ),
		//From SX stage
		.dsu_stop_issue        ( dsu_stop_issue        ),
		.freeze                ( freeze                )
	);

	barrier_core #(
		.TILE_ID( TILE_ID )
	)
	u_barrier_core (
		.clk                      ( clk                       ),
		.reset                    ( reset                     ),
		//Operand Fetch
		.opf_valid                ( opf_valid                 ), //controllare il bit dell'istruzione
		.opf_inst_scheduled       ( opf_inst_scheduled        ),
		//To ThreadScheduler
		.release_val              ( release_val               ),
		.scoreboard_empty         ( scoreboard_empty          ),
		//Id_Barrier
		.opf_fetched_op0          ( opf_fetched_op0           ),
		//Destination Barrier
		.opf_fetched_op1          ( opf_fetched_op1           ),
`ifdef PERFORMANCE_SYNC
		.counter_sync_send ( counter_sync_send ),
`endif
//to Network Interface
`ifndef SINGLE_CORE
		.c2n_account_destination_valid ( bc2n_account_destination_valid ),
`endif
		.c2n_account_valid        ( bc2n_account_valid        ),
		.c2n_account_message      ( bc2n_account_message      ),
		.network_available        ( n2bc_network_available    ),
		//From Network Interface
		.n2c_release_message      ( n2bc_release_message      ),
		.n2c_release_valid        ( n2bc_release_valid        ),
		.n2c_mes_service_consumed ( n2bc_mes_service_consumed ),

		//Load Store Unit
		.no_load_store_pending    ( no_load_store_pending     )
	);

`ifdef PERFORMANCE_SYNC
	assign perf_events[0] = counter_sync_detect;
	assign perf_events[1] = counter_sync_send;

	performance_counter #(
		.TILE_ID_PAR ( TILE_ID )
		)u_performance_counter (
		.clk            ( clk             ),
		.opf_fetched_op0( opf_fetched_op0 ),
		.perf_events    ( perf_events     ),
		.reset          ( reset           )
	);
`endif

endmodule
