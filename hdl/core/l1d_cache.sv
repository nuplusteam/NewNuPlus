`include "nuplus_defines.sv"
`include "nuplus_coherence_defines.sv"

/*
 * This file contains all the load-store stage. It includes the cache controller to manage the memory coherence.
 * Interface with NI: receives form NI the forwarded and response messages; sends to NI request and response to
 * a TILE (yet converted) plus signal to ack the consumption of the received messages.
 */

module l1d_cache #(
		parameter TILE_ID = 0,
		parameter CORE_ID = 0 )
	(

		input                                                       clk,
		input                                                       reset,

		// From Operand Fetch
		input  logic                                                opf_valid,
		input  instruction_decoded_t                                opf_inst_scheduled,
		input  hw_lane_t                                            opf_fetched_op0,
		input  hw_lane_t                                            opf_fetched_op1,
		input  hw_lane_mask_t                                       opf_hw_lane_mask,

		// From Network Interface
		input  logic                                                ni_request_network_available,
		input  logic                                                ni_forward_network_available,
		input  logic                                                ni_response_network_available,
		input  coherence_forwarded_message_t                        ni_forwarded_request,
		input  logic                                                ni_forwarded_request_valid,
		input  coherence_response_message_t                         ni_response,
		input  logic                                                ni_response_valid,

		// From Rollback Handler
		input  thread_mask_t                                        rollback_valid,

		// To Network Interface
		output logic                                                l1d_forwarded_request_consumed,
		output logic                                                l1d_response_consumed,
		output logic                                                l1d_request_valid,
		output coherence_request_message_t                          l1d_request,
		output logic                                                l1d_request_has_data,
		output tile_address_t                [1 : 0]                l1d_request_destinations,
		output logic                         [1 : 0]                l1d_request_destinations_valid,

		output logic                                                l1d_response_valid,
		output coherence_response_message_t                         l1d_response,
		output logic                                                l1d_response_has_data,
		output tile_address_t                [1 : 0]                l1d_response_destinations,
		output logic                         [1 : 0]                l1d_response_destinations_valid,

		output logic                                                l1d_forwarded_request_valid,
		output coherence_forwarded_message_t                        l1d_forwarded_request,
		output tile_address_t                                       l1d_forwarded_request_destination,

		// To Writeback
		output logic                                                l1d_valid,
		output instruction_decoded_t                                l1d_instruction,
		output dcache_line_t                                        l1d_result,
		output hw_lane_mask_t                                       l1d_hw_lane_mask,
		output dcache_store_mask_t                                  l1d_store_mask,
		output dcache_address_t                                     l1d_address,

		// To Instruction Scheduler
		output thread_mask_t                                        l1d_almost_full,

		// To Synch Core
		output logic                         [`THREAD_NUMB - 1 : 0] no_load_store_pending,

		// To Rollback Handler
		output logic                                                l1d_rollback_en,
		output register_t                                           l1d_rollback_pc,
		output thread_id_t                                          l1d_rollback_thread_id,

		// To Thread Controller
		output icache_lane_t                                        mem_instr_request_data_in,
		output logic                                                mem_instr_request_valid,

		// To Performance Counters
		output logic                                                ldst_miss,

		// From Control Register
		input  logic                                                cr_ctrl_cache_wt
	);

	logic                                     ci_store_request_valid;
	thread_id_t                               ci_store_request_thread_id;
	dcache_address_t                          ci_store_request_address;
	logic                                     ci_store_request_coherent;
	logic                                     ci_load_request_valid;
	thread_id_t                               ci_load_request_thread_id;
	dcache_address_t                          ci_load_request_address;
	logic                                     ci_load_request_coherent;
	logic                                     ci_replacement_request_valid;
	thread_id_t                               ci_replacement_request_thread_id;
	dcache_address_t                          ci_replacement_request_address;
	dcache_line_t                             ci_replacement_request_cache_line;
	logic                                     ci_flush_request_valid;
	dcache_address_t                          ci_flush_request_address;
	logic                                     ci_flush_request_coherent;
	logic                                     ci_flush_fifo_available;

	logic                                     cc_dequeue_store_request;
	logic                                     cc_dequeue_load_request;
	logic                                     cc_dequeue_replacement_request;
	logic                                     cc_snoop_tag_valid;
	logic                                     cc_dequeue_flush_request;
	dcache_set_t                              cc_snoop_tag_set;
	logic                                     cc_update_ldst_valid;
	cc_command_t                              cc_update_ldst_command;
	dcache_way_idx_t                          cc_update_ldst_way;
	dcache_address_t                          cc_update_ldst_address;
	dcache_privileges_t                       cc_update_ldst_privileges;
	dcache_line_t                             cc_update_ldst_store_value;
	logic                                     cc_snoop_data_valid;
	dcache_set_t                              cc_snoop_data_set;
	dcache_way_idx_t                          cc_snoop_data_way;
	logic                                     cc_wakeup;
	thread_id_t                               cc_wakeup_thread_id;

	dcache_privileges_t [`DCACHE_WAY - 1 : 0] ldst_snoop_privileges;
	dcache_tag_t        [`DCACHE_WAY - 1 : 0] ldst_snoop_tag;
	dcache_line_t                             ldst_snoop_data;
	dcache_set_t                              ldst_lru_update_set;
	logic                                     ldst_lru_update_en;
	dcache_way_idx_t                          ldst_lru_update_way;
	logic                                     ldst_evict;
	logic                                     ldst_flush;

	logic                                     vn_forwarded_valid;
    coherence_forwarded_message_t             vn_forwarded_request;
    logic                                     vn_response_valid;
    coherence_response_message_t              vn_response_request;
	logic                                     vn_forwarded_request_consumed;
	logic                                     vn_response_consumed;
	logic									  l1d_response_consumed_to_vn;
	logic									  l1d_forwarded_request_consumed_to_vn;

	assign l1d_forwarded_request_consumed = vn_forwarded_request_consumed;
	assign l1d_response_consumed          = vn_response_consumed;

	vn_interface u_vn_interface (
		.clk                        ( clk                        ),
		.reset                      ( reset                      ),

		// From Network Interface
		.ni_forwarded_request       ( ni_forwarded_request       ),
		.ni_forwarded_request_valid ( ni_forwarded_request_valid ),
		.ni_response			    ( ni_response                ),
		.ni_response_valid          ( ni_response_valid          ),

		// To Network Interface
		.vn_forwarded_request_consumed ( vn_forwarded_request_consumed ),
		.vn_response_consumed           ( vn_response_consumed          ),

		// To L1D
		.vn_forwarded_valid            ( vn_forwarded_valid            ),
		.vn_forwarded_request          ( vn_forwarded_request          ),
		.vn_response_valid             ( vn_response_valid             ),
		.vn_response_request           ( vn_response_request           ),

		// From L1D
		.l1d_forwarded_request_consumed ( l1d_forwarded_request_consumed_to_vn),
		.l1d_response_consumed          ( l1d_response_consumed_to_vn         )
	);

	core_interface u_core_interface (
		.clk                              ( clk                               ),
		.reset                            ( reset                             ),
		//Load Store Unit
		.ldst_instruction                 ( l1d_instruction                   ),
		.ldst_address                     ( l1d_address                       ),
		.ldst_miss                        ( ldst_miss                         ),
		.ldst_evict                       ( ldst_evict                        ),
		.ldst_flush                       ( ldst_flush                        ),
		.ldst_cache_line                  ( l1d_result                        ),
		//Cache Controller
		.cc_dequeue_store_request         ( cc_dequeue_store_request          ),
		.cc_dequeue_load_request          ( cc_dequeue_load_request           ),
		.cc_dequeue_replacement_request   ( cc_dequeue_replacement_request    ),
		.cc_dequeue_flush_request         ( cc_dequeue_flush_request          ),
		.ci_store_request_valid           ( ci_store_request_valid            ),
		.ci_store_request_thread_id       ( ci_store_request_thread_id        ),
		.ci_store_request_address         ( ci_store_request_address          ),
		.ci_store_request_coherent        ( ci_store_request_coherent         ),
		.ci_load_request_valid            ( ci_load_request_valid             ),
		.ci_load_request_thread_id        ( ci_load_request_thread_id         ),
		.ci_load_request_address          ( ci_load_request_address           ),
		.ci_load_request_coherent         ( ci_load_request_coherent          ),
		.ci_replacement_request_valid     ( ci_replacement_request_valid      ),
		.ci_replacement_request_thread_id ( ci_replacement_request_thread_id  ),
		.ci_replacement_request_address   ( ci_replacement_request_address    ),
		.ci_replacement_request_cache_line( ci_replacement_request_cache_line ),
		.ci_flush_request_valid           ( ci_flush_request_valid            ),
		.ci_flush_request_address         ( ci_flush_request_address          ),
		.ci_flush_request_coherent        ( ci_flush_request_coherent         ),
		.ci_flush_fifo_available          ( ci_flush_fifo_available           )
	);

	cache_controller #(
		.TILE_ID( TILE_ID ),
		.CORE_ID( CORE_ID )
	) u_cache_controller (
		.clk                                  ( clk                               ),
		.reset                                ( reset                             ),
		//From Core Interface
		.ci_store_request_valid               ( ci_store_request_valid            ),
		.ci_store_request_thread_id           ( ci_store_request_thread_id        ),
		.ci_store_request_address             ( ci_store_request_address          ),
		.ci_store_request_coherent            ( ci_store_request_coherent         ),
		.ci_load_request_valid                ( ci_load_request_valid             ),
		.ci_load_request_thread_id            ( ci_load_request_thread_id         ),
		.ci_load_request_address              ( ci_load_request_address           ),
		.ci_load_request_coherent             ( ci_load_request_coherent          ),
		.ci_replacement_request_valid         ( ci_replacement_request_valid      ),
		.ci_replacement_request_thread_id     ( ci_replacement_request_thread_id  ),
		.ci_replacement_request_address       ( ci_replacement_request_address    ),
		.ci_replacement_request_cache_line    ( ci_replacement_request_cache_line ),
		.ci_flush_request_valid               ( ci_flush_request_valid            ),
		.ci_flush_request_address             ( ci_flush_request_address          ),
		.ci_flush_request_coherent            ( ci_flush_request_coherent         ),
		//From Network Interface
		.ni_request_network_available         ( ni_request_network_available      ),
		.ni_forward_network_available         ( ni_forward_network_available      ),
		.ni_response_network_available        ( ni_response_network_available     ),
		.ni_forwarded_request                 ( vn_forwarded_request              ),
		.ni_forwarded_request_valid           ( vn_forwarded_valid                ),
		.ni_response_eject                    ( vn_response_request               ),
		.ni_response_eject_valid              ( vn_response_valid                 ),
		//From Load Store Unit
		.ldst_snoop_tag                       ( ldst_snoop_tag                    ),
		.ldst_snoop_privileges                ( ldst_snoop_privileges             ),
		.ldst_lru_update_set                  ( ldst_lru_update_set               ),
		.ldst_lru_update_en                   ( ldst_lru_update_en                ),
		.ldst_lru_update_way                  ( ldst_lru_update_way               ),
		.ldst_snoop_data                      ( ldst_snoop_data                   ),
		//To Core Interface
		.cc_dequeue_store_request             ( cc_dequeue_store_request          ),
		.cc_dequeue_load_request              ( cc_dequeue_load_request           ),
		.cc_dequeue_replacement_request       ( cc_dequeue_replacement_request    ),
		.cc_dequeue_flush_request             ( cc_dequeue_flush_request          ),
		//To Network Interface
		.cc_forwarded_request_consumed        ( l1d_forwarded_request_consumed_to_vn ),
		.cc_response_eject_consumed           ( l1d_response_consumed_to_vn       ),
		.cc_request_valid                     ( l1d_request_valid                 ),
		.cc_request                           ( l1d_request                       ),
		.cc_request_has_data                  ( l1d_request_has_data              ),
		.cc_request_destinations              ( l1d_request_destinations          ),
		.cc_request_destinations_valid        ( l1d_request_destinations_valid    ),
		.cc_response_inject_valid             ( l1d_response_valid                ),
		.cc_response_inject                   ( l1d_response                      ),
		.cc_response_inject_has_data          ( l1d_response_has_data             ),
		.cc_response_inject_destinations      ( l1d_response_destinations         ),
		.cc_response_inject_destinations_valid( l1d_response_destinations_valid   ),
		.cc_forwarded_request_valid           ( l1d_forwarded_request_valid       ),
		.cc_forwarded_request                 ( l1d_forwarded_request             ),
		.cc_forwarded_request_destination     ( l1d_forwarded_request_destination ),
		//To Load Store Unit
		.cc_snoop_tag_valid                   ( cc_snoop_tag_valid                ),
		.cc_snoop_tag_set                     ( cc_snoop_tag_set                  ),
		.cc_update_ldst_valid                 ( cc_update_ldst_valid              ),
		.cc_update_ldst_command               ( cc_update_ldst_command            ),
		.cc_update_ldst_way                   ( cc_update_ldst_way                ),
		.cc_update_ldst_address               ( cc_update_ldst_address            ),
		.cc_update_ldst_privileges            ( cc_update_ldst_privileges         ),
		.cc_update_ldst_store_value           ( cc_update_ldst_store_value        ),
		.cc_snoop_data_valid                  ( cc_snoop_data_valid               ),
		.cc_snoop_data_set                    ( cc_snoop_data_set                 ),
		.cc_snoop_data_way                    ( cc_snoop_data_way                 ),
		.cc_wakeup                            ( cc_wakeup                         ),
		.cc_wakeup_thread_id                  ( cc_wakeup_thread_id               ),
		.mem_instr_request_data_in            ( mem_instr_request_data_in         ),
		.mem_instr_request_valid              ( mem_instr_request_valid           )
	);


	load_store_unit #( 
		.TILE_ID( TILE_ID )
	)
	u_load_store_unit (
		.clk                        ( clk                         ),
		.reset                      ( reset                       ),
		//Operand Fetch
		.opf_valid                  ( opf_valid                   ),
		.opf_inst_scheduled         ( opf_inst_scheduled          ),
		.opf_fecthed_op0            ( opf_fetched_op0             ),
		.opf_fecthed_op1            ( opf_fetched_op1             ),
		.opf_hw_lane_mask           ( opf_hw_lane_mask            ),
		//Synch Core
		.no_load_store_pending      ( no_load_store_pending       ),
		//replacement and Cache Controller
		.ldst_valid                 ( l1d_valid                   ),
		.ldst_instruction           ( l1d_instruction             ),
		.ldst_cache_line            ( l1d_result                  ),
		.ldst_hw_lane_mask          ( l1d_hw_lane_mask            ),
		.ldst_store_mask            ( l1d_store_mask              ),
		.ldst_address               ( l1d_address                 ),
		.ldst_miss                  ( ldst_miss                   ),
		.ldst_evict                 ( ldst_evict                  ),
		.ldst_flush                 ( ldst_flush                  ),
		.ci_flush_fifo_available    ( ci_flush_fifo_available     ),
		//Instruction Scheduler
		.ldst_almost_full           ( l1d_almost_full             ),
		//Cache Controller - Thread weakup
		.cc_wakeup                  ( cc_wakeup                   ),
		.cc_wakeup_thread_id        ( cc_wakeup_thread_id         ),
		//Cache Controller - Update Bus
		.cc_update_ldst_valid       ( cc_update_ldst_valid        ),
		.cc_update_ldst_command     ( cc_update_ldst_command      ),
		.cc_update_ldst_way         ( cc_update_ldst_way          ),
		.cc_update_ldst_address     ( cc_update_ldst_address      ),
		.cc_update_ldst_privileges  ( cc_update_ldst_privileges   ),
		.cc_update_ldst_store_value ( cc_update_ldst_store_value  ),
		.cc_memory_busy             ( 1'b0                        ),
		//Cache Controller - Tag Snoop Bus
		.cc_snoop_tag_valid         ( cc_snoop_tag_valid          ),
		.cc_snoop_tag_set           ( cc_snoop_tag_set            ),
		.ldst_snoop_privileges      ( ldst_snoop_privileges       ),
		.ldst_snoop_tag             ( ldst_snoop_tag              ),
		//Cache Controller - Data Snoop Bus
		.cc_snoop_data_valid        ( cc_snoop_data_valid         ),
		.cc_snoop_data_set          ( cc_snoop_data_set           ),
		.cc_snoop_data_way          ( cc_snoop_data_way           ),
		.ldst_snoop_data            ( ldst_snoop_data             ),
		//Cache Controller Stage 2 - LRU Update Bus
		.ldst_lru_update_en         ( ldst_lru_update_en          ),
		.ldst_lru_update_set        ( ldst_lru_update_set         ),
		.ldst_lru_update_way        ( ldst_lru_update_way         ),
		//Rollback Handler
		.rollback_valid             ( rollback_valid              ),
		.ldst_rollback_en           ( l1d_rollback_en             ),
		.ldst_rollback_pc           ( l1d_rollback_pc             ),
		.ldst_rollback_thread_id    ( l1d_rollback_thread_id      ),

		.cc_update_hw_lane_mask     ( hw_lane_mask_t'( 0 )        ),
		.cc_update_ldst_instruction ( instruction_decoded_t'( 0 ) ),
		.cc_io_available            ( 1'b0                        ),
		.ldst_io_valid              (                             ),
		.ldst_io_inst_scheduled     (                             ),
		.ldst_io_hw_lane_mask       (                             ),
		.ldst_io_address            (                             ),
		.ldst_io_data               (                             ),
		.cr_ctrl_cache_wt           ( cr_ctrl_cache_wt            )
	);

endmodule
