`include "nuplus_coherence_defines.sv"

/* verilator lint_off WIDTHCONCAT */
function dcache_privileges_t state_2_privileges ( coherence_state_t state );
	dcache_privileges_t result;
	unique case ( state )
		M    : begin
			result.can_read  = 1'b1;
			result.can_write = 1'b1;
		end

		U    : begin
			result.can_read  = 1'b1;
			result.can_write = 1'b1;
		end

		S    : begin
			result.can_read  = 1'b1;
			result.can_write = 1'b0;
		end

		I    : begin
			result.can_read  = 1'b0;
			result.can_write = 1'b0;
		end

		ISd  : begin
			result.can_read  = 1'b0;
			result.can_write = 1'b0;
		end

		IMad : begin
			result.can_read  = 1'b0;
			result.can_write = 1'b0;
		end

		IMa  : begin
			result.can_read  = 1'b1;
			result.can_write = 1'b0;
		end

		SMad : begin
			result.can_read  = 1'b1;
			result.can_write = 1'b0;
		end

		SMa  : begin
			result.can_read  = 1'b1;
			result.can_write = 1'b0;
		end

		MIa  : begin
			result.can_read  = 1'b0;
			result.can_write = 1'b0;
		end

		SIa  : begin
			result.can_read  = 1'b0;
			result.can_write = 1'b0;
		end

		IIa  : begin
			result.can_read  = 1'b0;
			result.can_write = 1'b0;
		end

		default : begin
			result.can_read  = 1'b0;
			result.can_write = 1'b0;
		end


	endcase

	return result;

endfunction : state_2_privileges

module cc_protocol_rom (

		input  coherence_state_t    current_state,
		input  coherence_request_t  current_request,

		output protocol_rom_entry_t pr_output

	);
	
	coherence_states_enum_t current_state_enum;
	coherence_requests_enum_t current_request_enum;
	
	assign current_state_enum = coherence_states_enum_t'(current_state);
	assign current_request_enum = coherence_requests_enum_t'(current_request);
	
	always_comb begin

		pr_output.stall                  = 1'b0;
		pr_output.hit                    = 1'b0;
		pr_output.send_request           = 1'b0;
		pr_output.send_response          = 1'b0;
		pr_output.send_forward           = 1'b0;
		pr_output.request                = GETS;
		pr_output.response               = INV_ACK;
		pr_output.forward                = FWD_GETM;
		pr_output.send_data              = 1'b0;
		pr_output.is_receiver_dir        = 1'b0;
		pr_output.is_receiver_req        = 1'b0;
		pr_output.is_receiver_mc         = 1'b0;
		pr_output.incr_ack_count         = 1'b0;
		pr_output.next_state             = current_state;
		pr_output.next_state_is_stable   = 1'b0;

		pr_output.allocate_mshr_entry    = 1'b0;
		pr_output.deallocate_mshr_entry  = 1'b0;
		pr_output.update_mshr_entry      = 1'b0;

		pr_output.write_data_on_cache    = 1'b0;
		pr_output.send_data_from_cache   = 1'b0;
		pr_output.send_data_from_mshr    = 1'b0;
		pr_output.send_data_from_request = 1'b0;
		pr_output.req_has_data           = 1'b0;

		pr_output.req_has_ack_count      = current_request == Data_from_Dir_ack_gtz;
		pr_output.ack_count_eqz          = current_request == Data_from_Dir_ack_eqz;



		unique case ( {current_state, current_request} )

			{I, load}                     : begin
				pr_output.send_request           = 1'b1;
				pr_output.is_receiver_dir        = 1'b1;
				pr_output.request                = GETS;
				pr_output.next_state             = ISd;
				pr_output.allocate_mshr_entry    = 1'b1;
			end

			{I, store}                    : begin
				pr_output.send_request           = 1'b1;
				pr_output.is_receiver_dir        = 1'b1;
				pr_output.request                = GETM;
				pr_output.next_state             = IMad;
				pr_output.allocate_mshr_entry    = 1'b1;
			end

			{I, flush}                    : begin
				pr_output.send_request           = 1'b1;
				pr_output.is_receiver_dir        = 1'b1;
				pr_output.request                = DIR_FLUSH;
				pr_output.next_state             = I;
			end

			{I, load_uncoherent}          : begin
				pr_output.send_forward           = 1'b1;
				pr_output.forward                = FWD_GETM;
				pr_output.next_state             = IUd;
				pr_output.allocate_mshr_entry    = 1'b1;
			end

			{I, store_uncoherent}         : begin
				pr_output.send_forward           = 1'b1;
				pr_output.forward                = FWD_GETM;
				pr_output.next_state             = IUd;
				pr_output.allocate_mshr_entry    = 1'b1;
			end

			{ISd, load},
			{ISd, store},
			{ISd, replacement},
			{ISd, recall},
			{ISd, Inv}                    : begin
				pr_output.stall                  = 1'b1;
			end

			{ISd, Data_from_Dir_ack_eqz},
			{ISd, Data_from_Dir_ack_gtz}, //XXX: NB questa riga � differente dal protocollo del primer
			{ISd, Data_from_Owner}        : begin
				pr_output.next_state             = S;
				pr_output.next_state_is_stable   = 1'b1;
				pr_output.deallocate_mshr_entry  = 1'b1;
				pr_output.write_data_on_cache    = 1'b1;
				pr_output.req_has_data           = 1'b1;
				pr_output.ack_count_eqz          = 1'b1;
			end
			
			{IMad, flush},
			{IMad, load},
			{IMad, store},
			{IMad, replacement},
			{IMad, recall},
			{IMad, Fwd_GetS},
			{IMad, Fwd_GetM}              : begin
				pr_output.stall                  = 1'b1;
			end

			{IMad, Data_from_Dir_ack_eqz},
			{IMad, Data_from_Owner}       : begin
				pr_output.next_state             = M;
				pr_output.next_state_is_stable   = 1'b1;
				pr_output.deallocate_mshr_entry  = 1'b1;
				pr_output.write_data_on_cache    = 1'b1;
				pr_output.req_has_data           = 1'b1;
				pr_output.ack_count_eqz          = 1'b1;
			end

			{IMad, Data_from_Dir_ack_gtz} : begin
				pr_output.next_state             = IMa;
				pr_output.update_mshr_entry      = 1'b1;
				pr_output.req_has_data           = 1'b1;
			end

			{IMad, Inv_Ack}               : begin
				pr_output.incr_ack_count         = 1'b1;
				pr_output.update_mshr_entry      = 1'b1;
			end

			{IMad, Last_Inv_Ack}               : begin
				pr_output.next_state             = IMd;
				pr_output.incr_ack_count         = 1'b1;
				pr_output.update_mshr_entry      = 1'b1;
				pr_output.ack_count_eqz          = 1'b1;
			end
			
			{IMd, flush},
			{IMd, load},
			{IMd, store},
			{IMd, replacement},
			{IMd, recall},
			{IMd, Fwd_GetS},
			{IMd, Fwd_GetM}              : begin
				pr_output.stall                  = 1'b1;
			end

			{IMd, Data_from_Dir_ack_gtz},
			{IMd, Data_from_Dir_ack_eqz},
			{IMd, Data_from_Owner}       : begin
				pr_output.next_state             = M;
				pr_output.next_state_is_stable   = 1'b1;
				pr_output.deallocate_mshr_entry  = 1'b1;
				pr_output.write_data_on_cache    = 1'b1;
				pr_output.req_has_data           = 1'b1;
				pr_output.ack_count_eqz          = 1'b1;
			end

			{IUd, load_uncoherent},
			{IUd, store_uncoherent},
			{IUd, replacement_uncoherent},
			{IUd, flush_uncoherent}       : begin
				pr_output.stall                  = 1'b1;
			end

			{IUd, Data_from_Owner}        : begin
				pr_output.next_state             = U;
				pr_output.next_state_is_stable   = 1'b1;
				pr_output.deallocate_mshr_entry  = 1'b1;
				pr_output.write_data_on_cache    = 1'b1;
				pr_output.req_has_data           = 1'b1;
				pr_output.ack_count_eqz          = 1'b1;
			end

			{IMa, flush},
			{IMa, load},
			{IMa, store},
			{IMa, replacement},
			{IMa, recall},
			{IMa, Fwd_GetS},
			{IMa, Fwd_GetM}               : begin
				pr_output.stall                  = 1'b1;
			end

			{IMa, Inv_Ack}                : begin
				pr_output.incr_ack_count         = 1'b1;
				pr_output.update_mshr_entry      = 1'b1;
			end

			{IMa, Last_Inv_Ack}           : begin
				pr_output.next_state             = M;
				pr_output.next_state_is_stable   = 1'b1;
				pr_output.deallocate_mshr_entry  = 1'b1;
				pr_output.write_data_on_cache    = 1'b1;
			end

			{S, load}                     : begin
				pr_output.hit                    = 1'b1;
				pr_output.next_state_is_stable   = 1'b1;
			end

			{S, store}                    : begin
				pr_output.send_request           = 1'b1;
				pr_output.is_receiver_dir        = 1'b1;
				pr_output.request                = GETM;
				pr_output.next_state             = SMad;
				pr_output.allocate_mshr_entry    = 1'b1;
			end

			{S, replacement}              : begin
				pr_output.send_request           = 1'b1;
				pr_output.is_receiver_dir        = 1'b1;
				pr_output.request                = PUTS;
				pr_output.next_state             = SIa;
				pr_output.allocate_mshr_entry    = 1'b1;
			end

			{S, recall}                   : begin
				pr_output.next_state             = I;
				pr_output.next_state_is_stable   = 1'b1;
			end

			{S, Inv}                      : begin
				pr_output.send_response          = 1'b1;
				pr_output.is_receiver_req        = 1'b1;
				pr_output.response               = INV_ACK;
				pr_output.next_state             = I;
				pr_output.next_state_is_stable   = 1'b1;
			//  non alloca nessuna entry nel MSHR ma manda solo il messaggio in rete ed aggiorna i privilegi
			end

			{SMad, load}                  : begin
				pr_output.hit                    = 1'b1;
			end
			
			{SMad, flush},
			{SMad, store},
			{SMad, replacement},
			{SMad, recall},
			{SMad, Fwd_GetS},
			{SMad, Fwd_GetM}              : begin
				pr_output.stall                  = 1'b1;
			end

			{SMad, Inv}                   : begin
				pr_output.send_response          = 1'b1;
				pr_output.is_receiver_req        = 1'b1;
				pr_output.response               = INV_ACK;
				pr_output.next_state             = IMad;
				pr_output.update_mshr_entry      = 1'b1;
			end

			{SMad, Data_from_Dir_ack_eqz},
			{SMad, Data_from_Owner}       : begin
				pr_output.next_state             = M;
				pr_output.next_state_is_stable   = 1'b1;
				pr_output.deallocate_mshr_entry  = 1'b1;
			// pr_output.write_data_on_cache    = 1'b1; non serve, gi� � in cache
			end

			{SMad, Data_from_Dir_ack_gtz} : begin
				pr_output.next_state             = SMa;
				pr_output.update_mshr_entry      = 1'b1;
				pr_output.req_has_data           = 1'b1;
				// pr_output.write_data_on_cache    = 1'b1; non serve, gi� � in cache
			end

			{SMad, Inv_Ack}               : begin
				pr_output.incr_ack_count         = 1'b1;
				pr_output.update_mshr_entry      = 1'b1;
			end

			{SMa, load}                   : begin
				pr_output.hit                    = 1'b1;
			end
			
			{SMa, flush},
			{SMa, store},
			{SMa, replacement},
			{SMa, recall},
			{SMa, Fwd_GetS},
			{SMa, Fwd_GetM}               : begin
				pr_output.stall                  = 1'b1;
			end

			{SMa, Inv}                    : begin
				pr_output.send_response          = 1'b1;
				pr_output.is_receiver_req        = 1'b1;
				pr_output.response               = INV_ACK;
				pr_output.next_state             = IMa;
				pr_output.update_mshr_entry      = 1'b1;
			end

			{SMa, Inv_Ack}                : begin
				pr_output.incr_ack_count         = 1'b1;
				pr_output.update_mshr_entry      = 1'b1;
			end

			{SMa, Last_Inv_Ack}           : begin
				pr_output.next_state             = M;
				pr_output.next_state_is_stable   = 1'b1;
				pr_output.deallocate_mshr_entry  = 1'b1;
			// pr_output.write_data_on_cache    = 1'b1; non serve, gi� � in cache
			end

			{M, load},
			{M, store}                    : begin
				pr_output.hit                    = 1'b1;
				pr_output.next_state_is_stable   = 1'b1;
			end

			{M, replacement}              : begin
				pr_output.req_has_data           = 1'b1;
				pr_output.send_request           = 1'b1;
				pr_output.is_receiver_dir        = 1'b1;
				pr_output.send_data              = 1'b1;
				pr_output.request                = PUTM;
				pr_output.next_state             = MIa;
				pr_output.allocate_mshr_entry    = 1'b1;
				pr_output.send_data_from_request = 1'b1;
			end

			{M, recall}                   : begin
				pr_output.send_response          = 1'b1;
				pr_output.send_data              = 1'b1;
				pr_output.is_receiver_dir        = 1'b1;
				pr_output.is_receiver_mc         = 1'b1;
				pr_output.response               = WB;
				pr_output.next_state             = I;
				pr_output.next_state_is_stable   = 1'b1;
				pr_output.send_data_from_cache   = 1'b1;
			end

			{M, flush}                    : begin
				pr_output.send_response          = 1'b1;
				pr_output.send_data              = 1'b1;
				pr_output.is_receiver_mc         = 1'b1;
				pr_output.response               = WB;
				pr_output.send_data_from_cache   = 1'b1;
			end

			{M, Fwd_GetS}                 : begin
				pr_output.is_receiver_dir        = 1'b1;
				pr_output.is_receiver_req        = 1'b1;
				pr_output.send_data              = 1'b1;
				pr_output.next_state             = S;
				pr_output.next_state_is_stable   = 1'b1;
				pr_output.send_data_from_cache   = 1'b1;
				pr_output.send_response          = 1'b1;
				pr_output.response               = DATA;
			end

			{M, Fwd_GetM}                 : begin
				pr_output.send_response          = 1'b1;
				pr_output.is_receiver_req        = 1'b1;
				pr_output.send_data              = 1'b1;
				pr_output.next_state             = I;
				pr_output.next_state_is_stable   = 1'b1;
				pr_output.send_data_from_cache   = 1'b1;
				pr_output.response               = DATA;
			end

			{M, Put_Ack}                  : begin
				pr_output.next_state             = I;
				pr_output.next_state_is_stable   = 1'b1;
			end

			{U, load_uncoherent},
			{U, store_uncoherent}         : begin
				pr_output.hit                    = 1'b1;
				pr_output.next_state_is_stable   = 1'b1;
			end

			{U, replacement_uncoherent}   : begin
				pr_output.send_response          = 1'b1;
				pr_output.send_data              = 1'b1;
				pr_output.send_data_from_request = 1'b1;
				pr_output.is_receiver_mc         = 1'b1;
				pr_output.response               = WB;
				pr_output.next_state             = I;
				pr_output.next_state_is_stable   = 1'b1;
				pr_output.deallocate_mshr_entry  = 1'b1;
			end

			{U, flush_uncoherent}         : begin
				pr_output.send_response          = 1'b1;
				pr_output.send_data              = 1'b1;
				pr_output.is_receiver_mc         = 1'b1;
				pr_output.response               = WB;
				pr_output.send_data_from_cache   = 1'b1;
			end

			{MIa, load},
			{MIa, store} : begin
				pr_output.stall                  = 1'b1;
			end

			{MIa, replacement}            : begin
			    pr_output.req_has_data           = 1'b1;
				pr_output.stall                  = 1'b1;
			end

			{MIa, recall}                 : begin
				pr_output.send_response          = 1'b1;
				pr_output.send_data              = 1'b1;
				pr_output.is_receiver_dir        = 1'b1;
				pr_output.is_receiver_mc         = 1'b1;
				pr_output.response               = WB;
				pr_output.next_state             = I;
				pr_output.next_state_is_stable   = 1'b1;
				pr_output.send_data_from_mshr    = 1'b1;
				pr_output.deallocate_mshr_entry  = 1'b1;
			end

			{MIa, flush}                  : begin
				pr_output.send_response          = 1'b1;
				pr_output.send_data              = 1'b1;
				pr_output.is_receiver_mc         = 1'b1;
				pr_output.response               = WB;
				pr_output.send_data_from_mshr    = 1'b1;
			end

			{MIa, Fwd_GetS}               : begin
				pr_output.is_receiver_dir        = 1'b1;
				pr_output.is_receiver_req        = 1'b1;
				pr_output.send_data              = 1'b1;
				pr_output.send_data_from_mshr    = 1'b1;
			end

			{MIa, Fwd_GetM}               : begin
				pr_output.is_receiver_req        = 1'b1;
				pr_output.send_data              = 1'b1;
				pr_output.send_data_from_mshr    = 1'b1;
				pr_output.next_state             = IIa;
				pr_output.update_mshr_entry      = 1'b1;
			end

			{MIa, Put_Ack}                : begin
				pr_output.next_state             = I;
				pr_output.next_state_is_stable   = 1'b1;
				pr_output.deallocate_mshr_entry  = 1'b1;
			end

			{SIa, load},
			{SIa, store},
			{SIa, replacement}            : begin
				pr_output.stall                  = 1'b1;
			end

			{SIa, recall},
			{SIa, Put_Ack}                : begin
				pr_output.next_state             = I;
				pr_output.next_state_is_stable   = 1'b1;
				pr_output.deallocate_mshr_entry  = 1'b1;
			end

			{SIa, Inv}                    : begin
				pr_output.response               = INV_ACK;
				pr_output.is_receiver_req        = 1'b1;
				pr_output.next_state             = IIa;
				pr_output.update_mshr_entry      = 1'b1;
			end

			{IIa, load},
			{IIa, store},
			{IIa, recall},
			{IIa, replacement}            : begin
				pr_output.stall                  = 1'b1;
			end

			{IIa, Put_Ack}                : begin
				pr_output.next_state             = I;
				pr_output.next_state_is_stable   = 1'b1;
				pr_output.deallocate_mshr_entry  = 1'b1;
			end

			default : begin
				//$error("[CC - ROM]: Invalid state detected! Current State: %s,  Current Request: %s", 
				//	current_state_enum.name(), current_request_enum.name());
				pr_output.stall                  = 0;
				pr_output.next_state             = 0;
				pr_output.next_state_is_stable   = 0;
				pr_output.request                = 0;
				pr_output.response               = 0;
				pr_output.send_request           = 0;
				pr_output.send_response          = 0;
				pr_output.is_receiver_dir        = 0;
				pr_output.is_receiver_req        = 0;
				pr_output.send_data              = 0;
				pr_output.incr_ack_count         = 0;
				pr_output.update_privileges      = 0;
				pr_output.allocate_mshr_entry    = 0;
				pr_output.deallocate_mshr_entry  = 0;
				pr_output.update_mshr_entry      = 0;
				pr_output.write_data_on_cache    = 0;
				pr_output.send_data_from_cache   = 0;
				pr_output.send_data_from_mshr    = 0;
				pr_output.send_data_from_request = 0;
				pr_output.req_has_data           = 0;
				pr_output.req_has_ack_count      = 0;
			end

		endcase

		//FIXME: Cambiare la modalità di calcolo di update_privileges
		pr_output.next_privileges        = state_2_privileges( pr_output.next_state );
		pr_output.update_privileges      = state_2_privileges( current_state ) != pr_output.next_privileges;
	end

endmodule
/* verilator lint_on WIDTHCONCAT */

