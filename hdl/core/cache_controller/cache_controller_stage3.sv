`include "nuplus_user_defines.sv"
`include "nuplus_defines.sv"
`include "nuplus_coherence_defines.sv"
`include "nuplus_network_defines.sv"

/*
 * This is the most important file about the cache controller because contains the control logic that manages
 *  the cache controller modules and the load/store unit data path progresses.
 * 
 */

module cache_controller_stage3 (

		input                                            clk,
		input                                            reset,

		// From Cache Controller Stage 2

		input  logic                                     cc2_request_valid,
		input  coherence_request_t                       cc2_request,
		input  thread_id_t                               cc2_request_thread_id,
		input  dcache_address_t                          cc2_request_address,
		input  dcache_line_t                             cc2_request_data,
		input  tile_address_t                            cc2_request_source,
		input  sharer_count_t                            cc2_request_sharers_count,

		input  logic                                     cc2_request_mshr_hit,
		input  mshr_idx_t                                cc2_request_mshr_index,
		input  mshr_entry_t                              cc2_request_mshr_entry_info,
		input  dcache_line_t                             cc2_request_mshr_entry_data,
		input  mshr_idx_t                                cc2_request_mshr_empty_index,
		input  logic                                     cc2_request_mshr_full,

		input  dcache_way_idx_t                          cc2_request_lru_way_idx,
		input  dcache_way_idx_t                          cc2_request_snoop_way_idx,
		input  logic                                     cc2_request_snoop_hit,
		input  dcache_tag_t        [`DCACHE_WAY - 1 : 0] cc2_request_snoop_tag,
		input  dcache_privileges_t [`DCACHE_WAY - 1 : 0] cc2_request_snoop_privileges,
		input  coherence_state_t   [`DCACHE_WAY - 1 : 0] cc2_request_coherence_states,

		// To Load Store Unit

		output logic                                     cc3_update_ldst_valid,
		output cc_command_t                              cc3_update_ldst_command,
		output dcache_way_idx_t                          cc3_update_ldst_way,
		output dcache_address_t                          cc3_update_ldst_address,
		output dcache_privileges_t                       cc3_update_ldst_privileges,
		output dcache_line_t                             cc3_update_ldst_store_value,

		output logic                                     cc3_snoop_data_valid,
		output dcache_set_t                              cc3_snoop_data_set,
		output dcache_way_idx_t                          cc3_snoop_data_way,

		output logic                                     cc3_wakeup,
		output thread_id_t                               cc3_wakeup_thread_id,


		// To Cache Controller Stage 1

		output logic                                     cc3_pending_valid,
		output dcache_address_t                          cc3_pending_address,

		// To Cache Controller Stage 2

		output logic                                     cc3_update_mshr_en,
		output mshr_idx_t                                cc3_update_mshr_index,
		output mshr_entry_t                              cc3_update_mshr_entry_info,
		output dcache_line_t                             cc3_update_mshr_entry_data,

		output logic                                     cc3_update_coherence_state_en,
		output dcache_set_t                              cc3_update_coherence_state_index,
		output dcache_way_idx_t                          cc3_update_coherence_state_way,
		output coherence_state_t                         cc3_update_coherence_state_entry,

		output logic                                     cc3_update_lru_fill_en,

		// To Cache Controller Stage 4

		output logic                                     cc3_request_is_flush,

		output logic                                     cc3_message_valid,
		output logic                                     cc3_message_is_response,
		output logic                                     cc3_message_is_forward,
		output message_request_t                         cc3_message_request_type,
		output message_response_t                        cc3_message_response_type,
		output message_forwarded_request_t               cc3_message_forwarded_request_type,
		output dcache_address_t                          cc3_message_address,
		output dcache_line_t                             cc3_message_data,
		output logic                                     cc3_message_has_data,
		output logic                                     cc3_message_send_data_from_cache,
		output logic                                     cc3_message_is_receiver_dir,
		output logic                                     cc3_message_is_receiver_req,
		output logic                                     cc3_message_is_receiver_mc,
		output tile_address_t                            cc3_message_requestor

	);

//  -----------------------------------------------------------------------
//  -- Cache Controller Stage 3 - Signals declaration
//  -----------------------------------------------------------------------

	coherence_state_t    current_state;
	logic                do_replacement;
	logic                replaced_way_valid;
	dcache_address_t     replaced_way_address;
	coherence_state_t    replaced_way_state;
	protocol_rom_entry_t pr_output;
	
//  -----------------------------------------------------------------------
//  -- Cache Controller Stage 3 - State selector
//  -----------------------------------------------------------------------

	// If there is a MSHR hit, it means that the request is already pending, and the
	// most up to date state is stored in MSHR. If there is no entry in MSHR and there is a Snoop hit,
	// it means that the state is stored in Coherence State SRAM. If there is neither a MSHR hit nor a
	// Snoop hit, it means that the line is in Invalid state.

	always_comb begin : current_state_selector
		if ( cc2_request_mshr_hit )
			current_state = cc2_request_mshr_entry_info.state;
		else if ( cc2_request_snoop_hit )
			current_state = cc2_request_coherence_states[cc2_request_snoop_way_idx];
		else
			current_state = I;
	end

//  -----------------------------------------------------------------------
//  -- Cache Controller Stage 3 - Protocol ROM instantiation
//  -----------------------------------------------------------------------

	// This module handles the control unit behavior. The CU has elementary action defined in
	// this ROM. It takes in input the current state and the request and decodes the next
	// actions.

	cc_protocol_rom u_cc_protocol_rom (
		.current_state  ( current_state ),
		.current_request( cc2_request   ),
		.pr_output      ( pr_output     )
	);

//  -----------------------------------------------------------------------
//  -- Cache Controller Stage 3 - Replacement valutation
//  -----------------------------------------------------------------------
	// Valutiamo se la way uscente (cc2_request_lru_way_idx) ha un dato valido o meno, la valutazione � fatta sui privilegi. Ricalcoliamo
	// l'address originale concatenando il tag all'index (l'index della way entrante e della way uscente sono uguali),
	// in modo da poterlo usare in caso di replacement

	always_comb begin
		replaced_way_valid          = cc2_request_snoop_privileges[cc2_request_lru_way_idx].can_read | cc2_request_snoop_privileges[cc2_request_lru_way_idx].can_write;

		replaced_way_address.tag    = cc2_request_snoop_tag[cc2_request_lru_way_idx];
		replaced_way_address.index  = cc2_request_address.index;
		replaced_way_address.offset = {`DCACHE_OFFSET_LENGTH{1'b0}};

		replaced_way_state          = cc2_request_coherence_states[cc2_request_lru_way_idx];

		do_replacement              = pr_output.write_data_on_cache && !cc2_request_snoop_hit && replaced_way_valid;
	end

//  -----------------------------------------------------------------------
//  -- Cache Controller Stage 3 - MSHR info update calculation
//  -----------------------------------------------------------------------

	always_comb begin

		if ( do_replacement ) begin
			cc3_update_mshr_entry_info.valid                = 1'b1;
			cc3_update_mshr_entry_info.address              = replaced_way_address;
			cc3_update_mshr_entry_info.state                = replaced_way_state;
			cc3_update_mshr_entry_info.thread_id            = 0;					
			cc3_update_mshr_entry_info.ack_count            = 0;
			cc3_update_mshr_entry_info.waiting_for_eviction = 1'b1;
			cc3_update_mshr_entry_info.wakeup_thread        = 1'b0;
			cc3_update_mshr_entry_info.ack_count_received   = 1'b0;
			cc3_update_mshr_entry_info.inv_ack_received     = 0;
		end else begin
			cc3_update_mshr_entry_info.valid                = ( pr_output.allocate_mshr_entry || pr_output.update_mshr_entry ) && !pr_output.deallocate_mshr_entry;
			cc3_update_mshr_entry_info.address              = cc2_request_address;
			cc3_update_mshr_entry_info.state                = pr_output.next_state;
			cc3_update_mshr_entry_info.thread_id            = cc2_request_mshr_hit ? cc2_request_mshr_entry_info.thread_id         : cc2_request_thread_id;
			//cc3_update_mshr_entry_info.ack_count            = pr_output.decr_ack_count ? cc2_request_mshr_entry_info.ack_count - 1 : ( pr_output.req_has_ack_count ? cc2_request_sharers_count : cc2_request_mshr_entry_info.ack_count );
			cc3_update_mshr_entry_info.ack_count            =  pr_output.req_has_ack_count ? cc2_request_sharers_count : cc2_request_mshr_entry_info.ack_count;
			cc3_update_mshr_entry_info.waiting_for_eviction = 1'b0;
			cc3_update_mshr_entry_info.wakeup_thread        = cc2_request_mshr_hit ? cc2_request_mshr_entry_info.wakeup_thread     : ( cc2_request == load || cc2_request == store || cc2_request == replacement || cc2_request == load_uncoherent || cc2_request == store_uncoherent || cc2_request == replacement_uncoherent );
			cc3_update_mshr_entry_info.ack_count_received   = ( pr_output.req_has_ack_count ? cc2_request_sharers_count : cc2_request_mshr_entry_info.ack_count_received );
			cc3_update_mshr_entry_info.inv_ack_received     = pr_output.incr_ack_count ? (cc2_request_mshr_entry_info.inv_ack_received + 1) : cc2_request_mshr_entry_info.inv_ack_received;
		end

		cc3_update_mshr_entry_data = pr_output.req_has_data ? cc2_request_data     : cc2_request_mshr_entry_data;

		// In caso di preallocazione la ROM dirà di allocare una entry nel MSHR, ma questa � stata
		// gi� preallocata. In caso di replacement quindi dobbiamo utilizzare il way passato dallo
		// stadio precedente.
		cc3_update_mshr_index      = cc2_request_mshr_hit ? cc2_request_mshr_index : cc2_request_mshr_empty_index; //FIXME: Non va bene se si rilassa il vincolo del set.

	end

//  -----------------------------------------------------------------------
//  -- Cache Controller Stage 3 - LDST information & data update
//  -----------------------------------------------------------------------

	assign cc3_update_ldst_command          = do_replacement ? CC_REPLACEMENT                                          : ( pr_output.write_data_on_cache ? CC_UPDATE_INFO_DATA : CC_UPDATE_INFO );
	assign cc3_update_ldst_way              = cc2_request_snoop_hit ? cc2_request_snoop_way_idx                        : cc2_request_lru_way_idx;
	assign cc3_update_ldst_address          = cc2_request_address;
	assign cc3_update_ldst_store_value      = ( pr_output.ack_count_eqz && pr_output.req_has_data ) ? cc2_request_data : cc2_request_mshr_entry_data;
	assign cc3_update_ldst_privileges       = pr_output.next_privileges;
	assign cc3_wakeup_thread_id             = (cc2_request_mshr_hit &&  pr_output.deallocate_mshr_entry) ? cc2_request_mshr_entry_info.thread_id             : cc2_request_thread_id;
	
	assign cc3_update_coherence_state_index = cc2_request_address.index;
	assign cc3_update_coherence_state_way   = cc2_request_snoop_hit ? cc2_request_snoop_way_idx                        : cc2_request_lru_way_idx;
	assign cc3_update_coherence_state_entry = pr_output.next_state;
	
	assign cc3_pending_valid                = cc3_update_mshr_en;
	assign cc3_pending_address              = cc3_update_mshr_entry_info.address;

	assign cc3_snoop_data_valid             = cc2_request_valid && pr_output.send_data_from_cache;
	assign cc3_snoop_data_set               = cc2_request_address.index;
	assign cc3_snoop_data_way               = cc2_request_snoop_way_idx;


//  -----------------------------------------------------------------------
//  -- Cache Controller Stage 3 - Enable signals assignment
//  -----------------------------------------------------------------------
	always_comb begin
		if ( cc2_request_valid ) begin
			// Aggiorniamo i privilegi solo nel caso di snoop hit. Senza questo controllo andiamo
			// a sovrascrivere i privilegi di una nuova way che ha innescato un replacement.
			cc3_update_ldst_valid         = ( pr_output.update_privileges && cc2_request_snoop_hit ) || pr_output.write_data_on_cache;
			cc3_wakeup                    = pr_output.hit /*cc2_request_snoop_hit*/ || ( pr_output.deallocate_mshr_entry && cc2_request_mshr_entry_info.wakeup_thread );
			cc3_update_mshr_en            = ( pr_output.allocate_mshr_entry || pr_output.update_mshr_entry || pr_output.deallocate_mshr_entry );
			// L'aggiornamento di coherence_state non pu� avvenire su PUTACK poich� la condizione pr_next_state_is_stable && pr_write_data_on_cache non � verificata.
			cc3_update_coherence_state_en = ( pr_output.next_state_is_stable && cc2_request_snoop_hit ) || ( pr_output.next_state_is_stable && !cc2_request_snoop_hit && pr_output.write_data_on_cache );
		end else begin
			cc3_update_ldst_valid         = 1'b0;
			cc3_wakeup                    = 1'b0;
			cc3_update_mshr_en            = 1'b0;
			cc3_update_coherence_state_en = 1'b0;
		end
	end

//  -----------------------------------------------------------------------
//  -- Cache Controller Stage 3 - Output registration
//  -----------------------------------------------------------------------
	always_ff @( posedge clk ) begin
		cc3_message_is_response            <= pr_output.send_response;
		cc3_message_is_forward             <= pr_output.send_forward;
		cc3_message_request_type           <= pr_output.request;
		cc3_message_response_type          <= pr_output.response;
		cc3_message_forwarded_request_type <= pr_output.forward;
		cc3_message_address                <= cc2_request_address;
		cc3_message_requestor              <= cc2_request_source;
		cc3_message_is_receiver_dir        <= pr_output.is_receiver_dir;
		cc3_message_is_receiver_req        <= pr_output.is_receiver_req;
		cc3_message_is_receiver_mc         <= pr_output.is_receiver_mc;
		cc3_message_send_data_from_cache   <= pr_output.send_data_from_cache;
		cc3_message_has_data               <= pr_output.send_data_from_cache || pr_output.send_data_from_mshr || pr_output.send_data_from_request;
		cc3_message_data                   <= pr_output.send_data_from_mshr ? cc2_request_mshr_entry_data : cc2_request_data;
	end

	always_ff @( posedge clk, posedge reset )
		if ( reset ) begin
			cc3_message_valid    <= 1'b0;
			cc3_request_is_flush <= 1'b0;
		end else begin
			cc3_message_valid    <= cc2_request_valid && ( pr_output.send_response || pr_output.send_request || pr_output.send_forward );
			cc3_request_is_flush <= cc2_request == flush;
		end
		
endmodule
