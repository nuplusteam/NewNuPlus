`include "nuplus_coherence_defines.sv"

module mshr_cc #(
        WRITE_FIRST = "FALSE"
    )(
        input  logic                                     clk,
        input  logic                                     enable,
        input  logic                                     reset,

        input  dcache_tag_t [`MSHR_LOOKUP_PORTS - 1 : 0] lookup_tag,
        input  dcache_set_t [`MSHR_LOOKUP_PORTS - 1 : 0] lookup_set,
        output logic        [`MSHR_LOOKUP_PORTS - 1 : 0] lookup_hit,
        output logic        [`MSHR_LOOKUP_PORTS - 1 : 0] lookup_hit_set,
        output mshr_idx_t   [`MSHR_LOOKUP_PORTS - 1 : 0] lookup_index,
        output mshr_entry_t [`MSHR_LOOKUP_PORTS - 1 : 0] lookup_entry,

        output logic                                     full,
        output mshr_idx_t                                empty_index,

        input  logic                                     update_en,
        input  mshr_idx_t                                update_index,
        input  mshr_entry_t                              update_entry
    );

    mshr_entry_t [`MSHR_SIZE - 1 : 0] data;
    mshr_entry_t [`MSHR_SIZE - 1 : 0] data_updated;
    logic        [`MSHR_SIZE - 1 : 0] empty_oh;
    logic                             is_not_full;

//  --------------------------------------
//  -- MSHR write port
//  --------------------------------------

    generate
        genvar mshr_id;
        for ( mshr_id = 0; mshr_id < `MSHR_SIZE; mshr_id++ ) begin : mshr_entries

            logic update_this_index;
            assign update_this_index = update_en & (update_index == mshr_idx_t'(mshr_id));

            if (WRITE_FIRST == "TRUE")
                assign data_updated[mshr_id] = (enable && update_this_index) ? update_entry : data[mshr_id];
            else
                assign data_updated[mshr_id] = data[mshr_id];

            assign empty_oh[mshr_id] = ~(data_updated[mshr_id].valid);

						always_ff @(posedge clk, posedge reset) begin
							if (reset) begin
								data[mshr_id] <= 0;
							end else if (enable && update_this_index) begin
								if (data[mshr_id].valid == 1 && data[mshr_id].address != update_entry.address && update_entry.waiting_for_eviction == 0)
									$fatal("MSHR entry overwritten!");

								data[mshr_id] <= update_entry;
							end
						end
        end
    endgenerate

//  --------------------------------------
//  -- MSHR lookup read port
//  --------------------------------------
    generate
        genvar lookup_port_idx;
        for (lookup_port_idx = 0; lookup_port_idx < `MSHR_LOOKUP_PORTS; lookup_port_idx++) begin :lookup_ports

            mshr_cc_lookup_unit lookup_unit (
                .tag          ( lookup_tag[lookup_port_idx]     ),
                .index        ( lookup_set[lookup_port_idx]     ),
                .mshr_entries ( data_updated                    ),
                .hit          ( lookup_hit[lookup_port_idx]     ),
                .hit_set      ( lookup_hit_set[lookup_port_idx] ),
                .mshr_index   ( lookup_index[lookup_port_idx]   ),
                .mshr_entry   ( lookup_entry[lookup_port_idx]   )
            );

        end
    endgenerate

//  --------------------------------------
//  -- MSHR empty index generation
//  --------------------------------------

    priority_encoder_nuplus #(
        .INPUT_WIDTH (`MSHR_SIZE ),
        .MAX_PRIORITY("LSB"      )
    )
    u_priority_encoder (
        .decode(empty_oh   ),
        .encode(empty_index),
        .valid (is_not_full)
    );

    assign full = !is_not_full;


endmodule

module mshr_cc_lookup_unit (
        input  dcache_tag_t                      tag,
        input  dcache_set_t                      index,
        input  mshr_entry_t [`MSHR_SIZE - 1 : 0] mshr_entries,

        output logic                             hit,
        output logic                             hit_set,
        output mshr_idx_t                        mshr_index,
        output mshr_entry_t                      mshr_entry
    );

    logic      [`MSHR_SIZE - 1 : 0] hit_map;
    logic      [`MSHR_SIZE - 1 : 0] hit_set_map;
    mshr_idx_t                      hit_index;
    mshr_idx_t                      hit_set_index;

    genvar                          i;
    generate
        for ( i = 0; i < `MSHR_SIZE; i++ ) begin : lookup_logic
            assign hit_set_map[i] = ( mshr_entries[i].address.index == index ) && mshr_entries[i].valid;
            assign hit_map[i]     = ( mshr_entries[i].address.tag == tag ) && hit_set_map[i];
        end
    endgenerate


    oh_to_idx #(
        .NUM_SIGNALS( `MSHR_SIZE ),
        .DIRECTION  ( "LSB0"     )
    )
    u_hit_oh_to_idx (
        .index  ( hit_index ),
        .one_hot( hit_map   )
    );

    oh_to_idx #(
        .NUM_SIGNALS( `MSHR_SIZE ),
        .DIRECTION  ( "LSB0"     )
    )
    u_hit_set_oh_to_idx (
        .index  ( hit_set_index ),
        .one_hot( hit_set_map   )
    );

    assign mshr_entry = hit ? mshr_entries[hit_index] : mshr_entries[hit_set_index];
    assign hit        = |hit_map;
    assign hit_set    = |hit_set_map;
    assign mshr_index = hit ? hit_index : hit_set_index;
    
endmodule
