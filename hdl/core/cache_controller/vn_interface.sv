`include "nuplus_defines.sv"
`include "nuplus_coherence_defines.sv"

module vn_interface (
        input                                                       clk,
		input                                                       reset,

		// From Network Interface
		input  coherence_forwarded_message_t                        ni_forwarded_request,
		input  logic                                                ni_forwarded_request_valid,
		input  coherence_response_message_t                         ni_response,
		input  logic                                                ni_response_valid,

		// To Network Interface
		output logic                                                vn_forwarded_request_consumed,
		output logic                                                vn_response_consumed,

        // To L1D
        output logic                                                vn_forwarded_valid,
        output coherence_forwarded_message_t                        vn_forwarded_request,
        output logic                                                vn_response_valid,
        output coherence_response_message_t                         vn_response_request,

        // From L1D
        input logic                                                 l1d_forwarded_request_consumed,
		input logic                                                 l1d_response_consumed
	);

    logic response_empty;
    logic forward_empty;
    logic response_full;
    logic forward_full;
    logic response_en;
    logic forward_en;

	
    assign response_en = ~response_full & ni_response_valid;
    assign forward_en  = ~forward_full  & ni_forwarded_request_valid;

    assign vn_forwarded_valid = ~forward_empty;
    assign vn_response_valid  = ~response_empty;

    assign vn_forwarded_request_consumed = forward_en;
    assign vn_response_consumed          = response_en;

    sync_fifo #(
		.WIDTH ( $bits(coherence_response_message_t) ),
		.SIZE  ( 2 ),
		.ALMOST_FULL_THRESHOLD ( 1 )
	)
	response_fifo
	(
		.almost_empty( ),
		.almost_full ( response_full ),
		.clk         ( clk ),
		.dequeue_en  ( l1d_response_consumed ),
		.empty       ( response_empty ),
		.enqueue_en  ( response_en ),
		.flush_en    ( 1'b0 ),
		.full        ( ),
		.reset       ( reset ),
		.value_i     ( ni_response ),
		.value_o     ( vn_response_request )
	);

    sync_fifo #(
		.WIDTH ( $bits(coherence_forwarded_message_t) ),
		.SIZE  ( 2 ),
		.ALMOST_FULL_THRESHOLD ( 1 )
	)
	forward_fifo
	(
		.almost_empty( ),
		.almost_full ( forward_full ),
		.clk         ( clk ),
		.dequeue_en  ( l1d_forwarded_request_consumed ),
		.empty       ( forward_empty ),
		.enqueue_en  ( forward_en ),
		.flush_en    ( 1'b0 ),
		.full        ( ),
		.reset       ( reset ),
		.value_i     ( ni_forwarded_request ),
		.value_o     ( vn_forwarded_request )
	);
    
endmodule