`include "nuplus_defines.sv"
`include "nuplus_user_defines.sv"
`include "nuplus_coherence_defines.sv"

/*
 * This module receives from the LDST unit all the event concerned to the memory:
 *  - miss: a load/store operation doesn't find the data inside the L1 cache
 *  - evict: a cache line has to be deleted in favor of another fresh cache line
 *  - flush: the user wants to move some cache line to the memory (the line is NOT invalidated)
 * It basically manages four queues (load, store, evict, flush) which accept the requests from the LDST unit.
 * The cache controller sends the proper signals to dequeue the requests when it is ready to manage them.
 * Each request is a triple: {valid, threadID, address} (the flush event does not contain the threadID,
 * the evict event adds the data cache line).
 */

module core_interface (
		input                        clk,
		input                        reset,

		// Load Store Unit
		input  instruction_decoded_t ldst_instruction,
		input  dcache_address_t      ldst_address,
		input  logic                 ldst_miss,
		input  logic                 ldst_evict,
		input  dcache_line_t         ldst_cache_line,
		input  logic                 ldst_flush,

		// Cache Controller
		input  logic                 cc_dequeue_store_request,
		input  logic                 cc_dequeue_load_request,
		input  logic                 cc_dequeue_replacement_request,
		input  logic                 cc_dequeue_flush_request,
		output logic                 ci_store_request_valid,
		output thread_id_t           ci_store_request_thread_id,
		output dcache_address_t      ci_store_request_address,
		output logic                 ci_store_request_coherent,
		output logic                 ci_load_request_valid,
		output thread_id_t           ci_load_request_thread_id,
		output dcache_address_t      ci_load_request_address,
		output logic                 ci_load_request_coherent,
		output logic                 ci_replacement_request_valid,
		output thread_id_t           ci_replacement_request_thread_id,
		output dcache_address_t      ci_replacement_request_address,
		output dcache_line_t         ci_replacement_request_cache_line,
		output logic                 ci_flush_request_valid,
		output dcache_address_t      ci_flush_request_address,
		output logic                 ci_flush_request_coherent,
		output logic                 ci_flush_fifo_available
	);

	localparam FLUSH_QUEUE_WIDTH                              = $bits(dcache_address_t) + 1; // coherence bit attached
	localparam EVICT_QUEUE_WIDTH                              = $bits(thread_id_t) + $bits(dcache_address_t) + $bits(dcache_line_t);
	localparam WIDTH                                          = $bits(thread_id_t) + $bits(dcache_address_t) + 1; // coherence bit attached
	localparam SIZE                                           = `THREAD_NUMB;


	logic      flush_fifo_almost_full;
	assign     ci_flush_fifo_available = ~flush_fifo_almost_full;

	logic      rplcq_empty, lmq_empty, smq_empty, flush_empty;

	assign ci_load_request_valid         = !lmq_empty,
			ci_store_request_valid       = !smq_empty,
			ci_replacement_request_valid = !rplcq_empty,
			ci_flush_request_valid       = !flush_empty;

	sync_fifo #(
		.WIDTH ( WIDTH ),
		.SIZE  ( SIZE  )
	)
	store_miss_queue
	(
		.almost_empty(                                                                                        ),
		.almost_full (                                                                                        ),
		.clk         ( clk                                                                                    ),
		.dequeue_en  ( cc_dequeue_store_request                                                               ),
		.empty       ( smq_empty                                                                              ),
		.enqueue_en  ( ldst_miss && !ldst_instruction.is_load                                                 ),
		.flush_en    ( 1'b0                                                                                   ),
		.full        (                                                                                        ),
		.reset       ( reset                                                                                  ),
		.value_i     ( {ldst_instruction.is_memory_access_coherent, ldst_address, ldst_instruction.thread_id} ),
		.value_o     ( {ci_store_request_coherent, ci_store_request_address, ci_store_request_thread_id}      )
	);

	sync_fifo #(
		.WIDTH ( WIDTH ),
		.SIZE  ( SIZE  )
	)
	load_miss_queue
	(
		.almost_empty(                                                                                        ),
		.almost_full (                                                                                        ),
		.clk         ( clk                                                                                    ),
		.dequeue_en  ( cc_dequeue_load_request                                                                ),
		.empty       ( lmq_empty                                                                              ),
		.enqueue_en  ( ldst_miss && ldst_instruction.is_load                                                  ),
		.flush_en    ( 1'b0                                                                                   ),
		.full        (                                                                                        ),
		.reset       ( reset                                                                                  ),
		.value_i     ( {ldst_instruction.is_memory_access_coherent, ldst_address, ldst_instruction.thread_id} ),
		.value_o     ( {ci_load_request_coherent, ci_load_request_address, ci_load_request_thread_id}         )
	);

	sync_fifo #(
		.WIDTH ( EVICT_QUEUE_WIDTH ),
		.SIZE  ( SIZE              )
	)
	replacement_queue
	(
		.almost_empty(                                                                                                       ),
		.almost_full (                                                                                                       ),
		.clk         ( clk                                                                                                   ),
		.dequeue_en  ( cc_dequeue_replacement_request                                                                        ),
		.empty       ( rplcq_empty                                                                                           ),
		.enqueue_en  ( ldst_evict                                                                                            ),
		.flush_en    ( 1'b0                                                                                                  ),
		.full        (                                                                                                       ),
		.reset       ( reset                                                                                                 ),
		.value_i     ( {ldst_address, ldst_instruction.thread_id, ldst_cache_line}                                           ),
		.value_o     ( {ci_replacement_request_address, ci_replacement_request_thread_id, ci_replacement_request_cache_line} )
	);

	sync_fifo #(
		.WIDTH                 ( FLUSH_QUEUE_WIDTH ),
		.SIZE                  ( SIZE              ),
		.ALMOST_FULL_THRESHOLD ( SIZE - 3          )
	)
	flush_queue
	(
		.almost_empty(                                                            ),
		.almost_full ( flush_fifo_almost_full                                     ),
		.clk         ( clk                                                        ),
		.dequeue_en  ( cc_dequeue_flush_request                                   ),
		.empty       ( flush_empty                                                ),
		.enqueue_en  ( ldst_flush                                                 ),
		.flush_en    ( 1'b0                                                       ),
		.full        (                                                            ),
		.reset       ( reset                                                      ),
		.value_i     ( {ldst_instruction.is_memory_access_coherent, ldst_address} ),
		.value_o     ( {ci_flush_request_coherent, ci_flush_request_address}      )
	);

endmodule
