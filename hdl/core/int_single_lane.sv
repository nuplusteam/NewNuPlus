// TODO FARE LA DEFINITIVA
// E' SOLO UN DUMMY
`include "nuplus_defines.sv"

/*
 * This is one of the lane inside the execution int pipe.
 */
module int_single_lane (
		input  register_t op0,
		input  register_t op1,
		input  opcode_t   op_code,

		output register_t result
	);

	int                                      op1_signed;
	int                                      op2_signed;
	logic [`REGISTER_SIZE * 2 - 1 : 0]       aritm_shift;
	logic [`REGISTER_SIZE * 2 - 1 : 0]       mult_res;
	logic                                    is_greater_uns;
	logic                                    is_equal;
	logic                                    is_greater;

	assign op1_signed     = int'( op0 );
	assign op2_signed     = int'( op1 );
	assign mult_res       = op1_signed * op2_signed;

	assign is_equal       = op1_signed == op2_signed;
	assign is_greater     = op1_signed > op2_signed;
	assign is_greater_uns = op0 > op1;
	assign aritm_shift    = {{32{op0[31]}}, op0};

	logic                                    is_not_null;
	logic [$clog2( `REGISTER_SIZE ) - 1 : 0] encode;
	logic [`REGISTER_SIZE - 1 : 0]           clz_ctz_in, inverted_op0;
	always_comb begin
		for ( int i=0; i < `REGISTER_SIZE; i = i + 1 )
			inverted_op0[i] = op0[`REGISTER_SIZE - i - 1];
		clz_ctz_in = ( op_code == CLZ ) ? op0 : inverted_op0;
	end

	priority_encoder_nuplus #(
		.INPUT_WIDTH ( `REGISTER_SIZE ),
		.MAX_PRIORITY( "MSB"          )
	)
	u_priority_encoder_nuplus (
		.decode( clz_ctz_in  ),
		.encode( encode      ),
		.valid ( is_not_null )
	);

	always_comb begin
		case ( op_code )
			ADD : result     = op0 + op1;
			SUB : result     = op0 - op1;
			MOVE : result    = op0;
			GETLANE : result = op0;
			ASHR : result    = ( aritm_shift >> op1 );
			SHR : result     = ( op0 >> op1 );
			SHL : result     = ( op0 << op1 );
			CLZ,
			CTZ : result     = is_not_null ? `REGISTER_SIZE - encode - 1 : `REGISTER_SIZE;
			MULHI : result   = mult_res[`REGISTER_SIZE*2 - 1 : `REGISTER_SIZE];
			MULLO : result   = mult_res[`REGISTER_SIZE - 1 : 0               ];
			NOT : result     = ~op0;
			OR : result      = op0 | op1;
			AND : result     = op0 & op1;
			XOR : result     = op0 ^ op1;

			CMPEQ : result   = register_t'( is_equal );
			CMPNE : result   = register_t'( ~is_equal );
			CMPGT : result   = register_t'( is_greater );
			CMPGE : result   = register_t'( is_greater | is_equal );
			CMPLE : result   = register_t'( ~is_greater | is_equal );
			CMPLT : result   = register_t'( ~is_greater & ~is_equal );
			CMPGT_U : result = register_t'( is_greater_uns );
			CMPGE_U : result = register_t'( is_greater_uns | is_equal );
			CMPLT_U : result = register_t'( ~is_greater_uns & ~is_equal );
			CMPLE_U : result = register_t'( ~is_greater_uns | is_equal );

			SEXT8 : result   = {{24{op0[7]}}, op0[7 : 0]};
			SEXT16 : result  = {{16{op0[15]}}, op0[15 : 0]};
			SEXT32 : result  = op0;

			default :
			`ifdef SIMULATION
				result = {`REGISTER_SIZE{1'bx}};
			`else
			result     = {`REGISTER_SIZE{1'b0}};
			`endif
		endcase
	end

endmodule