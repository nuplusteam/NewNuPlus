`include "nuplus_user_defines.sv"
`include "nuplus_defines.sv"
`include "nuplus_message_service_defines.sv"
`include "nuplus_network_defines.sv"

`ifdef DISPLAY_REQUESTS_MANAGER
`include "nuplus_debug_log.sv"
`endif

module host_request_manager # (
		parameter TILE_ID = 0,
		parameter ITEM_w  = 32 	)
	(
		input clk,
		input reset,
		//input                                       enable,

		output logic [3 : 0] leds_state,

		// Interface to Host
		input        [ITEM_w - 1 : 0] item_data_i,  // Input: items from outside
		input                         item_valid_i, // Input: valid signal associated with item_data_i port
		output logic                  item_avail_o, // Output: avail signal to input port item_data_i
		output logic [ITEM_w - 1 : 0] item_data_o,  // Output: items to outside
		output logic                  item_valid_o, // Output: valid signal associated with item_data_o port
		input                         item_avail_i, // Input: avail signal to ouput port item_data_o

		// To host interface component
		output logic wait_boot_signal,
		output logic valid_arbiter,

		//Network Interface
		output service_c2n_message_t c2n_mes_service,
		output logic                 c2n_mes_valid,
		output logic                 c2n_has_data,
		input  service_c2n_message_t n2c_mes_service,
		input  logic                 n2c_mes_valid,
		output logic                 n2c_mes_service_consumed,
		input  logic                 ni_network_available,
		output dest_valid_t          c2n_mes_service_destinations_valid
	) ;

	interface_state_t next_state, state_next_control_supp, wait_net_support_state;

	host_messages_t                                  message_host_in, message_host_out;
	logic                                            has_data;
	boot_message_t                                   boot_message_to_net, message_from_net;
	logic           [$clog2 ( `TILE_COUNT ) - 1 : 0] message_to_net_destinations_valid;
	dest_valid_t                                     one_hot_destination_valid;

	//Message from host
	assign message_host_in = host_messages_t' ( item_data_i ) ;

	//Message to Network
	assign c2n_mes_service.message_type = BOOT;
	assign c2n_mes_service.data         = message_data_t' ( boot_message_to_net ) ;

	//Message from Network
	assign message_from_net = boot_message_t' ( n2c_mes_service.data ) ;

	assign c2n_mes_service_destinations_valid = one_hot_destination_valid;

	assign item_data_o = ITEM_w'( message_host_out );

//  -----------------------------------------------------------------------------------------------
//  -- Useful to set the has_data bit in the network to send a packet bigger than one flit (64 bit)
//  -----------------------------------------------------------------------------------------------
	generate
		if ( $bits ( service_c2n_message_t ) <= `PAYLOAD_W )
			assign has_data = 1'b0;
		else
			assign has_data = 1'b1;
	endgenerate

	always_ff @ ( posedge clk, posedge reset ) begin
		if ( reset ) begin
			next_state              <= START;
			wait_net_support_state  <= START;
			state_next_control_supp <= START;

			wait_boot_signal <= 1'b0;
			valid_arbiter    <= 1'b0;
			item_avail_o     <= 1'b0;
			item_valid_o     <= 1'b0;

			c2n_mes_valid <= 1'b0;
			c2n_has_data  <= 1'b0;

			n2c_mes_service_consumed <= 1'b0;

			message_host_out <= host_messages_t' ( 1'b0 ) ;

			leds_state <= 4'b0000;

		end else begin

			wait_boot_signal <= 1'b1;
			valid_arbiter    <= 1'b1;
			item_avail_o     <= 1'b0;
			item_valid_o     <= 1'b0;

			c2n_mes_valid            <= 1'b0;
			c2n_has_data             <= has_data;
			n2c_mes_service_consumed <= 1'b0;

			unique case ( next_state )

				START : begin
					leds_state       <= 4'b0001;
					wait_boot_signal <= 1'b0;
					valid_arbiter    <= 1'b0;
					next_state       <= IDLE;

				end

				IDLE : begin
					leds_state   <= 4'b0010;
					item_avail_o <= 1'b1;

					if ( item_valid_i ) begin
						if ( message_host_in == BOOT_COMMAND ) begin
							next_state              <= WAIT_DESTINATION;
							state_next_control_supp <= WAIT_THREAD_ID;
							wait_net_support_state  <= WAIT_CORE_BOOT;
						end else if ( message_host_in == ENABLE_THREAD ) begin
							next_state              <= WAIT_DESTINATION;
							state_next_control_supp <= WAIT_THREAD_EN;
							wait_net_support_state  <= WAIT_CORE_THREAD_ENABLE;
						end
					end else if ( n2c_mes_service.message_type == SYNC ) begin
						next_state <= RECEIVE_RELEASE;
					end else begin
						wait_boot_signal <= 1'b0;
						valid_arbiter    <= 1'b0;
					end
				end

				WAIT_DESTINATION : begin
					leds_state   <= 4'b0011;
					item_avail_o <= 1'b1;

					if ( item_valid_i ) begin
						message_to_net_destinations_valid <= item_data_i[$clog2 ( `TILE_COUNT ) - 1 : 0];
						next_state                        <= state_next_control_supp;
					end

				end

				WAIT_THREAD_ID : begin
					leds_state   <= 4'b0100;
					item_avail_o <= 1'b1;

					if ( item_valid_i ) begin
						boot_message_to_net.hi_job_thread_id <= item_data_i[$clog2 ( `THREAD_NUMB ) - 1 : 0];
						next_state                           <= WAIT_PC;
					end
				end

				WAIT_PC : begin
					leds_state   <= 4'b0101;
					item_avail_o <= 1'b1;

					if ( item_valid_i ) begin
						boot_message_to_net.hi_job_pc    <= address_t' ( item_data_i ) ;
						boot_message_to_net.hi_thread_en <= {`THREAD_NUMB{1'b0}};
						boot_message_to_net.hi_job_valid <= 1'b1;
						boot_message_to_net.message      <= BOOT_COMMAND;
						next_state                       <= WAIT_NETWORK;
					end
				end

				WAIT_NETWORK : begin
					leds_state <= 4'b0110;

					if ( ni_network_available ) begin
						c2n_mes_valid <= 1'b1;
						next_state    <= wait_net_support_state;
					end
				end

				WAIT_CORE_BOOT : begin
					leds_state <= 4'b0111;

					if ( ni_network_available & n2c_mes_valid ) begin
						if ( message_from_net.message == BOOT_ACK ) begin
							n2c_mes_service_consumed <= 1'b1;
							message_host_out         <= message_from_net.message;
							next_state               <= SEND_ACK_TO_HOST;
						end
					end
				end

				WAIT_THREAD_EN : begin
					leds_state   <= 4'b1000;
					item_avail_o <= 1'b1;

					if ( item_valid_i ) begin
						boot_message_to_net                                    <= 0;
						boot_message_to_net.hi_thread_en[`THREAD_NUMB - 1 : 0] <= item_data_i[`THREAD_NUMB - 1 : 0];
						boot_message_to_net.message                            <= ENABLE_THREAD;
						next_state                                             <= WAIT_NETWORK;
					end
				end

				WAIT_CORE_THREAD_ENABLE : begin
					leds_state <= 4'b1001;

					if ( ni_network_available & n2c_mes_valid ) begin
						if ( message_from_net.message == THREAD_ENABLED ) begin
							n2c_mes_service_consumed <= 1'b1;
							message_host_out         <= message_from_net.message;
							next_state               <= SEND_ACK_TO_HOST;
						end
					end
				end

				SEND_ACK_TO_HOST : begin
					item_valid_o <= 1'b1;
					next_state   <= IDLE;
				end

				RECEIVE_RELEASE : begin
					if ( ni_network_available & n2c_mes_valid ) begin
						message_host_out <= END_OF_KERNEL;
						next_state       <= SEND_DATA_TO_HOST;
					end
				end

				SEND_DATA_TO_HOST : begin
					if ( item_avail_i ) begin
						item_valid_o <= 1'b1;
						next_state   <= IDLE;
					end
				end

			endcase
		end
	end

	idx_to_oh # (
		.NUM_SIGNALS ( $bits ( dest_valid_t ) ),
		.DIRECTION   ( "LSB0"                 ),
		.INDEX_WIDTH ( $clog2 ( `TILE_COUNT ) )
	)
	u_idx_to_oh (
		.one_hot ( one_hot_destination_valid         ),
		.index   ( message_to_net_destinations_valid )
	);

`ifdef DISPLAY_REQUESTS_MANAGER
	always_ff @ ( posedge clk ) begin
		//Command from host
		if ( ( item_valid_i & ~reset ) & update_command ) begin
			$fdisplay ( `DISPLAY_REQ_MANAGER_VAR, "\n" );
			$fdisplay ( `DISPLAY_REQ_MANAGER_VAR, "=======================" );
			$fdisplay ( `DISPLAY_REQ_MANAGER_VAR, "Host Request Manager - [Time %.16d] [TILE %.2h]", $time ( ), TILE_ID );
			print_host_interface_command ( message_host_in, state, next_state );
			$fflush ( `DISPLAY_REQ_MANAGER_VAR );
		end

		if ( ( item_valid_i & ~reset ) & ~update_command ) begin
			$fdisplay ( `DISPLAY_REQ_MANAGER_VAR, "=======================" );
			$fdisplay ( `DISPLAY_REQ_MANAGER_VAR, "Host Request Manager - [Time %.16d] [TILE %.2h]", $time ( ), TILE_ID );
			print_host_interface_message_in ( item_data_i, state, next_state );
			$fflush ( `DISPLAY_REQ_MANAGER_VAR );
		end

		if ( c2n_mes_valid & ~reset ) begin
			$fdisplay ( `DISPLAY_REQ_MANAGER_VAR, "=======================" );
			$fdisplay ( `DISPLAY_REQ_MANAGER_VAR, "Host Request Manager - [Time %.16d] [TILE %.2h]", $time ( ), TILE_ID );
			print_h2c_interface_message_to_net ( boot_message_t' ( c2n_mes_service.data ) ,c2n_mes_service.destination, state );
			$fflush ( `DISPLAY_REQ_MANAGER_VAR );
		end

		if ( mess_consumed & ~reset ) begin
			$fdisplay ( `DISPLAY_REQ_MANAGER_VAR, "=======================" );
			$fdisplay ( `DISPLAY_REQ_MANAGER_VAR, "Host Request Manager - [Time %.16d] [TILE %.2h]", $time ( ), TILE_ID );
			print_h2c_interface_message_from_net ( n2c_mes_service, next_state );
			$fflush ( `DISPLAY_REQ_MANAGER_VAR );
		end
	//STATUS RECEIVED
	//      if ( received_status & ~reset ) begin
	//          $fdisplay( `DISPLAY_REQ_MANAGER_VAR, "=======================" );
	//          $fdisplay( `DISPLAY_REQ_MANAGER_VAR, "Host Request Manager - [Time %.16d] [TILE %.2h]", $time( ), TILE_ID );
	//          print_h2c_interface_status_from_net( n2c_mes_service, n2c_mes_service.hi_thread_en );
	//      end
	end
`endif
endmodule
