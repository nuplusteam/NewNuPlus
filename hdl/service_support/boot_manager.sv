`include "nuplus_user_defines.sv"
`include "nuplus_defines.sv"
`include "nuplus_message_service_defines.sv"

`ifdef DISPLAY_REQUESTS_MANAGER
`include "nuplus_debug_log.sv"
`endif

module boot_manager # (
		parameter TILE_ID = 0 )
	(
		input                                               clk,
		input                                               reset,                   // Input: positive reset signal

		output logic                 [3 : 0]                leds_state,

		// From Network
		input  logic                                        network_available,
		input  service_c2n_message_t						message_in,//boot_message_t                               message_in,

		//      input  tile_address_t                               from_destination,        //XXX:FOR DEBUG
		input  logic                                        message_in_valid,
		output logic                                        n2c_mes_service_consumed,

		// To Network
		output service_c2n_message_t                        message_out,
		output logic                                        message_out_valid,
		output dest_valid_t                                 destination_valid,

		// Interface To Nuplus
		output logic                 [`THREAD_NUMB - 1 : 0] hi_thread_en,
		output logic                                        hi_job_valid,
		output address_t                                    hi_job_pc,
		output thread_id_t                                  hi_job_thread_id

	);

	boot_state_t                                          state = IDLE_BOOT, next_state = IDLE_BOOT;

	boot_message_t                                        message_to_net, message_from_net;
	service_c2n_message_t                                 message_out_store;

	logic                                                 message_to_net_valid, message_destination_valid, message_consumed;
	logic                                                 update_boot, update_thread_en, update_status;
	logic                 [$clog2( `TILE_COUNT ) - 1 : 0] index;
	dest_valid_t                                          one_hot_destination;

	//Signal for update core fields
	logic                                                 job_valid;
	address_t                                             job_pc;
	thread_id_t                                           job_thread_id;


	assign message_out.message_type = BOOT;
	assign message_out.destination  = message_out_store.destination;
	assign message_out.data         = message_data_t' ( message_to_net ) ;

	assign destination_valid        = one_hot_destination;
	
	assign message_from_net			= boot_message_t'(message_in.data);

	/********************************************************************
	 *                          STATE LED CODE                          *
	 * IDLE_BOOT        : 0001                                          *
	 * NOTIFY_BOOT      : 0010                                          *
	 * NOTIFY_THREAD_EN : 0011                                          *
	 * NOTIFY_STATUS    : 0100                                          *
	 *                                                                  *
	 ********************************************************************/

	//  -----------------------------------------------------------------------
	//  -- Control Unit - Next State sequential
	//  -----------------------------------------------------------------------
	always_ff @ ( posedge clk, posedge reset ) begin
		if ( reset ) begin
			state        <= IDLE_BOOT;
			hi_thread_en <= 0;
		end else begin
			state        <= next_state; // default is to stay in current state
			if ( update_thread_en )
				hi_thread_en <= hi_thread_en | message_from_net.hi_thread_en;
			else
				hi_thread_en <= hi_thread_en;
		end
	end

	always_ff @ ( posedge clk ) begin
		if ( update_boot ) begin
			//message_out_store.destination.x <= 0;
			//message_out_store.destination.y <= 1;
			message_to_net.hi_job_pc <= 32'h00000000;
			message_to_net.hi_job_thread_id <= thread_id_t'(1'b0);
			message_to_net.hi_job_valid <= 1'b0;
			message_to_net.hi_thread_en <= {`THREAD_NUMB{1'b0}};
			message_to_net.message          <= BOOT_ACK;
		end else if ( update_thread_en ) begin
			//message_out_store.destination.x <= 0;
			//message_out_store.destination.y <= 1;
			message_to_net.hi_job_pc <= 32'h00000000;
			message_to_net.hi_job_thread_id <= thread_id_t'(1'b0);
			message_to_net.hi_job_valid <= 1'b0;
			message_to_net.hi_thread_en     <= message_from_net.hi_thread_en;
			message_to_net.message          <= THREAD_ENABLED;
		end else if ( update_status ) begin
			//message_out_store.destination.x <= 0;
			//message_out_store.destination.y <= 1;
			message_to_net.hi_job_pc <= 32'h00000000;
			message_to_net.hi_job_thread_id <= thread_id_t'(1'b0);
			message_to_net.hi_job_valid <= 1'b0;
			message_to_net.hi_thread_en     <= hi_thread_en;
			message_to_net.message          <= CORE_STATUS;
		end else begin
			//message_out_store               <= message_out_store;
			message_to_net                  <= message_to_net;
		end
	end


	//  -----------------------------------------------------------------------
	//  -- Control Unit - Next State and updating signal set block
	//  -----------------------------------------------------------------------
	always_comb begin
		message_to_net_valid      <= 1'b0;
		message_destination_valid <= 1'b0;
		message_consumed          <= 1'b0;
		update_boot               <= 1'b0;
		update_thread_en          <= 1'b0;
		update_status             <= 1'b0;
		job_valid                 <= 1'b0;

		leds_state                <= 0;
		next_state                <= state;

		job_pc                    <= 1'b0;
		job_thread_id             <= 1'b0;

		unique case ( state )

			IDLE_BOOT        : begin
				leds_state <= 4'b0001;
				if ( message_in_valid ) begin
					if ( message_from_net.message == BOOT_COMMAND ) begin
						job_valid     <= message_from_net.hi_job_valid;
						job_pc        <= message_from_net.hi_job_pc;
						job_thread_id <= message_from_net.hi_job_thread_id;
						next_state    <= NOTIFY_BOOT;
					end else if ( message_from_net.message == ENABLE_THREAD ) begin
						next_state    <= NOTIFY_THREAD_EN;
					end else if ( message_from_net.message == GET_CORE_STATUS ) begin
						next_state    <= NOTIFY_STATUS;
					end else begin
						next_state    <= IDLE_BOOT;
					end
					message_consumed          <= 1'b1;
				end else
					next_state                <= IDLE_BOOT;
			end


			NOTIFY_BOOT      : begin
				leds_state <= 4'b0010;
				if ( network_available ) begin
					update_boot               <= 1'b1;
					message_to_net_valid      <= 1'b1;
					message_destination_valid <= 1'b1;
					next_state                <= IDLE_BOOT;
				end else begin
					next_state                <= NOTIFY_BOOT;
				end
			end

			NOTIFY_THREAD_EN : begin
				leds_state <= 4'b0011;
				if ( network_available ) begin
					update_thread_en          <= 1'b1;

					message_to_net_valid      <= 1'b1;
					message_destination_valid <= 1'b1;
					next_state                <= IDLE_BOOT;
				end else begin
					next_state                <= NOTIFY_THREAD_EN;
				end
			end

			NOTIFY_STATUS    : begin
				leds_state <= 4'b0100;
				if ( network_available ) begin
					update_status             <= 1'b1;
					message_to_net_valid      <= 1'b1;
					message_destination_valid <= 1'b1;
					next_state                <= IDLE_BOOT;
				end else begin
					next_state                <= NOTIFY_STATUS;
				end
			end

		endcase
	end


	always_comb
		index <= `TILE_H2C_ID;


	idx_to_oh #(
		.NUM_SIGNALS( $bits( dest_valid_t ) ),
		.DIRECTION  ( "LSB0"                ),
		.INDEX_WIDTH( $clog2( `TILE_COUNT ) )
	)
	u_idx_to_oh (
		.one_hot( one_hot_destination ),
		.index  ( index               )
	);

	//  -----------------------------------------------------------------------
	//  -- Control Unit - Update Output
	//  -----------------------------------------------------------------------

	always_ff @ ( posedge clk, posedge reset ) begin
		if ( reset ) begin
			message_out_valid        <= 1'b0;
			//destination_valid                   <= 1'b0;
			n2c_mes_service_consumed <= 1'b0;
			hi_job_valid             <= 1'b0;
			hi_job_pc                <= 1'b0;
			hi_job_thread_id         <= 1'b0;
		end else begin
			message_out_valid        <= message_to_net_valid;
			//destination_valid                   <= message_destination_valid;
			n2c_mes_service_consumed <= message_consumed;
			hi_job_valid             <= job_valid;
			hi_job_pc                <= job_pc;
			hi_job_thread_id         <= job_thread_id;
		end
	end

`ifdef DISPLAY_REQUESTS_MANAGER


	always_ff @ ( posedge clk ) begin
		if ( message_consumed & ~reset ) begin
			$fdisplay ( `DISPLAY_REQ_MANAGER_VAR, "=======================" ) ;
			$fdisplay ( `DISPLAY_REQ_MANAGER_VAR, "Boot Manager - [Time %.16d] [TILE %.2h]", $time ( ), TILE_ID ) ;
			//print_boot_manager_message_in ( message_in, from_destination ) ;
		end

		if ( message_to_net_valid & ~reset ) begin
			$fdisplay ( `DISPLAY_REQ_MANAGER_VAR, "=======================" ) ;
			$fdisplay ( `DISPLAY_REQ_MANAGER_VAR, "Boot Manager - [Time %.16d] [TILE %.2h]", $time ( ), TILE_ID ) ;
			print_boot_manager_message_out ( message_to_net.message ) ;
		end

		if ( message_consumed | message_to_net_valid ) begin
			$fflush( `DISPLAY_REQ_MANAGER_VAR ) ;
	end
`endif

endmodule
