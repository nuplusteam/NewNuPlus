`ifndef __NUPLUS_MESSAGE_SERVICE_DEFINES
`define __NUPLUS_MESSAGE_SERVICE_DEFINES

`include "nuplus_defines.sv"
`include "nuplus_network_defines.sv"

`define HOST_COMMAND_NUM          16
`define HOST_COMMAND_WIDTH        ( $clog2( `HOST_COMMAND_NUM ) )
`define SETUP_IS_MASTER           ( $clog2( `BARRIER_NUMB ) + 2 )

typedef logic [`TILE_COUNT - 1 : 0] dest_valid_t;

/******FOR SINGLE CORE IMPLEMENTATION*******/
typedef enum{
	WAIT_COMMAND,
	STATE_BOOT_CORE,
	STATE_ENABLE_THREAD,
	STATE_SEND_ACK,
	STATE_RECEIVE_MEM_ADDRESS,
	STATE_RECEIVE_MEM_LINE,
	STATE_WRITE_MEMORY,
	STATE_READ_MEM_LINE,
	STATE_FWD_MEM_LINE,
	STATE_WAIT_MEM_READ,
	STATE_CHECK_FIFO_UART_EMPTY,
	STATE_END_JOB

}host_manager_state_t;


`define THREAD_EN                 8'h22	//todo parti da 20
`define BOOT_ACK                  8'h23
`define MEM_NOT_AVAILABLE         8'h24
`define END_MEM_LINE              8'h25
`define COMMAND_RECEIVED          8'h26

/*******************************************/


typedef enum logic [`HOST_COMMAND_WIDTH - 1 : 0] {
	//COMMAND FOR BOOT
	BOOT_COMMAND,
	ENABLE_THREAD,
	GET_CORE_STATUS,
	SET_CONTROL_REGISTER,

	//COMMAND FOR SYNC
	SYNC_COMMAND,

	//RESPONSE FROM BOOT COMMAND REQUEST
	BOOT_ACK,
	THREAD_ENABLED,
	END_OF_KERNEL,
	CORE_STATUS     
} host_messages_t;


typedef enum {
	START,
	IDLE,
	WAIT_DESTINATION,
	WAIT_THREAD_ID,
	WAIT_PC,
	WAIT_CORE_BOOT,
	WAIT_THREAD_EN,
	WAIT_CORE_THREAD_ENABLE,
	WAIT_NETWORK,
	SEND_STATUS_REQ,
	SEND_ACK_TO_HOST,
	SEND_DATA_TO_HOST,
	RECEIVE_RELEASE,
	WAIT_CORE_STATUS
}interface_state_t;

typedef enum {
	START_BOOT,
	IDLE_BOOT,
	RECEIVE_BOOT_INFO,
	NOTIFY_BOOT,
	NOTIFY_THREAD_EN,
	NOTIFY_STATUS
}boot_state_t;


typedef struct packed {                        // XXX: nel caso peggiore sono 63 - non dovrebbe servire has_data
	address_t hi_job_pc;                       //32
	logic hi_job_valid;                        //1
	thread_id_t hi_job_thread_id;              //2
	logic [`THREAD_NUMB - 1 : 0] hi_thread_en; //4
	host_messages_t message;                   //8
} boot_message_t;

`define HOST_MESSAGE_LENGTH       ( $bits( boot_message_t ) )
typedef logic [`HOST_MESSAGE_LENGTH - 1 : 0] message_data_t;

`define MESSAGE_TYPE_LENGTH       1

typedef enum logic [`MESSAGE_TYPE_LENGTH - 1 : 0] {
	BOOT = 0,
	SYNC = 1
} message_type_t;

typedef struct packed {
	message_type_t message_type;
	tile_address_t destination; //1
	message_data_t data;
} service_c2n_message_t;


function interface_state_t read_boot_command ( host_messages_t message_in );
	interface_state_t next_state;
	unique case ( message_in )
		BOOT_COMMAND    : next_state = WAIT_DESTINATION;
		ENABLE_THREAD   : next_state = WAIT_DESTINATION;
		GET_CORE_STATUS : next_state = WAIT_DESTINATION;
		default : next_state         = interface_state_t'(IDLE);
	endcase
	return next_state;
endfunction : read_boot_command

`endif