`ifndef __NUPLUS_SYSTEM_DEFINES_SV
`define __NUPLUS_SYSTEM_DEFINES_SV

`include "nuplus_defines.sv"

`define AXI_DATA_WIDTH        32
`define AXI_DATA_BYTE_WIDTH   `AXI_DATA_WIDTH/8
`define UART_HM_WIDTH         32
`define BUS_MASTER            2
`define CLOCK_RATE            50000000
`define UART_BAUD_RATE        9600
`define ID_DEVICE_LENGTH      32

//  -----------------------------------------------------------------------
//  -- Loader Defines
//  -----------------------------------------------------------------------

`define WRITE_MEMORY_COMMAND  8'h01
`define READ_MEMORY_COMMAND   8'h02
`define BOOT_CORE_COMMAND     8'h03
`define ENABLE_THREAD_COMMAND 8'h04
`define READ_ID_COMMAND       8'h05
`define BARRIER_COMMAND       8'h06

`define WRITE_MEMORY_ACK      8'h11
`define READ_MEMORY_ACK       8'h12
`define BOOT_CORE_ACK         8'h13
`define ENABLE_THREAD_ACK     8'h14
`define READ_ID_ACK           8'h15
`define BARRIER_ACK           8'h16

//  -----------------------------------------------------------------------
//  -- HOST and UART Defines
//  -----------------------------------------------------------------------

typedef enum {

	STATE_WAIT_COMMAND,
	STATE_WAIT_HOST_ACK,
	STATE_RECEIVE_DATA_COMMAND,
	STATE_WAIT_BUFFER_DEQUEUE,
	STATE_SEND_COMMAND,
	STATE_SEND_DATA,
	STATE_FINAL_ACK,
	STATE_CHECK_TERMINATION

} uart_state_t;

typedef enum {

	STATE_WAIT_MEMORY_COMMAND,
	STATE_READ_MEMORY,
	STATE_WAIT_DATA,
	STATE_WRITE_DATA

} uart_memory_t;

//  -----------------------------------------------------------------------
//  -- AMBA AXI Defines
//  -----------------------------------------------------------------------

// AMBA AXI and ACE Protocol Specification, rev E, Table A3-3
typedef enum logic[1:0] {
	AXI_BURST_FIXED = 2'b00,
	AXI_BURST_INCR  = 2'b01,
	AXI_BURST_WRAP  = 2'b10
} axi_burst_type_t;

`endif