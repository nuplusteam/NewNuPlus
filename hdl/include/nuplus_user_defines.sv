`ifndef __NUPLUS_USER_DEFINES_SV
`define __NUPLUS_USER_DEFINES_SV

/*
 * This project provides a simulation testbench (tb_manycore), which simulates the host
 * communication, the main memory and the synchronization core used during memory barriers.
 * It initializes and lunches the NuPlus processor, then waits the end of the kernel computation.
 *
 * In order to run a kernel in simulation, the below paths should be up-to-date. The testbench relies
 * on a memory dummy module which is pre-loaded with the hex image produced by the LLVM tool-chain.
 * The selected memory image file is linked through the MEMORY_BIN variable defined below.
 *
 * Furthermore, DISPLAY variables are defined, all commented by default. When a DISPLAY variable is
 * active, it generates a file, under a folder named after the selected kernel. Each DISPLAY variable
 * logs a defined kind of transaction, namely:
 *
 *  1  - DISPLAY_MEMORY: logs on file the memory state at the end of the kernel execution.
 *  2  - DISPLAY_MEMORY_TRANS: logs on file all requests to the main memory.
 *  3  - DISPLAY_INT: logs every integer operation in the integer pipeline, and their results.
 *  4  - DISPLAY_CORE: enables log from the core (file display_core.txt).
 *  5  - DISPLAY_ISSUE: logs all scheduled instructions, and tracks the scheduled PC and the issued Thread, when DISPLAY_CORE is defined.
 *  6  - DISPLAY_INT: logs all results from the integer module, when DISPLAY_CORE is defined.
 *  7  - DISPLAY_WB: logs all results from the writeback module, when DISPLAY_CORE is defined.
 *  8  - DISPLAY_LDST: enagles logging into the load/store unit (file display_ldst.txt).
 *  9  - DISPLAY_CACHE_CONTROLLER: logs memory transactions between Load/Store unit and the main memory.
 *  10 - DISPLAY_SYNCH_CORE: logs synchronization requests within the core.
 *  11 - DISPLAY_BARRIER_CORE: logs synchronization releases from the Synchronization master.
 *  12 - DISPLAY_COHERENCE: logs all coherence transactions among CCs, DCs and MC.
 *  13 - DISPLAY_THREAD_STATUS: displays all active threads status and trap reason.
 *
 */

//  -----------------------------------------------------------------------
//  -- Architecture Parameters
//  -----------------------------------------------------------------------

// Core user defines
`define THREAD_NUMB              8 // Must be power of 2
`define USER_ICACHE_SET          32 // Must be power of 2
`define USER_ICACHE_WAY          4 // Must be power of 2
`define USER_DCACHE_SET          32 // Must be power of 2
`define USER_DCACHE_WAY          4 // Must be power of 2
`define USER_L2CACHE_SET         128 // Must be power of 2
`define USER_L2CACHE_WAY         8 // Must be power of 2

`define DIRECTORY_BARRIER        // When defined the system supports a distributed directory over all tiles. Otherwise, it allocates a single directory
`define CENTRAL_SYNCH_ID         0 // Single directory ID, used only when DIRECTORY_BARRIER is undefined
//`define PERFORMANCE_SYNC

// Non-coherent Memory space range
`define IO_MAP_BASE_ADDR         32'hFF00_0000
`define IO_MAP_SIZE              32'h00FF_FFBF

// NoC user defines
//`define NoC_WIDTH      2
`define NoC_X_WIDTH              2 // Must be power of 2
`define NoC_Y_WIDTH              2 // Must be power of 2
`define TILE_COUNT               ( `NoC_X_WIDTH * `NoC_Y_WIDTH )
`define TILE_MEMORY_ID           ( `TILE_COUNT - 1 )
`define TILE_H2C_ID              ( `TILE_COUNT - 2 )
`define TILE_NUPLUS              ( `TILE_COUNT - 2 ) // core tiles count
`define BOOT_HOST                // lasciare definito se si vuole usare l'host come boot (nel manycore)

//  -----------------------------------------------------------------------
//  -- Simulation Parameters
//  -----------------------------------------------------------------------

`define SIMULATION

`ifdef SIMULATION

	`define KERNEL_NAME            "mmsc"
	`define PROJECT_PATH           "/home/mirko/workspace_newnuplus/NewNuPlus/"
	`define MEMORY_PATH            {"toolchain/kernel/",`KERNEL_NAME,"/obj/"}
	`define MEMORY_BIN             {`PROJECT_PATH, `MEMORY_PATH, `KERNEL_NAME,"_mem.hex"}

	`define DISPLAY_COHERENCE
	`define DISPLAY_CORE
	`define DISPLAY_ISSUE
	`define DISPLAY_INT
	`define DISPLAY_WB
 	`define DISPLAY_LDST
	`define DISPLAY_MEMORY
	`define DISPLAY_MEMORY_TRANS
	`define DISPLAY_THREAD_STATUS
	//`define DISPLAY_REQUESTS_MANAGER
	//`define DISPLAY_SYNC
	//`define DISPLAY_BARRIER_CORE
	//`define DISPLAY_SYNCH_CORE
	`define DISPLAY_SIMULATION_LOG

`endif

`endif
