`timescale 1ns / 1ps
`include "nuplus_defines.sv"
`include "nuplus_coherence_defines.sv"
`include "nuplus_network_defines.sv"
`include "nuplus_message_service_defines.sv"
`include "nuplus_debug_log.sv"

module tb_manycore #(
		parameter KERNEL_IMAGE        = `MEMORY_BIN,
		parameter SIMULATION_FILE_LOG = `DISPLAY_SIMULATION_LOG_FILE,
		parameter THREAD_MASK         = 8'hFF,
		parameter CORE_MASK           = 32'h03 )
	( );

    localparam NoC_ROW          = `NoC_Y_WIDTH;
    localparam NoC_COL          = `NoC_X_WIDTH;
    localparam MEM_ADDR_w       = `ADDRESS_SIZE;
    localparam MEM_DATA_BLOCK_w = `DCACHE_WIDTH;
    localparam ITEM_w           = 32;

	logic                              clk                   = 1'b1;
	logic                              reset                 = 1'b1;
	logic                              enable                = 1'b1;

	logic             [ITEM_w - 1 : 0] item_data_i;
	logic                              item_valid_i;
	logic                              item_avail_o;
	logic             [ITEM_w - 1 : 0] item_data_o;
	logic                              item_valid_o;
	logic                              item_avail_i;
	logic                              mc_avail_o;


	address_t                          n2m_request_address;
	dcache_line_t                      n2m_request_data;
	logic                              n2m_request_read;
	logic                              n2m_request_write;

	logic                              m2n_request_available;
	logic                              m2n_response_valid;
	address_t                          m2n_response_address;
	dcache_line_t                      m2n_response_data;

	int                                sim_log_file, sim_start, sim_end;
	logic                              simulation_end        = 1'b0;

    localparam PC_DEFAULT            = 32'h0000_0404;

	nuplus_noc #(
		.MEM_ADDR_w      ( MEM_ADDR_w       ),
		.MEM_DATA_BLOCK_w( MEM_DATA_BLOCK_w )
	)
	u_nuplus_noc (
		.clk                 ( clk                   ),
		.reset               ( reset                 ),
		.enable              ( enable                ),
		//interface MANGO_TILEREG <-> NUPLUS
		.item_data_i         ( item_data_i           ), //Input: items from outside
		.item_valid_i        ( item_valid_i          ), //Input: valid signal associated with item_data_i port
		.item_avail_o        ( item_avail_o          ), //Output: avail signal to input port item_data_i
		.item_data_o         ( item_data_o           ), //Output: items to outside
		.item_valid_o        ( item_valid_o          ), //Output: valid signal associated with item_data_o port
		.item_avail_i        ( item_avail_i          ), //Input: avail signal to output port item_data_o
		//interface MC
		.n2m_request_is_instr(                       ),

		.mc_address_o        ( n2m_request_address   ), //output: Address to MC
		.mc_block_o          ( n2m_request_data      ), //output: Data block to MC
		.mc_avail_o          ( mc_avail_o            ), //output: available bit from UNIT
		.mc_sender_o         (                       ), //output: sender to MC
		.mc_read_o           ( n2m_request_read      ), //output: read request to MC
		.mc_write_o          ( n2m_request_write     ), //output: write request to MC

		.mc_address_i        ( m2n_response_address  ), //input: Address from MC
		.mc_block_i          ( m2n_response_data     ), //input: Data block from MC
		.mc_dst_i            (                       ), //input: destination from MC
		.mc_sender_i         (                       ), //input: Sender from MC
		.mc_read_avail_i     ( m2n_request_available ), //input: read available signal from MC
		.mc_write_avail_i    ( m2n_request_available ), //input: write available signal from MC
		.mc_valid_i          ( m2n_response_valid    ), //input: valid bit from MC
		.mc_request_i        (                       )  //input: Read/Write request from MC
	);

	mango_dummy #(
		.OFF_WIDTH      ( `ICACHE_OFFSET_LENGTH ),
		.FILENAME_INSTR ( KERNEL_IMAGE          )
	)
	u_mango_dummy (
		.clk                  ( clk                   ),
		.reset                ( reset                 ),
		//From MC
		//To MANGO NI
		.n2m_request_address  ( n2m_request_address   ),
		.n2m_request_data     ( n2m_request_data      ),
		.n2m_request_read     ( n2m_request_read      ),
		.n2m_request_write    ( n2m_request_write     ),
		.mc_avail_o           ( mc_avail_o            ),
		//From MANGO NI
		.m2n_request_available( m2n_request_available ),
		.m2n_response_valid   ( m2n_response_valid    ),
		.m2n_response_address ( m2n_response_address  ),
		.m2n_response_data    ( m2n_response_data     )
	);

	typedef struct packed {
		tile_address_t destination;
		host_messages_t cmd;
	} message_to_core_t;

	message_to_core_t                  message_to_core;

	always #5 clk = ~clk;

	localparam ACTIVE_THREADS = $countones(CORE_MASK) * $countones(THREAD_MASK);
	localparam COUNTER_WIDTH = $clog2(ACTIVE_THREADS+1);

	logic [COUNTER_WIDTH-1:0]          write_cnt;

	always_ff @( posedge clk, posedge reset ) begin
		if ( reset )
			write_cnt <= {COUNTER_WIDTH{1'b0}};
		else if ( n2m_request_write && n2m_request_address == 32'hFFFFFFC0 )
			write_cnt <= write_cnt + 1;
	end

//  -----------------------------------------------------------------------
//  -- Testbench Body
//  -----------------------------------------------------------------------

	initial begin
		nuplus_configuration( );

		#100
		reset        = 1'b0;
		item_avail_i = 1'b1;

		// NuPlus Cores
		for ( int row = 0; row < NoC_ROW; row++ ) begin
			for ( int col = 0; col < NoC_COL; col++ ) begin
				if ( ( ( row * NoC_COL + col ) < `TILE_NUPLUS ) & CORE_MASK[row * NoC_COL + col] ) begin
					// Sets thread PCs
					for ( int i = 0; i < `THREAD_NUMB; i++ ) begin
						if ( THREAD_MASK[i] )
							set_core_pc( row, col, i, PC_DEFAULT );
					end
				end
			end
		end

		#100
		// NuPlus Cores
		for ( int row = 0; row < NoC_ROW; row++ ) begin
			for ( int col = 0; col < NoC_COL; col++ ) begin
				if ( ( ( row * NoC_COL + col ) < `TILE_NUPLUS ) & CORE_MASK[row * NoC_COL + col] ) begin
					//#10000
					enable_core( row, col, THREAD_MASK );
					//if (col == 0) #400000;
				end
			end
		end
		
		sim_start = read_global_counter( );
		
		wait( write_cnt == ACTIVE_THREADS );
		#1000 $finish;
	end
	
	final begin
		sim_end   = read_global_counter( );
		$display( "[Time %t] [TESTBENCH] Total Simulation Time: %d ", $time( ), sim_end - sim_start );
		$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] Total Simulation Time: %d ", $time( ), sim_end - sim_start );
		nuplus_stats( 0, 0 );
	end 
	
//  -----------------------------------------------------------------------
//  -- TB Tasks
//  -----------------------------------------------------------------------

	task set_core_pc;
        input [`TOT_X_NODE_W-1:0] row;
        input [`TOT_Y_NODE_W-1:0] col;
        input [7 : 0] thread_id;
        input [31 : 0] pc;

        $display( "[Time %t] [HOST INTERFACE] Initializing Thread %2d of Core %1d - PC %4x ", $time( ), thread_id, ( row * NoC_COL + col ), pc );
        $fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [HOST INTERFACE] Initializing Thread %2d of Core1d - PC %4x ", $time( ), thread_id, ( row * NoC_COL + col ), pc );

        #10
        message_to_core.destination.x = col;
        message_to_core.destination.y = row;
        message_to_core.cmd           = BOOT_COMMAND;

        #10
        wait( item_avail_o == 1 );

        #5
        item_valid_i                  = 1'b1;
        item_data_i                   = message_to_core.cmd;

        #10
        item_data_i                   = message_to_core.destination;

        #10
        item_data_i                   = thread_id; // ID THREAD

        #10
        item_data_i                   = pc;        // PC

        #10
        item_valid_i                  = 1'b0;
        endtask

	task enable_core;
        input [`TOT_X_NODE_W-1:0] row;
        input [`TOT_Y_NODE_W-1:0] col;
        input [`THREAD_NUMB-1 : 0] thread_mask;

        $display( "[Time %t] [HOST INTERFACE] Enabling Core %1d - Thread mask: %4x", $time( ), ( row * NoC_COL + col ), thread_mask );
        $fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [HOST INTERFACE] Enabling Core %1d - Thread mask: %4x", $time( ), ( row * NoC_COL + col ), thread_mask );

        #10
        message_to_core.destination.x = col;
        message_to_core.destination.y = row;
        message_to_core.cmd           = ENABLE_THREAD;

        #10
        wait( item_avail_o == 1 );

        #5
        item_valid_i                  = 1'b1;
        item_data_i                   = message_to_core.cmd;

        #10
        item_data_i                   = message_to_core.destination;

        #10
        item_data_i                   = thread_mask; // THREAD MASK

        #10
        item_valid_i                  = 1'b0;
        endtask

	task set_control_register;
        input [`TOT_X_NODE_W-1:0] row;
        input [`TOT_Y_NODE_W-1:0] col;
        input [31 : 0] register;
        input [31 : 0] data;

        $display( "[Time %t] [HOST INTERFACE] Writing Control Register %2d of Core %1d - Value %4x ", $time( ), register, ( row * NoC_COL + col ), register, data );
        $fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [HOST INTERFACE] Writing Control Register %2d of Core %1d - Value %4x ", $time( ), register, ( row * NoC_COL + col ), register, data );

        #10
        message_to_core.destination.x = col;
        message_to_core.destination.y = row;
        message_to_core.cmd           = SET_CONTROL_REGISTER;

        #10
        wait( item_avail_o == 1 );

        #5
        item_valid_i                  = 1'b1;
        item_data_i                   = message_to_core.cmd;

        #10
        item_data_i                   = message_to_core.destination;

        #10
        item_data_i                   = register; // Register ID

        #10
        item_data_i                   = data;     // Value

        #10
        item_valid_i                  = 1'b0;
        endtask

	task nuplus_configuration;
		$display( "[Time %t] [TESTBENCH] Total Tiles: %4d ", $time( ), `TILE_COUNT );
		$display( "[Time %t] [TESTBENCH] Net X Width: %4d ", $time( ), `NoC_X_WIDTH );
		$display( "[Time %t] [TESTBENCH] Net Y Width: %4d ", $time( ), `NoC_Y_WIDTH );
		$display( "[Time %t] [TESTBENCH] Tile MC ID : %4d ", $time( ), `TILE_MEMORY_ID );
		$display( "[Time %t] [TESTBENCH] Tile H2C ID: %4d ", $time( ), `TILE_H2C_ID );
		$display( "[Time %t] [TESTBENCH] Total Cores: %4d ", $time( ), `TILE_NUPLUS );
		$display( "[Time %t] [TESTBENCH] Thread Numb: %4d ", $time( ), `THREAD_NUMB );
		$display( "[Time %t] [TESTBENCH] DCache Sets: %4d ", $time( ), `USER_ICACHE_SET );
		$display( "[Time %t] [TESTBENCH] DCache Ways: %4d ", $time( ), `USER_ICACHE_WAY );
		$display( "[Time %t] [TESTBENCH] ICache Sets: %4d ", $time( ), `USER_DCACHE_SET );
		$display( "[Time %t] [TESTBENCH] ICache Ways: %4d ", $time( ), `USER_DCACHE_WAY );

		$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] Total Tiles: %4d ", $time( ), `TILE_COUNT );
		$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] Net X Width: %4d ", $time( ), `NoC_X_WIDTH );
		$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] Net Y Width: %4d ", $time( ), `NoC_Y_WIDTH );
		$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] Tile MC ID : %4d ", $time( ), `TILE_MEMORY_ID );
		$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] Tile H2C ID: %4d ", $time( ), `TILE_H2C_ID );
		$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] Total Cores: %4d ", $time( ), `TILE_NUPLUS );
		$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] Thread Numb: %4d ", $time( ), `THREAD_NUMB );
		$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] DCache Sets: %4d ", $time( ), `USER_ICACHE_SET );
		$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] DCache Ways: %4d ", $time( ), `USER_ICACHE_WAY );
		$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] ICache Sets: %4d ", $time( ), `USER_DCACHE_SET );
		$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] ICache Ways: %4d ", $time( ), `USER_DCACHE_WAY );
	endtask


	task nuplus_stats;
		input int row;
		input int col;
		$display( "[Time %t] [TESTBENCH] [CORE %1d] Output Memory: %h ", $time( ), ( col * NoC_COL + row ), u_nuplus_noc.NOC_ROW_GEN[0].NOC_COL_GEN[0].TILE_NUPLUS_INST.u_tile_nuplus.u_nuplus_core.u_control_register.argv_register );
		$display( "[Time %t] [TESTBENCH] [CORE %1d] Kernel Cycles: %d ", $time( ), ( col * NoC_COL + row ), u_nuplus_noc.NOC_ROW_GEN[0].NOC_COL_GEN[0].TILE_NUPLUS_INST.u_tile_nuplus.u_nuplus_core.u_control_register.kernel_cycles );
		$display( "[Time %t] [TESTBENCH] [CORE %1d] DCache misses: %d ", $time( ), ( col * NoC_COL + row ), u_nuplus_noc.NOC_ROW_GEN[0].NOC_COL_GEN[0].TILE_NUPLUS_INST.u_tile_nuplus.u_nuplus_core.u_control_register.data_miss_counter );
		$display( "[Time %t] [TESTBENCH] [CORE %1d] ICache misses: %d ", $time( ), ( col * NoC_COL + row ), u_nuplus_noc.NOC_ROW_GEN[0].NOC_COL_GEN[0].TILE_NUPLUS_INST.u_tile_nuplus.u_nuplus_core.u_control_register.instr_miss_counter );


		$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] [CORE %1d] Kernel Cycles: %d ", $time( ), ( col * NoC_COL + row ), u_nuplus_noc.NOC_ROW_GEN[0].NOC_COL_GEN[0].TILE_NUPLUS_INST.u_tile_nuplus.u_nuplus_core.u_control_register.kernel_cycles );
		$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] [CORE %1d] DCache misses: %d ", $time( ), ( col * NoC_COL + row ), u_nuplus_noc.NOC_ROW_GEN[0].NOC_COL_GEN[0].TILE_NUPLUS_INST.u_tile_nuplus.u_nuplus_core.u_control_register.data_miss_counter );
		$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] [CORE %1d] ICache misses: %d ", $time( ), ( col * NoC_COL + row ), u_nuplus_noc.NOC_ROW_GEN[0].NOC_COL_GEN[0].TILE_NUPLUS_INST.u_tile_nuplus.u_nuplus_core.u_control_register.instr_miss_counter );

		for ( int thread_id = 0; thread_id < `THREAD_NUMB; thread_id++ ) begin
			$display( "[Time %t] [TESTBENCH] [CORE %1d] Thread %2d active cycles: %d ", $time( ), ( col * NoC_COL + row ), thread_id, u_nuplus_noc.NOC_ROW_GEN[0].NOC_COL_GEN[0].TILE_NUPLUS_INST.u_tile_nuplus.u_nuplus_core.u_control_register.thread_work_cycles[thread_id] );
			$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] [CORE %1d] Thread %2d active cycles: %d ", $time( ), ( col * NoC_COL + row ), thread_id, u_nuplus_noc.NOC_ROW_GEN[0].NOC_COL_GEN[0].TILE_NUPLUS_INST.u_tile_nuplus.u_nuplus_core.u_control_register.thread_work_cycles[thread_id] );
		end

		for ( int thread_id = 0; thread_id < `THREAD_NUMB; thread_id++ ) begin
			$display( "[Time %t] [TESTBENCH] [CORE %1d] Thread %2d misses cycles: %d ", $time( ), ( col * NoC_COL + row ), thread_id, u_nuplus_noc.NOC_ROW_GEN[0].NOC_COL_GEN[0].TILE_NUPLUS_INST.u_tile_nuplus.u_nuplus_core.u_control_register.thread_blocked_cycle_count[thread_id] );
			$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] [CORE %1d] Thread %2d misses cycles: %d ", $time( ), ( col * NoC_COL + row ), thread_id, u_nuplus_noc.NOC_ROW_GEN[0].NOC_COL_GEN[0].TILE_NUPLUS_INST.u_tile_nuplus.u_nuplus_core.u_control_register.thread_blocked_cycle_count[thread_id] );
		end
	endtask
	
	function int read_global_counter ( );
		int data = u_nuplus_noc.NOC_ROW_GEN[0].NOC_COL_GEN[0].TILE_NUPLUS_INST.u_tile_nuplus.u_nuplus_core.u_control_register.global_counter;
		$display( "[Time %t] [TESTBENCH] Global Counter: %x ", $time( ), data );
		$fdisplay( `DISPLAY_SIMULATION_LOG_VAR, "[Time %t] [TESTBENCH] Global Counter: %x ", $time( ), data );
		return data;
	endfunction : read_global_counter
	
//  -----------------------------------------------------------------------
//  -- TB Simulation File log
//  -----------------------------------------------------------------------

`ifdef DISPLAY_SIMULATION_LOG
	initial sim_log_file = $fopen ( SIMULATION_FILE_LOG, "wb" ) ;

	final $fclose ( sim_log_file ) ;
`endif

endmodule
