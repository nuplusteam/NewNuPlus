`include "nuplus_user_defines.sv"
`include "nuplus_defines.sv"
`include "nuplus_debug_log.sv"

module mango_dummy #(
		parameter ADDRESS_WIDTH  = 32,
		parameter DATA_WIDTH     = 512,
		parameter MAX_WIDTH      = 262400, // 0x41000 * 64 byte = 0x01004000
		parameter OFF_WIDTH      = 6,
		parameter FILENAME_INSTR = "" )
	(
		input                                clk,
		input                                reset,

		// From MC
		// To MANGO NI
		input  logic [ADDRESS_WIDTH - 1 : 0] n2m_request_address,
		input  logic [DATA_WIDTH - 1 : 0]    n2m_request_data,
		input  logic                         n2m_request_read,
		input  logic                         n2m_request_write,
		input  logic                         mc_avail_o,

		// From MANGO NI
		output logic                         m2n_request_available,
		output logic                         m2n_response_valid,
		output logic [ADDRESS_WIDTH - 1 : 0] m2n_response_address,
		output logic [DATA_WIDTH - 1 : 0]    m2n_response_data
	);

	logic   [DATA_WIDTH - 1 : 0]    mem_dummy [MAX_WIDTH];
	logic   [ADDRESS_WIDTH - 1 : 0] address_in;
	logic   [ADDRESS_WIDTH - 1 : 0] address_max;
	logic                           request_was_read;

	typedef enum logic[1 : 0] {IDLE, WAITING} state_t;

	integer                         delay = 0;
	state_t                         state = IDLE;

	always_ff @ ( posedge clk, posedge reset ) begin
		if ( reset )
			address_max <= 0;
		else
			if ( n2m_request_write & ( n2m_request_address > address_max ) & ( n2m_request_address != 32'hFFFFFFC0 ) )
				address_max <= n2m_request_address;
	end

	initial begin
		integer fd;

		fd = $fopen( FILENAME_INSTR, "r" );
		assert (fd != 0) else $error( "[MEMORY] Cannot open memory image" );
		$fclose( fd );

		$readmemh( FILENAME_INSTR, mem_dummy );
		$display( "[Time %t] [MEMORY] Memory image %s loaded", $time(), FILENAME_INSTR );
	end

	integer avail_next_cycle;
	always_ff @(posedge clk) begin
		//avail_next_cycle <= $random % 10;
		avail_next_cycle <= 1;
	end

	assign m2n_request_available = ~(n2m_request_read | n2m_request_write) & state != WAITING & avail_next_cycle == 1;

	always_ff @( posedge clk, posedge reset ) begin
		if ( reset ) begin
			state                 <= IDLE;
			m2n_response_data     <= 0;
			m2n_response_valid    <= 1'b0;
			delay                 <= 0;
		end else begin

			m2n_response_valid    <= 1'b0;

			case( state )
				IDLE    : begin
					state <= IDLE;
					delay <= 0;
					if ( n2m_request_read ) begin
						state                                           <= WAITING;
						request_was_read                                <= 1'b1;
						address_in                                      <= n2m_request_address >> OFF_WIDTH;
						//delay                                           <= ( ( $urandom ) % 10 ) + 20;
						delay                                           <= 0;
					end else if ( n2m_request_write ) begin
						state                                           <= WAITING;
						request_was_read                                <= 1'b0;
						address_in                                      <= n2m_request_address >> OFF_WIDTH;
						mem_dummy[( n2m_request_address >> OFF_WIDTH )] <= n2m_request_data;
					end
				end

				WAITING : begin
					if ( delay == 0 ) begin
						if ( ~request_was_read || mc_avail_o ) begin
							state                <= IDLE;
						end else begin
							state                <= WAITING;
						end

						if ( request_was_read && mc_avail_o ) begin
							m2n_response_valid   <= 1'b1;
							m2n_response_data    <= mem_dummy[address_in[$clog2( MAX_WIDTH ) - 1 : 0]];
							m2n_response_address <= address_in << OFF_WIDTH;
						end
					end else begin
						delay                   <= delay - 1;
					end
				end
			endcase
		end
	end

`ifdef DISPLAY_MEMORY

	final begin
		for ( int i = 0; i <= address_max[ADDRESS_WIDTH - 1 : OFF_WIDTH]; i++ ) begin
			$fdisplay( `DISPLAY_MEMORY_VAR, "%h \t %h", ( i * 64 ), mem_dummy[i] );
		end
		$fclose( `DISPLAY_MEMORY_VAR );
	end

`endif

`ifdef DISPLAY_MEMORY_TRANS

	always_ff @( posedge clk ) begin
		if( n2m_request_write )begin
			$fdisplay ( `DISPLAY_MEMORY_TRANS_VAR, "[Time :%t] [MEMORY]: Write request - Address: %h\tData: %h", $time(), n2m_request_address, n2m_request_data);
			$fflush ( `DISPLAY_MEMORY_TRANS_VAR );
		end

		if( n2m_request_read )begin
			$fdisplay ( `DISPLAY_MEMORY_TRANS_VAR, "[Time :%t] [MEMORY]: Read request - Address: %h", $time(), n2m_request_address);
			$fflush ( `DISPLAY_MEMORY_TRANS_VAR );
		end

		if( m2n_response_valid )begin
			$fdisplay ( `DISPLAY_MEMORY_TRANS_VAR, "[Time :%t] [MEMORY]: Read response - Address: %h\tData: %h", $time(), m2n_response_address, m2n_response_data);
			$fflush ( `DISPLAY_MEMORY_TRANS_VAR );
		end
	end
`endif

endmodule
