`include "nuplus_user_defines.sv"
`include "nuplus_defines.sv"
`include "nuplus_coherence_defines.sv"
`include "nuplus_network_defines.sv"
`include "nuplus_message_service_defines.sv"
`include "nuplus_synchronization_defines.sv"

`ifdef DISPLAY_CORE
	`include "nuplus_debug_log.sv"
`endif

module tile_nuplus # (
		parameter TILE_ID        = 0,
		parameter TILE_MEMORY_ID = 9,
		parameter CORE_ID        = 0 )
	(
		input                                                   clk,
		input                                                   reset,
		input                                                   enable,
		// From Network
		input         [`PORT_NUM - 1 : 1]                       tile_wr_en_in,
		input  flit_t [`PORT_NUM - 1 : 1]                       tile_flit_in,
		input         [`PORT_NUM - 1 : 1][`VC_PER_PORT - 1 : 0] tile_on_off_in ,

		// To Network
		output        [`PORT_NUM - 1 : 1]                       tile_flit_out_valid,
		output flit_t [`PORT_NUM - 1 : 1]                       tile_flit_out,
		output        [`PORT_NUM - 1 : 1][`VC_PER_PORT - 1 : 0] tile_on_off_out
	);

    localparam logic [`TOT_X_NODE_W-1 : 0] X_ADDR = TILE_ID[`TOT_X_NODE_W - 1 : 0 ];
    localparam                    logic [`TOT_Y_NODE_W-1 : 0]                     Y_ADDR                                 = TILE_ID[`TOT_Y_NODE_W + `TOT_X_NODE_W - 1 : `TOT_X_NODE_W]; //FIXME -:

	//---- Router Signals ----//
	logic                               [`VC_PER_PORT - 1 : 0]                    router_credit;
	logic                               [`VC_PER_PORT - 1 : 0]                    ni_credit;

	logic                               [`PORT_NUM - 1 : 0]                       wr_en_in;
	flit_t                              [`PORT_NUM - 1 : 0]                       flit_in;
	logic                               [`PORT_NUM - 1 : 0][`VC_PER_PORT - 1 : 0] on_off_in;
	logic                               [`PORT_NUM - 1 : 0]                       wr_en_out;
	flit_t                              [`PORT_NUM - 1 : 0]                       flit_out;
	logic                               [`PORT_NUM - 1 : 0][`VC_PER_PORT - 1 : 0] on_off_out;

	//---- Cache controller Signals ----//
	// Request
	coherence_request_message_t                                                   l1d_request;
	logic                                                                         l1d_request_valid;
	logic                                                                         l1d_request_has_data;
	tile_address_t                      [1 : 0]                                   l1d_request_destinations;
	logic                               [1 : 0]                                   l1d_request_destinations_valid;
	logic                                                                         ni_request_network_available;
	// Forwarded Request
	coherence_forwarded_message_t                                                 ni_forwarded_request;
	logic                                                                         ni_forwarded_request_valid;
	logic                                                                         l1d_forwarded_request_consumed;
	// Response from CC
	coherence_response_message_t                                                  l1d_response;
	logic                                                                         l1d_response_valid;
	logic                                                                         l1d_response_has_data;
	tile_address_t                      [1 : 0]                                   l1d_response_destinations;
	logic                               [1 : 0]                                   l1d_response_destinations_valid;
	logic                                                                         ni_response_to_cc_network_available;
	// Forward from CC
	coherence_response_message_t                                                  l1d_forwarded_request;
	logic                                                                         l1d_forwarded_request_valid;
	tile_address_t                      [1 : 0]                                   l1d_forwarded_request_destination;
	logic                                                                         ni_forwarded_request_cc_network_available;
	// Response to CC
	logic                                                                         l1d_response_consumed;
	logic                                                                         ni_response_to_cc_valid;
	coherence_response_message_t                                                  ni_response_to_cc;

	//---- Directory controller Signals ----//
	// Forwarded Request
	coherence_forwarded_message_t                                                 dc_forwarded_request;
	logic                                                                         dc_forwarded_request_valid;
	logic                               [`TILE_COUNT - 1 : 0]                     dc_forwarded_request_destinations;
	logic                                                                         ni_forwarded_request_dc_network_available;
	// Request
	coherence_request_message_t                                                   ni_request;
	logic                                                                         ni_request_valid;
	logic                                                                         dc_request_consumed;
	// Response Inject
	coherence_response_message_t                                                  dc_response;
	logic                                                                         dc_response_valid;
	logic                                                                         dc_response_has_data;
	logic                               [`TILE_COUNT - 1 : 0]                     dc_response_destinations;
	tile_address_t                                                                dc_response_destination_idx;
	logic                                                                         ni_response_dc_network_available;
	// Response Eject
	logic                                                                         ni_response_to_dc_valid;
	coherence_response_message_t                                                  ni_response_to_dc;
	logic                                                                         dc_response_to_dc_consumed;

	//---- Core Signals ----//
	// Instruction Requests
	logic                                                                         tc_instr_request_valid;
	address_t                                                                     tc_instr_request_address;
	logic                                                                         mem_instr_request_available;
	// Boot manager
	logic                                                                         hi_job_valid;
	address_t                                                                     hi_job_pc;
	thread_id_t                                                                   hi_job_thread_id;
	logic                               [`THREAD_NUMB - 1 : 0]                    hi_thread_en;

	// ---- Service signals ---- //

	// NI Signal (net) to host requests manager (core)
	service_c2n_message_t                                                         ni_n2c_mes_service;
	logic                                                                         ni_n2c_mes_service_valid;
	logic                                                                         c2n_mes_service_consumed;

	sync_account_t                                                                ni_account_mess;
	logic                                                                         ni_account_mess_valid;
	logic                                                                         sc_account_consumed;
	// Barrier core
	service_c2n_message_t                                                         n2c_release_message;
	logic                                                                         n2c_release_valid;
	logic                                                                         n2c_mes_service_consumed;

	// Boot manager
	logic                                                                         bm_n2c_mes_service_valid;
	service_c2n_message_t                                                         bm_n2c_mes_service;
	logic                                                                         bm_c2n_mes_service_consumed;

	// NI Signal (core) to net
	service_c2n_message_t                                                         c2n_mes_service;                                                                                     // message to net service
	logic                                                                         c2n_mes_service_valid;
	logic                               [`TILE_COUNT - 1 : 0]                     c2n_mes_service_destinations_valid;
	logic                                                                         c2n_mes_service_has_data;
	logic                                                                         ni_c2n_mes_service_network_available;

	// TODO DANIELE
	// Signal FIFO Scheduler
    localparam NUM_FIFO   = 4;
    localparam INDEX_FIFO = 2;

	logic                               [NUM_FIFO - 1 : 0]                        c2n_network_available;
	service_c2n_message_t               [NUM_FIFO - 1 : 0]                        c2n_message_out;
	logic                               [NUM_FIFO - 1 : 0]                        c2n_message_out_valid;
	dest_valid_t                        [NUM_FIFO - 1 : 0]                        c2n_destination_valid;


//  -----------------------------------------------------------------------
//  -- Tile - Core
//  -----------------------------------------------------------------------

	nuplus_core # (
		.TILE_ID ( TILE_ID ),
		.CORE_ID ( CORE_ID ) )
	u_nuplus_core (
		.clk                             ( clk                                 ),
		.reset                           ( reset                               ),
		.thread_en                       ( hi_thread_en                        ),
		// Host Interface
		.hi_job_valid                    ( hi_job_valid                        ),
		.hi_job_pc                       ( hi_job_pc                           ),
		.hi_job_thread_id                ( hi_job_thread_id                    ),
		//Memory interface
		.mem_instr_request_available     ( mem_instr_request_available         ),
		.tc_instr_request_valid          ( tc_instr_request_valid              ),
		.tc_instr_request_address        ( tc_instr_request_address            ),
		//From Network Interface
		.ni_request_network_available    ( ni_request_network_available        ),
		.ni_forward_network_available    ( ni_forwarded_request_cc_network_available ),
		.ni_response_network_available   ( ni_response_to_cc_network_available ),
		.ni_forwarded_request            ( ni_forwarded_request                ),
		.ni_forwarded_request_valid      ( ni_forwarded_request_valid          ),
		.ni_response                     ( ni_response_to_cc                   ),
		.ni_response_valid               ( ni_response_to_cc_valid             ),
		//To Network Interface
		.l1d_forwarded_request_consumed  ( l1d_forwarded_request_consumed      ),
		.l1d_response_consumed           ( l1d_response_consumed               ),
		.l1d_request_valid               ( l1d_request_valid                   ),
		.l1d_request                     ( l1d_request                         ),
		.l1d_request_has_data            ( l1d_request_has_data                ),
		.l1d_request_destinations        ( l1d_request_destinations            ),
		.l1d_request_destinations_valid  ( l1d_request_destinations_valid      ),
		.l1d_response_valid              ( l1d_response_valid                  ),
		.l1d_response                    ( l1d_response                        ),
		.l1d_response_has_data           ( l1d_response_has_data               ),
		.l1d_response_destinations       ( l1d_response_destinations           ),
		.l1d_response_destinations_valid ( l1d_response_destinations_valid     ),
		.l1d_forwarded_request_valid       ( l1d_forwarded_request_valid       ),
		.l1d_forwarded_request             ( l1d_forwarded_request             ),
		.l1d_forwarded_request_destination ( l1d_forwarded_request_destination ),
		// Service network interface

		.bc2n_account_valid              ( c2n_message_out_valid[1]            ),
		.bc2n_account_message            ( c2n_message_out[1]                  ),
		.bc2n_account_destination_valid  ( c2n_destination_valid[1]            ),
		.n2bc_network_available          ( c2n_network_available[1]            ),

		.n2bc_release_message            ( n2c_release_message                 ),
		.n2bc_release_valid              ( n2c_release_valid                   ),
		.n2bc_mes_service_consumed       ( n2c_mes_service_consumed            )
	) ;

//  -----------------------------------------------------------------------
//  -- Tile - Network Interface
//  -----------------------------------------------------------------------
	// The local router port is directly connected to the Network Interface. The local port is not
	// propagated to the Tile output

	assign router_credit[VC0]           = on_off_out [LOCAL ][VC0];
	assign router_credit[VC1]           = on_off_out [LOCAL ][VC1];
	assign router_credit[VC2]           = on_off_out [LOCAL ][VC2];
	assign router_credit[VC3]           = on_off_out [LOCAL ][VC3];

	assign on_off_in[LOCAL ] [VC0]      = ni_credit[VC0];
	assign on_off_in[LOCAL ] [VC1]      = ni_credit[VC1];
	assign on_off_in[LOCAL ] [VC2]      = ni_credit[VC2];
	assign on_off_in[LOCAL ] [VC3]      = ni_credit[VC3];

	network_interface_core #(
		.X_ADDR( X_ADDR ),
		.Y_ADDR( Y_ADDR )
	)
	u_network_interface_core (
		.clk                                    ( clk                                    ),
		.reset                                  ( reset                                  ),
		.enable                                 ( enable                                 ),
		//CACHE CONTROLLER INTERFACE
		//Request
		.l1d_request                            ( l1d_request                            ),
		.l1d_request_valid                      ( l1d_request_valid                      ),
		.l1d_request_has_data                   ( l1d_request_has_data                   ),
		.l1d_request_destinations               ( l1d_request_destinations               ),
		.l1d_request_destinations_valid         ( l1d_request_destinations_valid         ),
		.ni_request_network_available           ( ni_request_network_available           ),
		//Forwarded Request
		.ni_forwarded_request                   ( ni_forwarded_request                   ),
		.ni_forwarded_request_valid             ( ni_forwarded_request_valid             ),
		.l1d_forwarded_request_consumed         ( l1d_forwarded_request_consumed         ),
		.ni_forwarded_request_cc_network_available ( ni_forwarded_request_cc_network_available ),
		//Response Inject
		.l1d_response                           ( l1d_response                           ),
		.l1d_response_valid                     ( l1d_response_valid                     ),
		.l1d_response_has_data                  ( l1d_response_has_data                  ),
		.l1d_response_to_cc_valid               ( l1d_response_destinations_valid[CC_ID] ),
		.l1d_response_to_cc                     ( l1d_response_destinations[CC_ID]       ),
		.l1d_response_to_dc_valid               ( l1d_response_destinations_valid[DC_ID] ),
		.l1d_response_to_dc                     ( l1d_response_destinations[DC_ID]       ),
		.ni_response_cc_network_available       ( ni_response_to_cc_network_available    ),
		//Forward Inject
		.l1d_forwarded_request_valid            ( l1d_forwarded_request_valid              ),
		.l1d_forwarded_request                  ( l1d_forwarded_request                    ),
		.l1d_forwarded_request_destination      ( l1d_forwarded_request_destination        ),
		//Response Eject
		.ni_response_to_cc_valid                ( ni_response_to_cc_valid                ),
		.ni_response_to_cc                      ( ni_response_to_cc                      ),
		.l1d_response_to_cc_consumed            ( l1d_response_consumed                  ),

		//DIRECTORY CONTROLLER
		//Forwarded Request
		.dc_forwarded_request                   ( dc_forwarded_request                   ),
		.dc_forwarded_request_valid             ( dc_forwarded_request_valid             ),
		.dc_forwarded_request_destinations_valid( dc_forwarded_request_destinations      ),
		.ni_forwarded_request_dc_network_available ( ni_forwarded_request_dc_network_available ),
		//Request
		.ni_request                             ( ni_request                             ),
		.ni_request_valid                       ( ni_request_valid                       ),
		.dc_request_consumed                    ( dc_request_consumed                    ),
		//Response Inject
		.dc_response                            ( dc_response                            ),
		.dc_response_valid                      ( dc_response_valid                      ),
		.dc_response_has_data                   ( dc_response_has_data                   ),
		.dc_response_destination                ( dc_response_destination_idx            ),
		.ni_response_dc_network_available       ( ni_response_dc_network_available       ),
		//Response Eject
		.ni_response_to_dc_valid                ( ni_response_to_dc_valid                ),
		.ni_response_to_dc                      ( ni_response_to_dc                      ),
		.dc_response_to_dc_consumed             ( dc_response_to_dc_consumed             ),

		//VC SERVICE INTERFACE

		//Core to Net
		.c2n_mes_service                        ( c2n_mes_service                        ),
		.c2n_mes_service_valid                  ( c2n_mes_service_valid                  ),
		.c2n_mes_service_has_data               ( c2n_mes_service_has_data               ),
		.c2n_mes_service_destinations_valid     ( c2n_mes_service_destinations_valid     ),
		.ni_c2n_mes_service_network_available   ( ni_c2n_mes_service_network_available   ),
		//Net to Core
		.ni_n2c_mes_service                     ( ni_n2c_mes_service                     ),
		.ni_n2c_mes_service_valid               ( ni_n2c_mes_service_valid               ),
		.c2n_mes_service_consumed               ( c2n_mes_service_consumed               ),

		//ROUTER INTERFACE
		// flit in/out
		.ni_flit_out                            ( flit_in [LOCAL]                        ),
		.ni_flit_out_valid                      ( wr_en_in [LOCAL ]                      ),
		.router_flit_in                         ( flit_out [LOCAL ]                      ),
		.router_flit_in_valid                   ( wr_en_out [LOCAL ]                     ),
		// on-off backpressure
		.router_credit                          ( router_credit                          ),
		.ni_credit                              ( ni_credit                              )
	);

	//Utile per settare il bit has_data nella network per inviare un pacchetto pi� grande di 1 flit (64 bit)
	generate
		if ( $bits( service_c2n_message_t ) > ( `PAYLOAD_W ) )
			assign c2n_mes_service_has_data = 1'b1;
		else
			assign c2n_mes_service_has_data = 1'b0;
	endgenerate

//  -----------------------------------------------------------------------
//  -- Tile - Directory Controller
//  -----------------------------------------------------------------------

	directory_controller #(
		.TILE_ID       ( TILE_ID        ),
		.TILE_MEMORY_ID( TILE_MEMORY_ID )
	)
	u_directory_controller (
		.clk                                   ( clk                                    ),
		.reset                                 ( reset                                  ),
		//From Thread Controller
		.tc_instr_request_valid                ( tc_instr_request_valid                 ),
		.tc_instr_request_address              ( tc_instr_request_address               ),
		//To Thread Controller
		.mem_instr_request_available           ( mem_instr_request_available            ),
		//From Network Interface
		.ni_response_network_available         ( ni_response_dc_network_available       ),
		.ni_forwarded_request_network_available( ni_forwarded_request_dc_network_available ),
		.ni_request_valid                      ( ni_request_valid                       ),
		.ni_request                            ( ni_request                             ),
		.ni_response_valid                     ( ni_response_to_dc_valid                ),
		.ni_response                           ( ni_response_to_dc                      ),
		//To Network Interface
		.dc_request_consumed                   ( dc_request_consumed                    ),
		.dc_response_consumed                  ( dc_response_to_dc_consumed             ),
		.dc_forwarded_request                  ( dc_forwarded_request                   ),
		.dc_forwarded_request_valid            ( dc_forwarded_request_valid             ),
		.dc_forwarded_request_destinations     ( dc_forwarded_request_destinations      ),
		.dc_response                           ( dc_response                            ),
		.dc_response_valid                     ( dc_response_valid                      ),
		.dc_response_has_data                  ( dc_response_has_data                   ),
		.dc_response_destinations              ( dc_response_destinations               )
	);

	oh_to_idx #(
		.NUM_SIGNALS( `TILE_COUNT             ),
		.DIRECTION  ( "LSB0"                  ),
		.INDEX_WIDTH( $bits( tile_address_t ) )
	)
	dc_destination_oh_to_idx (
		.one_hot( dc_response_destinations    ),
		.index  ( dc_response_destination_idx )
	);

//  -----------------------------------------------------------------------
//  -- Tile - Boot Manager
//  -----------------------------------------------------------------------

	boot_manager #(
		.TILE_ID( TILE_ID )
	)
	u_boot_manager (
		.clk                      ( clk                         ),
		.reset                    ( reset                       ),
		.leds_state               (                             ),
		// to net
		.network_available        ( c2n_network_available[0]    ),
		.message_out              ( c2n_message_out[0]          ),
		.message_out_valid        ( c2n_message_out_valid[0]    ),
		.destination_valid        ( c2n_destination_valid[0]    ),
		// from net
		.n2c_mes_service_consumed ( bm_c2n_mes_service_consumed ),
		.message_in               ( bm_n2c_mes_service          ),
		.message_in_valid         ( bm_n2c_mes_service_valid    ),
		// to nu+ core
		.hi_thread_en             ( hi_thread_en                ),
		.hi_job_valid             ( hi_job_valid                ),
		.hi_job_pc                ( hi_job_pc                   ),
		.hi_job_thread_id         ( hi_job_thread_id            )
	);

//  -----------------------------------------------------------------------
//  -- Tile -  Message service scheduler
//  -----------------------------------------------------------------------
	assign c2n_destination_valid[3]     = 0;
	assign c2n_message_out[3]           = 0;
	assign c2n_message_out_valid[3]     = 0;

	c2n_service_scheduler #(
		.NUM_FIFO  ( NUM_FIFO   ), // Set for more FIFO on VN Service, Core to Network
		.INDEX_FIFO( INDEX_FIFO )
	)
	u_c2n_service_scheduler (
		.clk                  ( clk                                  ),
		.reset                ( reset                                ),
		//From Tile
		.c2n_destination_valid( c2n_destination_valid                ),
		.c2n_message_out      ( c2n_message_out                      ),
		.c2n_message_out_valid( c2n_message_out_valid                ),
		.c2n_network_available( c2n_network_available                ),
		//To Virtual Network
		.destination_valid    ( c2n_mes_service_destinations_valid   ),
		.message_out          ( c2n_mes_service                      ),
		.message_out_valid    ( c2n_mes_service_valid                ),
		.network_available    ( ni_c2n_mes_service_network_available )

	);

//  -----------------------------------------------------------------------
//  -- Tile - Arbiter Service Message
//  -----------------------------------------------------------------------

	always_comb begin
		bm_n2c_mes_service_valid = 0;;
		ni_account_mess_valid    = 0;
		n2c_release_valid        = 0;
		c2n_mes_service_consumed = 0;
		bm_n2c_mes_service       = 0;
		ni_account_mess          = 0;
		n2c_release_message      = 0;
//      dsu_n2c_mes_service_valid<= 0;

		if( ni_n2c_mes_service.message_type == BOOT ) begin
			bm_n2c_mes_service.data  = ni_n2c_mes_service.data;
			bm_n2c_mes_service_valid = ni_n2c_mes_service_valid;
			c2n_mes_service_consumed = bm_c2n_mes_service_consumed;

//      end else if (ni_n2c_mes_service.message_type == DEBUG) begin
//          dsu_n2c_mes_service.data    <= ni_n2c_mes_service.data;
//          dsu_n2c_mes_service_valid   <= ni_n2c_mes_service_valid;
//          c2n_mes_service_consumed    <= dsu_c2n_mes_service_consumed;

		end else begin

			//      if ( ni_n2c_mes_service.data[1 : 0] == SETUP )begin
			//          ni_setup_mess_valid            = ni_n2c_mes_service_valid;
			//          ni_setup_mess.id_barrier       = ni_n2c_mes_service.data[$clog2( `BARRIER_NUMB ) + 1 : 2];
			//          ni_setup_mess.is_master        = ni_n2c_mes_service.data[`SETUP_IS_MASTER];
			//          ni_setup_mess.cnt_setup        = ni_n2c_mes_service.data[`SETUP_IS_MASTER + $clog2( `TILE_COUNT ) : `SETUP_IS_MASTER + 1];
			//          c2n_mes_service_consumed       = sc_setup_consumed;
			if ( ni_n2c_mes_service.data[1 : 0] == ACCOUNT ) begin
				ni_account_mess.cnt_setup      = ni_n2c_mes_service.data[$bits( cnt_barrier_t )+$bits( barrier_t )+$clog2( `TILE_COUNT )+1:$bits( barrier_t )+$clog2( `TILE_COUNT )+2];
				ni_account_mess.tile_id_source = ni_n2c_mes_service.data[$clog2( `TILE_COUNT ) + 1 : 2];
				ni_account_mess.id_barrier     = ni_n2c_mes_service.data[$bits( barrier_t ) + $clog2( `TILE_COUNT ) + 1 : $clog2( `TILE_COUNT ) + 2];
				ni_account_mess_valid          = ni_n2c_mes_service_valid;
				c2n_mes_service_consumed       = sc_account_consumed;
			end else if ( ni_n2c_mes_service.data[1 : 0] == RELEASE ) begin
				n2c_release_message.data       = {ni_n2c_mes_service.data[$bits( message_data_t ) - 2 : 2], 2'b10};
				n2c_release_valid              = ni_n2c_mes_service_valid;
				c2n_mes_service_consumed       = n2c_mes_service_consumed;
			end
		end
	end

`ifdef DIRECTORY_BARRIER

//  -----------------------------------------------------------------------
//  -- Tile -  Directory Synchronization Core
//  -----------------------------------------------------------------------
	sync_release_t release_out_tmp;

	synchronization_core #(
		.TILE_ID( TILE_ID ) )
	u_synchronization_core (
		.clk                   ( clk                      ),
		.reset                 ( reset                    ),
		//NETWORK INTERFACE
		//Account
		.ni_account_mess       ( ni_account_mess          ),
		.ni_account_mess_valid ( ni_account_mess_valid    ),
		.ss1_account_consumed  ( sc_account_consumed      ),
		//Release
		.ss3_release_mess      ( release_out_tmp          ),// c2n_message_out[2]       ), XXX: Wrapper per multicast, WARNING, TO_DO
		.ss3_release_valid     ( c2n_message_out_valid[2] ),
		.ni_available          ( c2n_network_available[2] ) //controllare questo segnale con MIRKO
	);
	assign c2n_message_out[2].message_type                     = SYNC;
	assign c2n_message_out[2].data[1 : 0]                      = RELEASE;
	assign c2n_message_out[2].data[$bits( barrier_t ) + 1 : 2] = release_out_tmp.id_barrier;
	assign c2n_destination_valid[2]                            = release_out_tmp.mask_slave;
`else
//  -----------------------------------------------------------------------
//  -- Core Sync - Centralized Synchronization Core
//  -----------------------------------------------------------------------
	generate
		if ( TILE_ID == `CENTRAL_SYNCH_ID ) begin

			sync_release_t release_out_tmp;

			synchronization_core #(
				`ifdef DISPLAY_SYNC
				.TILE_ID( TILE_ID )
					`endif
			)
			u_synchronization_core (
				.clk                   ( clk                      ),
				.reset                 ( reset                    ),
				//NETWORK INTERFACE
				//Account
				.ni_account_mess       ( ni_account_mess          ),
				.ni_account_mess_valid ( ni_account_mess_valid    ),
				.ss1_account_consumed  ( sc_account_consumed      ),
				//Release
				.ss3_release_mess      ( release_out_tmp          ),// c2n_message_out[2]       ), XXX: Wrapper per multicast, WARNING, TO_DO
				.ss3_release_valid     ( c2n_message_out_valid[2] ),
				.ni_available          ( c2n_network_available[2] ) //controllare questo segnale con MIRKO
			);
			assign c2n_message_out[2].message_type                     = SYNC;
			assign c2n_message_out[2].data[1 : 0]                      = RELEASE;
			assign c2n_message_out[2].data[$bits( barrier_t ) + 1 : 2] = release_out_tmp.id_barrier;
			assign c2n_destination_valid[2]                            = release_out_tmp.mask_slave;
		end else begin

			assign c2n_destination_valid[2]                            = 0;
			assign c2n_message_out[2]                                  = 0;
			assign c2n_message_out_valid[2]                            = 0;
		end
	endgenerate
`endif

//  -----------------------------------------------------------------------
//  -- Tile - Router
//  -----------------------------------------------------------------------
	// All router port are directly connected to the tile output. Instead, the local port
	// is connected to the Network Interface.
	assign tile_flit_out_valid          = wr_en_out [`PORT_NUM - 1 : 1];
	assign tile_flit_out                = flit_out[`PORT_NUM - 1 : 1 ];
	assign tile_on_off_out              = on_off_out[`PORT_NUM - 1 : 1];
	assign flit_in[`PORT_NUM - 1 : 1 ]  = tile_flit_in;
	assign wr_en_in[`PORT_NUM - 1 : 1 ] = tile_wr_en_in;
	assign on_off_in[`PORT_NUM - 1 : 1] = tile_on_off_in;

	router # (
		.MY_X_ADDR ( X_ADDR ),
		.MY_Y_ADDR ( Y_ADDR )
	)
	u_router (
		.wr_en_in   ( wr_en_in   ),
		.flit_in    ( flit_in    ),
		.on_off_in  ( on_off_in  ),
		.wr_en_out  ( wr_en_out  ),
		.flit_out   ( flit_out   ),
		.on_off_out ( on_off_out ),
		.clk        ( clk        ),
		.reset      ( reset      )
	);

`ifdef SIMULATION

	always_ff @( posedge clk )
		if ( !reset & dc_response_valid )
			assert( $onehot( dc_response_destinations ) ) else $fatal( "Non OH!" );

`endif

endmodule
