`include "nuplus_user_defines.sv"
`include "nuplus_network_defines.sv"
`include "nuplus_message_service_defines.sv"
`include "nuplus_coherence_defines.sv"

`ifdef DISPLAY_REQUESTS_MANAGER
`include "nuplus_debug_log.sv"
`endif

module tile_h2c # (
		parameter TILE_ID        = 0,
		parameter TILE_MEMORY_ID = 0,
		parameter ITEM_w = 32 ) // Input and output item width (control interface)
	(
		input clk,
		input reset,
		input enable,

		// From Network
		input        [`PORT_NUM - 1 : 1]                       tile_wr_en_in,
		input flit_t [`PORT_NUM - 1 : 1]                       tile_flit_in,
		input        [`PORT_NUM - 1 : 1][`VC_PER_PORT - 1 : 0] tile_on_off_in,

		// To Network
		output        [`PORT_NUM - 1 : 1]                       tile_flit_out_valid,
		output flit_t [`PORT_NUM - 1 : 1]                       tile_flit_out,
		output        [`PORT_NUM - 1 : 1][`VC_PER_PORT - 1 : 0] tile_on_off_out,


		// Interface to Host
		input  [ITEM_w - 1 : 0] item_data_i,  // Input: items from outside
		input                   item_valid_i, // Input: valid signal associated with item_data_i port
		output                  item_avail_o, // Output: avail signal to input port item_data_i
		output [ITEM_w - 1 : 0] item_data_o,  // Output: items to outside
		output                  item_valid_o, // Output: valid signal associated with item_data_o port
		input                   item_avail_i  // Input: avail signal to output port item_data_o
	//output logic                                          hit_breakpoint
	);

    localparam logic [`TOT_X_NODE_W-1 : 0] X_ADDR = TILE_ID[`TOT_X_NODE_W-1 : 0];
    localparam logic [`TOT_Y_NODE_W-1 : 0] Y_ADDR = TILE_ID[`TOT_Y_NODE_W + `TOT_X_NODE_W -1 : `TOT_X_NODE_W]; //FIXME -:


	//---- Router Signals ----//
	logic [`VC_PER_PORT - 1 : 0] router_credit;
	logic [`VC_PER_PORT - 1 : 0] ni_credit;

	logic  [`PORT_NUM - 1 : 0]                       wr_en_in;
	flit_t [`PORT_NUM - 1 : 0]                       flit_in;
	logic  [`PORT_NUM - 1 : 0][`VC_PER_PORT - 1 : 0] on_off_in;
	logic  [`PORT_NUM - 1 : 0]                       wr_en_out;
	flit_t [`PORT_NUM - 1 : 0]                       flit_out;
	logic  [`PORT_NUM - 1 : 0][`VC_PER_PORT - 1 : 0] on_off_out;

	//---- Directory controller Signals ----//
	// Forwarded Request
	coherence_forwarded_message_t                       dc_forwarded_request;
	logic                                               dc_forwarded_request_valid;
	logic                         [`TILE_COUNT - 1 : 0] dc_forwarded_request_destinations;
	logic                                               ni_forwarded_request_network_available;
	// Request
	coherence_request_message_t                         ni_request;
	logic                                               ni_request_valid;
	logic                                               dc_request_consumed;
	// Response Inject
	coherence_response_message_t                        dc_response;
	logic                                               dc_response_valid;
	logic                                               dc_response_has_data;
	logic                         [`TILE_COUNT - 1 : 0] dc_response_destinations;
	tile_address_t                                      dc_response_destination_idx;
	logic                                               ni_response_dc_network_available;
	// Response Eject
	logic                                               ni_response_to_dc_valid;
	coherence_response_message_t                        ni_response_to_dc;
	logic                                               dc_response_to_dc_consumed;

	//---- Service Signals ----//
	// From Core to NI
	service_c2n_message_t c2n_mes_service;
	logic                 c2n_response_valid;
	dest_valid_t          c2n_mes_service_destinations_valid;
	logic                 c2n_has_data;
	logic                 ni_response_network_available;
	// From NI to Core
	service_c2n_message_t ni_n2c_mes_service;
	logic                 ni_n2c_mes_service_valid;
	logic                 core_n2c_mes_service_consumed;

	//---- Arbiter Signals ----//
	logic boot_avail, sync_avail;//, dsu_avail;
	logic can_boot, can_sync;    //, can_dsu;

	//---- Host manager Signals ----//
	service_c2n_message_t                  hm_c2n_mes_service, sync_c2n_mes_service;                          //, dsu_c2n_mes_service;
	logic                                  hm_c2n_response_valid, sync_c2n_response_valid;                    //, dsu_c2n_response_valid;
	logic                                  hm_arbiter_valid, sync_setup_valid;                                //, dsu_enable;
	logic                                  hm_c2n_has_data, sync_c2n_has_data;                                //, dsu_c2n_has_data;
	dest_valid_t                           c2n_mes_boot_destinations_valid, c2n_mes_setup_destinations_valid; //, c2n_mes_dsu_destinations_valid;
	logic                                  core_n2c_boot_mes_consumed;                                        //, core_n2c_dsu_mes_consumed;
	//---- Host Interface Signals ----//
	logic                                  hm_item_avail_o, hm_item_avail_i, hm_item_valid_i, hm_item_valid_o;
	logic                                  sm_item_avail_o, sm_item_avail_i, sm_item_valid_i, sm_item_valid_o;
	//logic                                                                           dsu_item_avail_o, dsu_item_avail_i, dsu_item_valid_i, dsu_item_valid_o;
	logic                                  wait_boot, wait_sync;                                              //, wait_dsu;
	logic                 [ITEM_w - 1 : 0] hm_item_data_i, hm_item_data_o, sm_item_data_i, sm_item_data_o;    //, dsu_item_data_i, dsu_item_data_o;

//  -----------------------------------------------------------------------
//  -- Tile H2C - Arbiter
//  -----------------------------------------------------------------------

	assign can_boot                     = hm_arbiter_valid & ni_response_network_available;
	assign can_sync                     = sync_setup_valid & ni_response_network_available;
	//assign can_dsu                    = dsu_enable & ni_response_network_available;
	assign core_n2c_mes_service_consumed= core_n2c_boot_mes_consumed;// | core_n2c_dsu_mes_consumed;

	always_comb begin

		boot_avail                         = 1'b0;
		sync_avail                         = 1'b0;
		//      dsu_avail                          = 1'b0;
		c2n_mes_service                    = 0;
		c2n_response_valid                 = 1'b0;
		c2n_has_data                       = 1'b0;
		c2n_mes_service_destinations_valid = 0;

		if( can_boot )begin
			boot_avail                         = 1'b1;
			c2n_mes_service                    = hm_c2n_mes_service;
			c2n_response_valid                 = hm_c2n_response_valid;
			c2n_has_data                       = hm_c2n_has_data;
			c2n_mes_service_destinations_valid = c2n_mes_boot_destinations_valid;
		end else if ( can_sync ) begin
			sync_avail                         = 1'b1;
			c2n_mes_service                    = sync_c2n_mes_service;
			c2n_response_valid                 = sync_c2n_response_valid;
			c2n_has_data                       = sync_c2n_has_data;
			c2n_mes_service_destinations_valid = c2n_mes_setup_destinations_valid;
		//      end else if ( can_dsu ) begin
		//          dsu_avail                          = 1'b1;
		//          c2n_mes_service                    = dsu_c2n_mes_service;
		//          c2n_response_valid                 = dsu_c2n_response_valid;
		//          c2n_has_data                       = dsu_c2n_has_data;
		//          c2n_mes_service_destinations_valid = c2n_mes_dsu_destinations_valid;
		end
	end

//  -----------------------------------------------------------------------
//  -- Tile H2C - Host Interface
//  -----------------------------------------------------------------------
	assign wait_sync = 1'b0;

	host_interface #(
		.TILE_ID( TILE_ID ),
		.ITEM_w ( ITEM_w  )
	)
	u_host_interface (
		.clk       ( clk       ),
		.wait_boot ( wait_boot ),
		.wait_sync ( wait_sync ),
		.wait_dsu  ( 1'b0      ),//( wait_dsu            ),

		.hm_item_avail_i ( hm_item_avail_i ),
		.hm_item_avail_o ( hm_item_avail_o ),
		.hm_item_data_i  ( hm_item_data_i  ),
		.hm_item_data_o  ( hm_item_data_o  ),
		.hm_item_valid_i ( hm_item_valid_i ),
		.hm_item_valid_o ( hm_item_valid_o ),

		.sm_item_avail_i ( sm_item_avail_i ),
		.sm_item_avail_o ( sm_item_avail_o ),
		.sm_item_data_i  ( sm_item_data_i  ),
		.sm_item_data_o  ( sm_item_data_o  ),
		.sm_item_valid_i ( sm_item_valid_i ),
		.sm_item_valid_o ( sm_item_valid_o ),

		.dsu_item_avail_i (              ),//( dsu_item_avail_i  ),
		.dsu_item_avail_o ( 1'b0         ),//( dsu_item_avail_o  ),
		.dsu_item_data_i  (              ),//( dsu_item_data_i   ),
		.dsu_item_data_o  ( ITEM_w'( 0 ) ),//( dsu_item_data_o   ),
		.dsu_item_valid_i (              ),//( dsu_item_valid_i  ),
		.dsu_item_valid_o ( 1'b0         ),//( dsu_item_valid_o  ),

		.item_avail_i ( item_avail_i ), //Input: avail signal to output port item_data_o
		.item_avail_o ( item_avail_o ), //Output: avail signal to input port item_data_i
		//Interface to Host
		.item_data_i  ( item_data_i  ), //Input: items from outside
		.item_data_o  ( item_data_o  ), //Output: items to outside
		.item_valid_i ( item_valid_i ), //Input: valid signal associated with item_data_i port
		.item_valid_o ( item_valid_o )  //Output: valid signal associated with item_data_o port
	);

//  -----------------------------------------------------------------------
//  -- Tile H2C - Host Request Manager
//  -----------------------------------------------------------------------

	host_request_manager #(
		.TILE_ID( TILE_ID ),
		.ITEM_w ( ITEM_w  )
	)
	u_host_request_manager (
		.clk                               ( clk                             ),
		.reset                             ( reset                           ),
		.leds_state                        (                                 ),
		//From/To host
		.item_data_i                       ( hm_item_data_i                  ),
		.item_valid_i                      ( hm_item_valid_i                 ),
		.item_avail_o                      ( hm_item_avail_o                 ),
		.item_data_o                       ( hm_item_data_o                  ),
		.item_valid_o                      ( hm_item_valid_o                 ),
		.item_avail_i                      ( hm_item_avail_i                 ),
		.wait_boot_signal                  ( wait_boot                       ),
		//From/To Net
		.valid_arbiter                     ( hm_arbiter_valid                ),
		.c2n_mes_service                   ( hm_c2n_mes_service              ),
		.c2n_mes_valid                     ( hm_c2n_response_valid           ),
		.c2n_has_data                      ( hm_c2n_has_data                 ),
		.n2c_mes_service                   ( ni_n2c_mes_service              ),
		.n2c_mes_valid                     ( ni_n2c_mes_service_valid        ),
		.n2c_mes_service_consumed          ( core_n2c_boot_mes_consumed      ),
		.ni_network_available              ( boot_avail                      ),
		.c2n_mes_service_destinations_valid( c2n_mes_boot_destinations_valid )
	);

//  -----------------------------------------------------------------------
//  -- Tile H2C - Boot setup
//  -----------------------------------------------------------------------
	
	/*boot_setup #(
		.ITEM_w( ITEM_w )
	)
	u_boot_setup (
		.clk                             ( clk                              ),
		.reset                           ( reset                            ),
		.c2n_has_data                    ( sync_c2n_has_data                ),
		.c2n_mes_setup                   ( sync_c2n_mes_service             ),
		.c2n_mes_setup_destinations_valid( c2n_mes_setup_destinations_valid ),
		.c2n_mes_valid                   ( sync_c2n_response_valid          ),
		.ni_network_available            ( sync_avail                       ),
		.sm_item_avail_i                 ( sm_item_avail_i                  ),
		.sm_item_avail_o                 ( sm_item_avail_o                  ),
		.sm_item_data_i                  ( sm_item_data_i                   ),
		.sm_item_data_o                  ( sm_item_data_o                   ),
		.sm_item_valid_i                 ( sm_item_valid_i                  ),
		//Host interface
		.sm_item_valid_o                 ( sm_item_valid_o                  ),
		//Vitual Network
		.valid_arbiter                   ( sync_setup_valid                 ),
		.wait_sync                       (                         )
	);*/

//  -----------------------------------------------------------------------
//  -- Tile H2C - Debugger Request Manager
//  -----------------------------------------------------------------------

	//  debugger_request_manager # (
	//      .TILE_ID (TILE_ID) ,
	//      .ITEM_w  (ITEM_w )
	//  )
	//  debugger_request_manager (
	//      .clk                                (clk                               ) ,
	//      .reset                              (reset                             ) ,
	//      //Interface to Host
	//      .item_data_i                        (dsu_item_data_i                   ) , //Input: items from outside
	//      .item_valid_i                       (dsu_item_valid_i                  ) , //Input: valid signal associated with item_data_i port
	//      .item_avail_o                       (dsu_item_avail_o                  ) , //Output: avail signal to input port item_data_i
	//      .item_data_o                        (dsu_item_data_o                   ) , //Output: items to outside
	//      .item_valid_o                       (dsu_item_valid_o                  ) , //Output: valid signal associated with item_data_o port
	//      .item_avail_i                       (dsu_item_avail_i                  ) , //Input: avail signal to ouput port item_data_o
	//
	//      .hit_breakpoint                     (hit_breakpoint                    ) ,
	//      //To host interface component
	//      .wait_dsu                           (wait_dsu                          ) ,
	//      .valid_arbiter                      (dsu_enable                        ) ,
	//      //Network Interface
	//      .n2c_mes_service                    (ni_n2c_mes_service                ) ,
	//      .n2c_mes_valid                      (ni_n2c_mes_service_valid          ) ,
	//      .ni_network_available               (dsu_avail                         ) ,
	//      .n2c_mes_service_consumed           (core_n2c_dsu_mes_consumed         ) ,
	//      .c2n_mes_service                    (dsu_c2n_mes_service               ) ,
	//      .c2n_mes_valid                      (dsu_c2n_response_valid            ) ,
	//      .c2n_has_data                       (dsu_c2n_has_data                  ) ,
	//      .c2n_mes_service_destinations_valid (c2n_mes_dsu_destinations_valid    )
	//
	//
	//  ) ;

//  -----------------------------------------------------------------------
//  -- Tile H2C - Network Interface
//  -----------------------------------------------------------------------
	// The local router port is directly connected to the Network Interface. The local port is not
	// propagated to the Tile output
	assign router_credit[VC0] = on_off_out [LOCAL ][VC0];
	assign router_credit[VC1] = on_off_out [LOCAL ][VC1];
	assign router_credit[VC2] = on_off_out [LOCAL ][VC2];
	assign router_credit[VC3] = on_off_out [LOCAL ][VC3];

	assign on_off_in[LOCAL ] [VC0] = ni_credit[VC0];
	assign on_off_in[LOCAL ] [VC1] = ni_credit[VC1];
	assign on_off_in[LOCAL ] [VC2] = ni_credit[VC2];
	assign on_off_in[LOCAL ] [VC3] = ni_credit[VC3];

	network_interface_core #(
		.X_ADDR( X_ADDR ),
		.Y_ADDR( Y_ADDR )
	)
	u_network_interface_core (
		.clk    ( clk    ),
		.reset  ( reset  ),
		.enable ( enable ),

		// SERVICE
		//Core to Net
		.c2n_mes_service                      ( c2n_mes_service                    ),
		.c2n_mes_service_valid                ( c2n_response_valid                 ),
		.c2n_mes_service_has_data             ( c2n_has_data                       ),
		//.c2n_mes_service_destinations           ( c2n_mes_service.destination            ),
		.c2n_mes_service_destinations_valid   ( c2n_mes_service_destinations_valid ),
		.ni_c2n_mes_service_network_available ( ni_response_network_available      ),
		//Net to Core
		.ni_n2c_mes_service                   ( ni_n2c_mes_service                 ),
		.ni_n2c_mes_service_valid             ( ni_n2c_mes_service_valid           ),
		.c2n_mes_service_consumed             ( core_n2c_mes_service_consumed      ),

		//CACHE CONTROLLER INTERFACE
		//Request
		.l1d_request                      (      ),
		.l1d_request_valid                ( 1'b0 ),
		.l1d_request_has_data             (      ),
		.l1d_request_destinations         (      ),
		.l1d_request_destinations_valid   (      ),
		.ni_request_network_available     (      ),
		//Forwarded Request
		.ni_forwarded_request             (      ),
		.ni_forwarded_request_valid       (      ),
		.l1d_forwarded_request_consumed   ( 1'b0 ),
		//Response Inject
		.l1d_response                     (      ),
		.l1d_response_valid               ( 1'b0 ),
		.l1d_response_has_data            (      ),
		.l1d_response_to_cc_valid         (      ),
		.l1d_response_to_cc               (      ),
		.l1d_response_to_dc_valid         (      ),
		.l1d_response_to_dc               (      ),
		.ni_response_cc_network_available (      ),
		//Response Eject
		.ni_response_to_cc_valid          (      ),
		.ni_response_to_cc                (      ),
		.l1d_response_to_cc_consumed      ( 1'b0 ),

		//DIRECTORY CONTROLLER
		//Forwarded Request
		.dc_forwarded_request                   ( dc_forwarded_request                   ),
		.dc_forwarded_request_valid             ( dc_forwarded_request_valid             ),
		.dc_forwarded_request_destinations_valid( dc_forwarded_request_destinations      ),
		.ni_forwarded_request_dc_network_available ( ni_forwarded_request_network_available ),
		//Request
		.ni_request                             ( ni_request                             ),
		.ni_request_valid                       ( ni_request_valid                       ),
		.dc_request_consumed                    ( dc_request_consumed                    ),
		//Response Inject
		.dc_response                            ( dc_response                            ),
		.dc_response_valid                      ( dc_response_valid                      ),
		.dc_response_has_data                   ( dc_response_has_data                   ),
		.dc_response_destination                ( dc_response_destination_idx            ),
		.ni_response_dc_network_available       ( ni_response_dc_network_available       ),
		//Response Eject
		.ni_response_to_dc_valid                ( ni_response_to_dc_valid                ),
		.ni_response_to_dc                      ( ni_response_to_dc                      ),
		.dc_response_to_dc_consumed             ( dc_response_to_dc_consumed             ),

		//ROUTER INTERFACE
		// flit in/out
		.ni_flit_out          ( flit_in [LOCAL]    ),
		.ni_flit_out_valid    ( wr_en_in [LOCAL ]  ),
		.router_flit_in       ( flit_out [LOCAL ]  ),
		.router_flit_in_valid ( wr_en_out [LOCAL ] ),
		// on-off backpressure
		.router_credit        ( router_credit      ),
		.ni_credit            ( ni_credit          )
	);

//  -----------------------------------------------------------------------
//  -- Tile H2C - Directory Controller
//  -----------------------------------------------------------------------

	directory_controller #(
		.TILE_ID       ( TILE_ID        ),
		.TILE_MEMORY_ID( TILE_MEMORY_ID )
	)
	u_directory_controller (
		.clk                                   ( clk                                    ),
		.reset                                 ( reset                                  ),
		//From Thread Controller
		.tc_instr_request_valid                ( 1'b0                                   ),
		.tc_instr_request_address              (                                        ),
		//To Thread Controller
		.mem_instr_request_available           (                                        ),
		//From Network Interface
		.ni_response_network_available         ( ni_response_dc_network_available       ),
		.ni_forwarded_request_network_available( ni_forwarded_request_network_available ),
		.ni_request_valid                      ( ni_request_valid                       ),
		.ni_request                            ( ni_request                             ),
		.ni_response_valid                     ( ni_response_to_dc_valid                ),
		.ni_response                           ( ni_response_to_dc                      ),
		//To Network Interface
		.dc_request_consumed                   ( dc_request_consumed                    ),
		.dc_response_consumed                  ( dc_response_to_dc_consumed             ),
		.dc_forwarded_request                  ( dc_forwarded_request                   ),
		.dc_forwarded_request_valid            ( dc_forwarded_request_valid             ),
		.dc_forwarded_request_destinations     ( dc_forwarded_request_destinations      ),
		.dc_response                           ( dc_response                            ),
		.dc_response_valid                     ( dc_response_valid                      ),
		.dc_response_has_data                  ( dc_response_has_data                   ),
		.dc_response_destinations              ( dc_response_destinations               )
	);

	oh_to_idx #(
		.NUM_SIGNALS( `TILE_COUNT             ),
		.DIRECTION  ( "LSB0"                  ),
		.INDEX_WIDTH( $bits( tile_address_t ) )
	)
	dc_destination_oh_to_idx (
		.one_hot( dc_response_destinations    ),
		.index  ( dc_response_destination_idx )
	);

//  -----------------------------------------------------------------------
//  -- Tile H2C - Router
//  -----------------------------------------------------------------------

	// All router port are directly connected to the tile output. Instead, the local port
	// is connected to the Network Interface.
	assign tile_flit_out_valid          = wr_en_out [`PORT_NUM - 1 : 1];
	assign tile_flit_out                = flit_out[`PORT_NUM - 1 : 1 ];
	assign tile_on_off_out              = on_off_out[`PORT_NUM - 1 : 1];
	assign flit_in[`PORT_NUM - 1 : 1 ]  = tile_flit_in;
	assign wr_en_in[`PORT_NUM - 1 : 1 ] = tile_wr_en_in;
	assign on_off_in[`PORT_NUM - 1 : 1] = tile_on_off_in;

	router #(
		.MY_X_ADDR( X_ADDR ),
		.MY_Y_ADDR( Y_ADDR )
	)
	u_router (
		.wr_en_in  ( wr_en_in   ),
		.flit_in   ( flit_in    ),
		.on_off_in ( on_off_in  ),
		.wr_en_out ( wr_en_out  ),
		.flit_out  ( flit_out   ),
		.on_off_out( on_off_out ),
		.clk       ( clk        ),
		.reset     ( reset      )
	);

`ifdef DISPLAY_REQUESTS_MANAGER
	int file;

	initial file = $fopen( `DISPLAY_REQ_MANAGER_FILE, "wb" );

	final $fclose( file );

`endif

endmodule
