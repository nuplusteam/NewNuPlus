`include "nuplus_network_defines.sv"

module control_unit_flit_to_packet #(
		parameter PACKET_BODY_SIZE = 256,
		parameter FLIT_NUMB        = 9 )
	(
		input                                    clk,
		input                                    reset,
		input                                    enable,
		
//		input 		        packet_alm_fifo_full,
//		input               core_packet_consumed,
//		output logic		credit_out,

		// From Router
		input  logic                             router_flit_valid,
		input  flit_t                            router_flit_in,

		// To rebuilt packet logic
		output logic  [PACKET_BODY_SIZE - 1 : 0] cu_rebuilt_packet,
		output logic                             cu_packet_rebuilt_compl,
		output logic                             cu_is_for_cc,
		output logic                             cu_is_for_dc
	);

	
	logic       [$clog2( FLIT_NUMB ) - 1 : 0] count;

	flit_body_t [FLIT_NUMB - 1 : 0]           rebuilt_packet;
	
	assign cu_rebuilt_packet = rebuilt_packet;
	
	always_ff @ ( posedge clk, posedge reset ) begin
		if ( reset ) begin
			cu_packet_rebuilt_compl <= 1'b0;
			rebuilt_packet       <= '{default: '0};
			cu_is_for_cc            <= 1'b0;
			cu_is_for_dc            <= 1'b0;
			count <= '{default: '0};
		end else begin
			
			cu_packet_rebuilt_compl <= 1'b0;
			if (enable) begin
			
				if (router_flit_valid) begin
					rebuilt_packet[count] <= router_flit_in.payload;
					
					if (router_flit_in.flit_type == TAIL || router_flit_in.flit_type == HT) begin
						count <= '{default: '0};
						cu_packet_rebuilt_compl <= 1'b1;
					end else
						count <= count + 1;
						
					if (router_flit_in.flit_type == HEADER || router_flit_in.flit_type == HT) begin
						cu_is_for_cc <= router_flit_in.core_destination == TO_CC;
						cu_is_for_dc <= router_flit_in.core_destination == TO_DC;
					end 
					
				end

			end
		end
	end
	
// VERSIONE CHE VOLEVA RISPARMIARE , MA HA PROBLEMI SE CI SONO FLIT HEAD-TAIL IN SEQUENZA
	
	//  -----------------------------------------------------------------------
	//  -- Control Unit - Next State Block
	//  -----------------------------------------------------------------------

//	typedef enum logic {ACTIVE, FULL} state_t;
//	state_t                                       state;
//	
//	always_ff @ ( posedge clk, posedge reset ) begin
//		if ( reset ) begin
//			cu_packet_rebuilt_compl <= 1'b0;
//			rebuilt_packet       <= '{default: '0};
//			cu_is_for_cc            <= 1'b0;
//			cu_is_for_dc            <= 1'b0;
//			count <= '{default: '0};
//			credit_out <= 1'b1;
//		end else begin
//			cu_packet_rebuilt_compl <= 1'b0;
//			unique case ( state )
//				ACTIVE     : begin
//					if (enable) begin
//						credit_out <= 1'b1;
//						if (router_flit_valid) begin
//							rebuilt_packet[count] <= router_flit_in.payload;
//							
//							if (router_flit_in.flit_type == TAIL || router_flit_in.flit_type == HT) begin
//								
//								count <= '{default: '0};
//								cu_packet_rebuilt_compl <= 1'b1;
//								if (packet_alm_fifo_full) begin
//									state <= FULL;
//									credit_out <= 1'b0;
//								end
//								
//							end else
//								count <= count + 1;
//								
//							if (router_flit_in.flit_type == HEADER || router_flit_in.flit_type == HT) begin
//								cu_is_for_cc <= router_flit_in.core_destination == TO_CC;
//								cu_is_for_dc <= router_flit_in.core_destination == TO_DC;
//							end 
//							
//						end
//		
//					end else credit_out <= 1'b0;
//				end
//
//				FULL : begin
//					if (~core_packet_consumed & packet_alm_fifo_full)
//						state <= FULL;
//					else begin
//						credit_out <= 1'b1;
//						state <= ACTIVE;
//					end
//					
//
//				end
//
//				default : state <= ACTIVE;
//			endcase
//		end
//	end
	

endmodule
