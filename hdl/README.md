Here are explained all the folders contents:

- coherence: 

    all the files about the coherence are contained here, included the load/store stage and the directory controller
	
- common:

    there are various support files, like arbiters or id/oh converters
	
- core:

    all the elements inside the nu+ core are selfcontained here, included the nuplus_system.sv file that contains other elements necessary for the single-core synthesyzed version
	
- include: 

    all the typedef, struct and defines are contained in these files. The user should modify only the user_define.sv file
	
- mango integration:

    the files included here are just used when the nu+ system is integrated inside the MANGO architecture
	
- network:

    the router and the network interface are the basic blocks of this folder; so there are 3 types of tile - with the memory controller or the core or nothing inside - all instantiated in the nuplus_noc.sv file with a mesh structure
	
- service_support:

    another kind of tile (host2core) contains the host request manager, in order to communicate with the host, and the boot setup
	
- system:

    all the files abuout the implementation on Xilinx board