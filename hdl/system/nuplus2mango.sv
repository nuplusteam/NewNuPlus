`include "nuplus_user_defines.sv"
`include "nuplus_network_defines.sv"
`include "nuplus_coherence_defines.sv"

/*
 * This is the module that manage the communication between memory and the tile_mc
 */

`ifdef DISPLAY_COHERENCE
` include "nuplus_debug_log.sv"
`endif

module nuplus2mango #(
		parameter TILE_ID             = 0,
		parameter MANGO_ADDRESS_WIDTH = 32,
		parameter MANGO_DATA_WIDTH    = 512 )
	(
		input                                                              clk,
		input                                                              reset,
		input                                                              enable,

		// Requests from nuplus NI
		input  coherence_forwarded_message_t                               ni_forwarded_request,
		input  logic                                                       ni_forwarded_request_valid,
		output logic                                                       n2m_forwarded_request_available,
		output logic                                                       n2m_forwarded_request_consumed,

		input  coherence_response_message_t                                ni_response,
		input  logic                                                       ni_response_valid,
		output logic                                                       n2m_response_consumed,

		// Read responses to nuplus NI
		output coherence_response_message_t                                n2m_response,
		output logic                                                       n2m_response_valid,
		output logic                                                       n2m_response_has_data,
		output logic                                                       n2m_response_to_cc_valid,
		output tile_address_t                                              n2m_response_to_cc,
		output logic                                                       n2m_response_to_dc_valid,
		output tile_address_t                                              n2m_response_to_dc,
		input  logic                                                       ni_response_network_available,

		// RW requests to MANGO NI
		output logic                         [MANGO_ADDRESS_WIDTH - 1 : 0] n2m_request_address,
		output logic                         [MANGO_DATA_WIDTH - 1 : 0]    n2m_request_data,
		output logic                                                       n2m_request_read,
		output logic                                                       n2m_request_write,
		output logic                                                       n2m_request_is_instr,
		input  logic                                                       m2n_request_read_available,
		input  logic                                                       m2n_request_write_available,

		// Read responses from MANGO NI
		input  logic                                                       m2n_response_valid,
		input  logic                         [MANGO_ADDRESS_WIDTH - 1 : 0] m2n_response_address,
		input  logic                         [MANGO_DATA_WIDTH - 1 : 0]    m2n_response_data,
		output logic                                                       n2m_avail
	);

	localparam NUM_REQ = 2;
	localparam FIFO_LENGTH = 16;

	logic                                                    request_is_valid, can_issue_response;
	logic                                                    request_is_read, request_is_write;
	logic                         [NUM_REQ - 1 : 0]          request_oh;
	logic                         [NUM_REQ - 1 : 0]          grant_oh;

	coherence_response_message_t                             pending_response_out;
	coherence_forwarded_message_t                            pending_fwd_request_out;
	logic                                                    pending_read;
	logic                                                    response_fifo_full, fwd_request_fifo_full;
	logic                                                    response_fifo_empty, fwd_request_fifo_empty;
	logic                                                    response_fifo_dequeue_en, fwd_request_fifo_dequeue_en;
	logic                                                    mc_response_fifo_full;
	logic                         [MANGO_DATA_WIDTH - 1 : 0] mc_response_fifo_out;
	logic                                                    mc_response_fifo_empty;

	logic                         [MANGO_DATA_WIDTH - 1 : 0] m2n_response_data_swap;
	logic                         [MANGO_DATA_WIDTH - 1 : 0] out_data_swap;

//  -----------------------------------------------------------------------
//  -- MANGO to NuPlus
//  -----------------------------------------------------------------------

	// Endian swap vector data
	genvar                                                   swap_word;
	generate
		for ( swap_word = 0; swap_word < 16; swap_word++ ) begin : swap_word_gen
			assign m2n_response_data_swap[swap_word * 32 +: 8]      = m2n_response_data[swap_word * 32 + 24 +: 8];
			assign m2n_response_data_swap[swap_word * 32 + 8 +: 8]  = m2n_response_data[swap_word * 32 + 16 +: 8];
			assign m2n_response_data_swap[swap_word * 32 + 16 +: 8] = m2n_response_data[swap_word * 32 + 8 +: 8];
			assign m2n_response_data_swap[swap_word * 32 + 24 +: 8] = m2n_response_data[swap_word * 32 +: 8];

			assign out_data_swap[swap_word * 32 +: 8]               = pending_response_out.data[swap_word * 32 + 24 +: 8];
			assign out_data_swap[swap_word * 32 + 8 +: 8]           = pending_response_out.data[swap_word * 32 + 16 +: 8];
			assign out_data_swap[swap_word * 32 + 16 +: 8]          = pending_response_out.data[swap_word * 32 + 8 +: 8];
			assign out_data_swap[swap_word * 32 + 24 +: 8]          = pending_response_out.data[swap_word * 32 +: 8];
		end
	endgenerate

	sync_fifo #(
		.WIDTH                 ( MANGO_DATA_WIDTH ),
		.SIZE                  ( FIFO_LENGTH      ),
		.ALMOST_FULL_THRESHOLD ( FIFO_LENGTH - 8  )
	)
	fifo_from_mango (
		.clk         ( clk                         ),
		.reset       ( reset                       ),
		.flush_en    ( 1'b0                        ),
		.full        (                             ),
		.almost_full ( mc_response_fifo_full       ),
		.enqueue_en  ( m2n_response_valid          ),
		.value_i     ( m2n_response_data_swap      ),
		.empty       ( mc_response_fifo_empty      ),
		.almost_empty(                             ),
		.dequeue_en  ( fwd_request_fifo_dequeue_en ),
		.value_o     ( mc_response_fifo_out        )
	);

	assign n2m_avail                       = ~mc_response_fifo_full;
	assign can_issue_response              = ~mc_response_fifo_empty & ni_response_network_available;

	always_ff @ ( posedge clk, posedge reset )
		if ( reset ) begin
			n2m_response_valid       <= 1'b0;
			n2m_response_to_dc_valid <= 1'b0;
			n2m_response_to_cc_valid <= 1'b0;
		end else begin
			n2m_response_to_dc_valid <= pending_fwd_request_out.packet_type == FWD_GETS & ~pending_fwd_request_out.req_is_uncoherent;
			n2m_response_to_cc_valid <= 1'b1;
			if ( can_issue_response )
				n2m_response_valid <= 1'b1;
			else
				n2m_response_valid <= 1'b0;
		end

	always_ff @ ( posedge clk ) begin
		n2m_response_has_data          <= 1'b1;
		n2m_response_to_cc             <= pending_fwd_request_out.source;
		n2m_response_to_dc             <= pending_fwd_request_out.memory_address[`ADDRESS_SIZE - 1 -: $clog2( `TILE_COUNT )];

		n2m_response.data              <= mc_response_fifo_out;
		n2m_response.from_directory    <= 1'b0;
		n2m_response.memory_address    <= pending_fwd_request_out.memory_address;
		n2m_response.source            <= TILE_ID;
		n2m_response.sharers_count     <= 0;
		n2m_response.packet_type       <= DATA;
		n2m_response.requestor         <= pending_fwd_request_out.requestor;
		n2m_response.req_is_uncoherent <= pending_fwd_request_out.req_is_uncoherent;
	end

//  -----------------------------------------------------------------------
//  -- NuPlus to MANGO - FIFOs
//  -----------------------------------------------------------------------

	sync_fifo #(
		.WIDTH                ( $bits( coherence_response_message_t ) ),
		.SIZE                 ( FIFO_LENGTH                           ),
		.ALMOST_FULL_THRESHOLD( FIFO_LENGTH - 8                       )
	)
	response_fifo (
		.clk         ( clk                      ),
		.reset       ( reset                    ),
		.flush_en    ( 1'b0                     ),
		.full        (                          ),
		.almost_full ( response_fifo_full       ),
		.enqueue_en  ( ni_response_valid        ),
		.value_i     ( ni_response              ),
		.empty       ( response_fifo_empty      ),
		.almost_empty(                          ),
		.dequeue_en  ( response_fifo_dequeue_en ),
		.value_o     ( pending_response_out     )
	);

	assign n2m_response_consumed           = ~response_fifo_full & ni_response_valid;
	
	// When a read request is scheduled to the MANGO network, its source is stored in this FIFO. When the MANGO
	// network responses this information is used to forward this response to the right requestor tile.
	sync_fifo #(
		.WIDTH                ( $bits( coherence_forwarded_message_t ) ),
		.SIZE                 ( FIFO_LENGTH                            ),
		.ALMOST_FULL_THRESHOLD( FIFO_LENGTH - 8                        )
	)
	fwd_request_fifo (
		.clk         ( clk                         ),
		.reset       ( reset                       ),
		.flush_en    ( 1'b0                        ),
		.full        (                             ),
		.almost_full ( fwd_request_fifo_full       ),
		.enqueue_en  ( n2m_forwarded_request_consumed ),
		.value_i     ( ni_forwarded_request        ),
		.empty       ( fwd_request_fifo_empty      ),
		.almost_empty(                             ),
		.dequeue_en  ( fwd_request_fifo_dequeue_en ),
		.value_o     ( pending_fwd_request_out     )
	);

	assign n2m_forwarded_request_available = ~fwd_request_fifo_full;
	assign n2m_forwarded_request_consumed  = ~fwd_request_fifo_full & ni_forwarded_request_valid;

	always_ff @ ( posedge clk, posedge reset ) begin
		if ( reset ) begin
			pending_read <= 1'b0;
		end else begin
			if ( fwd_request_fifo_dequeue_en ) begin
				pending_read <= 1'b0;
			end else if( grant_oh[1] & m2n_request_read_available ) begin
				pending_read <= 1'b1;
			end
		end
	end

//  -----------------------------------------------------------------------
//  -- NuPlus to MANGO
//  -----------------------------------------------------------------------
	assign request_oh                      = {~fwd_request_fifo_empty, ~response_fifo_empty};

	rr_arbiter #(
		.NUM_REQUESTERS( NUM_REQ )
	)
	rr_arbiter (
		.clk       ( clk        ),
		.reset     ( reset      ),
		.request   ( request_oh ),
		.update_lru( fwd_request_fifo_dequeue_en | response_fifo_dequeue_en ),
		.grant_oh  ( grant_oh   )
	);

	// A request is valid if there is a pending request from NI and pending request FIFO is not full
	assign request_is_valid                = |request_oh & enable & ~pending_read;
	assign request_is_read                 = request_is_valid & ( pending_fwd_request_out.packet_type == FWD_GETS | pending_fwd_request_out.packet_type == FWD_GETM ) & grant_oh[1];
	assign request_is_write                = request_is_valid & pending_response_out.packet_type == WB & grant_oh[0];

	assign fwd_request_fifo_dequeue_en     = can_issue_response & pending_read;
	assign response_fifo_dequeue_en        = m2n_request_write_available & request_is_write;

	always_ff @(posedge clk, posedge reset)  begin
		if (reset) begin
			n2m_request_address = {MANGO_ADDRESS_WIDTH{1'b0}};
		end else begin
			if ( request_is_read ) begin
				n2m_request_address = pending_fwd_request_out.memory_address;
			end else if( request_is_write ) begin
				n2m_request_address = pending_response_out.memory_address;
			end
		end
	end

	always_ff @(posedge clk) begin
		if (m2n_request_read_available)
			n2m_request_read             = request_is_read;
		else
			n2m_request_read             = 1'b0;

		if (m2n_request_write_available)
			n2m_request_write            = request_is_write;
		else
			n2m_request_write            = 1'b0;

		n2m_request_is_instr            = pending_fwd_request_out.req_is_uncoherent;
		n2m_request_data                = out_data_swap;
	end

`ifdef DISPLAY_COHERENCE

	always_ff @( posedge clk ) begin

		if ( ( grant_oh[0] & response_fifo_dequeue_en ) | ( grant_oh[1] & fwd_request_fifo_dequeue_en & pending_fwd_request_out.req_is_uncoherent ) ) begin
			$fdisplay( `DISPLAY_COHERENCE_VAR, "=======================" );
			$fdisplay( `DISPLAY_COHERENCE_VAR, "Memory Controller - [Time %.16d] [TILE %.2d] - Message Received", $time( ), TILE_ID );

			if ( grant_oh[0] )
				print_resp( pending_response_out );
			else if ( grant_oh[1] & ~pending_fwd_request_out.req_is_uncoherent )
				print_fwd_req( pending_fwd_request_out );

			$fflush( `DISPLAY_COHERENCE_VAR );
		end

		if ( ~reset & ( n2m_response_valid ) ) begin
			if ( n2m_response_valid & ~n2m_response.req_is_uncoherent ) begin
				$fdisplay( `DISPLAY_COHERENCE_VAR, "=======================" );
				$fdisplay( `DISPLAY_COHERENCE_VAR, "Memory Controller - [Time %.16d] [TILE %.2d] - Message Sent", $time( ), TILE_ID );
				$fdisplay( `DISPLAY_COHERENCE_VAR, "Destination: %h", n2m_response_to_cc );
				print_resp( n2m_response );

				$fflush( `DISPLAY_COHERENCE_VAR );
			end
		end
	end

`endif

endmodule
