//
// Copyright 
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

`include "nuplus_defines.sv"


module sram_2r1w
    #(
	parameter DATA_WIDTH = 32,
    parameter SIZE = 1024,
    parameter READ_DURING_WRITE = "NEW_DATA")
	    
    (
	input                             clk,
	    
    input                            read1_en,
    input [ADDR_WIDTH - 1:0]         read1_addr,
    input                            read2_en,
    input [ADDR_WIDTH - 1:0]         read2_addr,
    
    input                            write_en,
    input [ADDR_WIDTH - 1:0]         write_addr,
    input [DATA_WIDTH - 1:0]         write_data,
    
    output logic[DATA_WIDTH - 1:0]   read1_data,
    output logic[DATA_WIDTH - 1:0]   read2_data 
    );

    localparam ADDR_WIDTH = $clog2(SIZE);

	reg [DATA_WIDTH - 1:0] ram1 [SIZE];
    reg [DATA_WIDTH - 1:0] ram2 [SIZE];

    logic[DATA_WIDTH - 1:0] data_from_ram1;
    logic[DATA_WIDTH - 1:0] data_from_ram2;

    always_ff @(posedge clk)
    begin
        if (write_en)
            ram1[write_addr] <= write_data;
        if (read1_en)
            data_from_ram1 <= ram1[read1_addr];
    end

    always_ff @(posedge clk)
    begin
        if (write_en)
            ram2[write_addr] <= write_data;
        if (read2_en)
            data_from_ram2 <= ram2[read2_addr];
    end

    generate
        if (READ_DURING_WRITE == "NEW_DATA")
        begin
            logic pass_thru1_en;
            logic pass_thru2_en;
            logic[DATA_WIDTH - 1:0] pass_thru_data;

            always_ff @(posedge clk)
            begin
                pass_thru1_en <= write_en && read1_en && read1_addr == write_addr;
                pass_thru2_en <= write_en && read2_en && read2_addr == write_addr;
                pass_thru_data <= write_data;
            end

            assign read1_data = pass_thru1_en ? pass_thru_data : data_from_ram1;
            assign read2_data = pass_thru2_en ? pass_thru_data : data_from_ram2;
        end
        else
        begin
            assign read1_data = data_from_ram1;
            assign read2_data = data_from_ram2;
        end
    endgenerate

endmodule
