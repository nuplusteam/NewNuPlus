\documentclass[a4paper,11pt,oneside]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{graphicx}
\usepackage{geometry}
\usepackage{listings}
\usepackage{color}
\usepackage[numbered]{mcode}
\usepackage{indentfirst}
\usepackage{fancyhdr}
\usepackage{float}
\usepackage{cite}
\usepackage{tikz}
\usepackage{multirow,bigdelim}
\usepackage{pgfplots}

\pagestyle{fancy}
%\renewcommand{\chaptermark}[1]{\markboth{#1}{}}
%\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

\fancyhf{}
\fancyhead[LE,RO]{\bfseries\thepage}
\fancyhead[LO]{\bfseries\rightmark}
\fancyhead[RE]{\bfseries\leftmark}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}
\setlength{\headheight}{1pt}
\geometry{verbose,a4paper,tmargin=25mm,bmargin=30mm,lmargin=20mm,rmargin=20mm}
\linespread{1}

\title{Nu+ Coherence}


\begin{document}
\nocite{*}

{\let\newpage\relax\maketitle}
\vspace{3cm}


\section{Architecture Overview}
\paragraph{Assumptions:}
\begin{enumerate}
	\item Our Processor allows only one outstanding miss per thread. If a thread has memory miss, the \texttt{Control Unit} suspends it until the pending request is satisfied. It is a really conservative assumption but in this initial stage we want to simplify the architecture.
	\item If a thread does a memory access on a already pending address, its request is dispatched after the pending one, we do not support request merging.  
\end{enumerate}
	
\paragraph{Architecture:} 
Our main focus is to decouple the coherence protocol from the core datapath. There are two main actors, as Figure \ref{fig:cc} depicts: 

\begin{enumerate}
	\item \texttt{Load Store Unit}: core side (above). It holds cache L1 data and tags, schedules different thread memory requests, and brings data in \texttt{Writeback}. 
	\item \texttt{Cache Controller}: memory side (below). It handles core and network requests. 
\end{enumerate}

The \texttt{Load Store Unit} does not store specific coherence protocol information (as stable states) but it stores \textit{privileges} for all cached addresses. Each cache line has two privileges: \textit{can read} and \textit{can write}. Those privileges determine cache misses/hits and are updated only by the \texttt{Cache Controller}. 

The other main actor is the \texttt{Cache Controller}. As the previous one, we kept its design independent from a specific coherence protocol as possible.

\section{Cache Controller}
\begin{figure}[t]
	\centering
	\includegraphics[scale=0.55]{Images/Architettura.eps}
	\caption{Cache Controller Architecture.}
	\label{fig:cc}
\end{figure}

Our aim is to decouple the control unit from the coherence protocol. The \texttt{Cache Controller} main components are:

\begin{enumerate}
	\item \texttt{Core Interface}: it holds core FIFOs, one for each type of request.
	\item \texttt{Protocol ROM}: it stores the specific coherence protocol. It receives the current state and the scheduled request and decodes the next atomic actions that the \texttt{Fetch Unit} and the \texttt{Protocol Handler} have to take. For now it is hardcoded, in the future it will be made programmable.
	\item \texttt{Fetch Unit}: it checks all the pending requests from \texttt{Network Interface} (NI) and \texttt{Core Interface} (CI), checks all the pending memory requests (in MSHR), and issues the next request. This module queries the \texttt{Protocol ROM} that asserts if a request with a given state should stall or not.
	\item \texttt{Cache Snoop and State Lookup}: It reads the scheduled address and issues a tag and privileges snoop request on the \texttt{Tag and Privileges Snoop Request} bus. It also reads the state and checks if there is an already pending request in MSHR. It issues a Data snoop request to L1 data cache if a replacement occurs on \texttt{Data Snoop Request} bus. This information is dispatched to the \texttt{Protocol Handler}. 
	\item \texttt{Protocol Handler}: This module queries the \texttt{Protocol ROM} with the scheduled address current state and request, updates the MSHR entry and \texttt{Coherence State SRAM} if needed. In case of response from the network (after a GetS request, for instance), the \texttt{Protocol Handler} updates the L1 cache tag, the privileges, and the data, issuing an update request on its \texttt{High Priority Update} bus. An update from \texttt{Cache Controller} has the highest priority in the \texttt{Load Store Unit Arbiter}. Furthermore, this module handles block replacement and issues coherence requests.
	\item \texttt{Network Message Composer}: This module translates the coherence request from the \texttt{Protocol Handler} in a network request, and issues it on the right virtual network.  
	\item \texttt{Network Interface}: it handles network messages, it has three virtual networks in order to avoid deadlocks.
\end{enumerate}

The \texttt{Protocol Handler} is the main module of our \texttt{Cache Controller}, it reads coherence state and the protocol table to generate the next action. The \texttt{Protocol ROM} indicates the next step to take in the coherence protocol based on the current state and the scheduled request. The \texttt{Coherence State SRAM} holds a stable state for each cache line, while transient states are kept in MSHR with pending requests.

\paragraph{Assumptions:}
\begin{enumerate}
	\item The \texttt{Fetch Unit} does not check if there is room for a new MSHR entry when a request is scheduled. We suppose that a new request has always room for a new MSHR entry. Our architecture blocks a thread that issues a miss memory request, hence from the core side we can receive a number of requests equal to the number of threads (one request per thread), and only a core request may allocate a new MSHR entry. We keep the number of MSHR entries equal to the number of threads.
	\item When a request is issued (both from NI or CI), it can modify an MSHR entry after two clock cycles, hence the next request may read a non up to date entry. In order to avoid this, the \texttt{Fetch Unit} does not schedule two consecutive requests on the same address. This is done with a scoreboard-like mechanism. 
	\item MSHR structure is a read-first memory, it may cause issues if the \texttt{Protocol Unit} updates an entry and a request reads that entry in the same cycle. In order not to read an non up to date MSHR entry, when \texttt{Protocol Unit} is updating a specific MSHR entry, \texttt{Fetch Unit} does not schedules request on that entry.
\end{enumerate}

\subsection{Replacement and Evict - First strategy}
Our main issue is related to Eviction and Replacement. When a response from the network occurs (after a GetS/GetM request), it holds the new data. In \texttt{Cache Snoop and State Lookup} we recognize that L1 cache has no more room, so a replacement must be done. To the \texttt{Protocol Handler} we issue:

\begin{enumerate}
	\item The new cache line data, taken from the response message.
	\item The new cache line state from MSHR. 
	\item The old data from \texttt{Data Snoop} bus.
	\item The old cache line state from \texttt{Coherence State}. 
	\item The PseudoLRU decision.
\end{enumerate} 

The \texttt{Protocol Handler} sends the new tag, the privileges, and the data on the \texttt{High Priority Update} bus and concurrently updates \texttt{Coherence State SRAM}.

In the same time, if it must send back the old data, the \texttt{Protocol Unit} allocates a new entry in MSHR. We suppose that there will always be room for this replacement entry, because a replacement can occur only after a response message and it always deallocates a MSHR entry. 

\paragraph{Open Issues:}
\begin{enumerate}
	\item When a replacement occurs, the cache controller has to PUT the data back to the directory. We are generating a request after a response, may this cause a deadlock?
\end{enumerate}

\subsection{Replacement and Evict - Second strategy}

We are implementing the first strategy, but we have a backup solution in case of the first one may cause deadlock. After a response, the \texttt{Protocol Handler} preallocates the MSHR entry for the replacement and forces the \texttt{Load Store Unit} to evict the old data in the Evict Buffer. The \texttt{Protocol Unit} fills this entry with the address to evict and sets the \texttt{waiting\_for\_eviction} bit. All requests to this address are stalled. When \texttt{Fetch Unit} schedules the replacement request from Evict Buffer, the remaining information (such as data and state) are filled in MSHR and the \texttt{Protocol Handler} can issue the PUT request.


\paragraph{MSHR}

\begin{table}[h]    
	\centering
	\begin{tabular}{ | l | l | l | l | l | l | l |}
		\hline
		Valid & State & Address & Thread Id & Data & Ack Count & Waiting For Eviction \\
		\hline
	\end{tabular}
	\caption{Tabella MSHR}
	\label{tab:mshr}
\end{table}

In our architecture an Eviction/Replacement allocates a new MSHR entry, this new line is not filled in one single cycle because the old data is coming from Evict Buffer, hence the \texttt{waiting\_for\_eviction} bit indicates that the eviction request cannot be issued until the data is arrived from \texttt{Load Store Unit}. The Evict Buffer has the highest priority in the \texttt{Fetch Unit}.

\section{Router}

\paragraph{Main Features:}

\begin{enumerate}
	\item Messages = Packets.
	\item Flits = Phits.
	\item FLIT-based.
	\item Routing look-ahead DOR.
	\item Buffer backpressue  on/off.
	\item Wormhole control flow. 
\end{enumerate}

\paragraph{Assumption:}

\begin{enumerate}
	\item With DOR, FLITs from source to destination arrive in order, hence there is no need of FLIT Number in FLIT Header.
\end{enumerate}

\paragraph{Question:}
\begin{enumerate}
	\item Can each Virtual Network be mapped on a single Virtual Channel?
\end{enumerate}


\section{Network Interface}
\begin{figure}[t]
	\centering
	\includegraphics[scale=0.6]{Images/network_interface.eps}
	\caption{Network Interface Architecture.}
	\label{fig:ni}
\end{figure}

\paragraph{Assumptions:}
\begin{enumerate}
	\item Messages from \texttt{Cache Controller} are made of one packet.
	\item NI translates messages in FLITs. The NI needs to know VCID in FLIT construction. We are supposing that VN and VC are direct mapped. In other word, the NI determines VCID based on the type of the scheduled message (e.g. response message are mapped to VC 0).
	\item Each VN is direct mapped to one VC.
\end{enumerate}

\subsection{NI-Router Interface}

\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.65]{Images/NI_Router.eps}
	\caption{NI-Router Interface.}
	\label{fig:ni_router}
\end{figure}

\subsection{Virtual Network Messages}

\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.65]{Images/CC_Mex.eps}
	\caption{Virtual Network Messages.}
	\label{fig:CC_Mex}
\end{figure}



\end{document} 