% !TeX spellcheck = en_US
\documentclass[a4paper,11pt,oneside]{extarticle}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{graphicx}
\usepackage{geometry}
\usepackage{listings}
\usepackage{color}
\usepackage[dvipsnames]{xcolor}
\usepackage{indentfirst}
\usepackage{fancyhdr}
\usepackage{float}
\usepackage{cite}
\usepackage{tikz}
\usepackage{multicol}
\usepackage{multirow,bigdelim}
\usepackage{booktabs}
\usepackage{pgfplots}
\usepackage{wrapfig}

\pagestyle{fancy}
%\renewcommand{\chaptermark}[1]{\markboth{#1}{}}
%\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

\fancyhf{}
\fancyhead[LE,RO]{\bfseries\thepage}
\fancyhead[LO]{\bfseries\rightmark}
\fancyhead[RE]{\bfseries\leftmark}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}
\setlength{\headheight}{1pt}
\geometry{verbose,a4paper,tmargin=10mm,bmargin=15mm,lmargin=13mm,rmargin=13mm}

\definecolor{codegray}{gray}{0.9}
\newcommand{\code}[1]{\colorbox{codegray}{\texttt{#1}}}
\newcommand{\nuplus}{\textbf{\textcolor{TealBlue}{nu+ }}}

\lstset{basicstyle=\ttfamily,
	showstringspaces=false,
	commentstyle=\color{red},
	keywordstyle=\color{blue}
}

\date{}
\pagenumbering{gobble}
\title{\nuplus - Internal Document}
%\author{Mirko Gagliardi}

\begin{document}

{\let\newpage\relax\maketitle}


\section{Introduction}
\nuplus is an open-source GPU-like compute core, developed by CeRICT in the framework of the MANGO FETHPC project. The main objective of \nuplus is to enable resource-efficient HPC based on special-purpose customized hardware. Our aim is to build an application-driven architecture to achieve the best hardware/software configuration for any data-parallel kernel. Specialized data-parallel accelerators have been known to provide higher efficiency than general-purpose processors for codes with significant amounts of regular data-level parallelism (DLP). However every parallel kernel has its own \textit{ideal} configuration. The \nuplus solution provides a configurable manycore solution meant to explore future HPC solutions.

\begin{wrapfigure}{R}{0.7\textwidth}
	\centering
	\includegraphics[width=0.7\textwidth]{Images/nuplus_manycore.eps}
	\caption{\nuplus manycore overview.}
	\label{fig:manycore}
\end{wrapfigure}

The \nuplus manycore is a parametrizable regular mesh Network on Chip (NoC) of configurable tile. Each \nuplus tile has the same basic components, it provides a configrable GPU-like open-source softcore meant to be used as a configurable FPGA overlay. This HPC-oriented accelerator merges the SIMT paradigm with vector processor model. Futhermore, each tile has a Cache Controller and a Directory Controller, those components handle data coherence between different cores in different tiles.

User design can set an high number of parameter for every need, such as: 

\begin{enumerate}
	\item NoC topology and Tile number.
	\item Threads per core number. Each core has user defined number of hardware threads. Each thread has a different PC, so one core can executes as many program as many threads it has.
	\item Hardware lanes per thread. Each thread can be a vector operation (here called hardware lane). 
	\item Register file size (scalar and vector).
	\item L1 and L2 cache size and way number.
\end{enumerate}


On top of the customized hardware core, we are also developing a \nuplus compiler backend relying on the \texttt{LLVM} infrastructure. 

\section{Installation}
The \nuplus development environment requires \texttt{Vivado} (preferably version \texttt{2016.3}) for launching simulation and FPGA implementation. Also, \texttt{eclipse DVT} is highly suggested but not strictly necessary. The project can be downloaded launching the following git command:
\begin{lstlisting}[language=bash]
	git clone git@bitbucket.org:nuplusteam/newnuplus.git
\end{lstlisting}


\section{Project Overview}

\begin{itemize}
	\item compiler
	\item docs
	\item hdl
	\item simulation
	\item VivadoProject
\end{itemize}

\section{Toolchain and Simulation}
Before launching a simulation some parameter and defines must be modified. In \texttt{hdl/include/user\_define.sv} the \texttt{PROJECT\_PATH} define must be update with the local host. This variable sets the path for memory images and the message log file. 

In order to compile a new program the java variable \texttt{path} in \texttt{/compiler/NuAssembler/src/main/Main.java} has to be update with the current local host, and rerun the java application.

\section{GPU-like core}
The core is based on a RISC in-order pipeline. Its control unit is intentionally kept lightweight. The architecture masks memory and operation latencies by heavily relying on hardware multithreading. By ensuring a light control logic, the core can devote most of its resources for accelerating computing in highly data-parallel kernels. In the hardware multithreading \nuplus architecture, each hardware thread has its own PC, register file, and control registers. The number of threads is user configurable. A \nuplus hardware thread is equivalent to a wavefront in the AMD terminology and a CUDA warp in the NVIDIA terminology. The processor uses a deep pipeline to improve clock speed. 

\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=0.5\textwidth]{Images/nuplus_core.eps}
	\caption{\nuplus core overview.}
	\label{fig:core}
\end{wrapfigure}


All threads share the same compute units. Execution pipelines are organized in hardware vector lanes (like vector processors, each operator is replicated $N$ times). Each thread can perform a SIMD operation on independent data, while data are organized in a vector register file. The core supports a high-throughput non-coherent scratchpad memory, or SPM (corresponding to the shared memory in the NVIDIA terminology). The SPM is divided in a parameterized number of banks based on a user-configurable mapping function. The memory controller resolves bank collisions at run-time ensuring a correct execution of SPM accesses from concurrent threads. Coherence mechanisms incur a high latency and are not strictly necessary for many applications. 

\paragraph{Instruction Fetch}
\texttt{Instruction Fetch} stage schedules the next thread PC from the eligible thread pool, handled by the \texttt{Thread Controller}. Available threads are scheduled in a Round Robin fashion. Furthermore, at the boot phase, the \texttt{Thread Controller} can initialize each thread PC through a specific interface. 

The instruction cache is set associative and has two stages. Once an eligible thread is selected, \texttt{Instruction Fetch} reads its PC, and determines if the next instruction cache line is already in instruction cache memory or not. In the first stage each way has a bank of memory containing tag values and valid bits for the cache sets. This stage reads the way memories in parallel and passes those data to the second stage. The next stage tag memory has one cycle of latency, so the next stage handles the result. This stage compares the way tags read in the last stage, if they match, it is a cache hit. In this case, this stage issues the instruction cache data address to instruction cache data memory. If a miss occurs an instruction memory transaction is issued to the \texttt{Network Interface} and the thread is blocked until the instruction line is not retrieved from main memory.

Finally, this module handles the PC restoring in case of rollback. When a rollback occurs and the rollback signals are set by \texttt{Rollback Handler} stage, the \texttt{Instruction Fetch} module overwrites the PC of the thread that issued the rollback.

\paragraph{Decode}
Decode stage decodes fetched instruction from \texttt{Instruction Fetch} and produces the control signals for the datapath directly from the instruction bits. Output dec\_instr helps execution and control modules to manage the issued instruction and is propagated in each pipeline stage. Instruction type are presented in the ISA section \ref{section:ISA}.

\paragraph{Thread Scheduler}
Fetched instructions are stored in FIFOs in this stage, one per thread. The \texttt{Dynamic Scheduler} checks data hazard and states which thread can be fetched in the \texttt{Operand Fetch}; this is done through a light scoreboarding system, each thread has its own scoreboard. There are no structural hazard check, it is done in \texttt{Writeback} stage.

\paragraph{Operand Fetch}
\texttt{Operand Fetch} prepares operands to the \texttt{Execution} pipeline. As said before, \nuplus core supports SIMD operations, for this purpose it has two register files: a scalar register file (SRF) and a vector register file (VRF).
The SRF is general purpose register and in the base configuration it has $64$ registers of $32$-bits. Dually, the VRF register size has a scalar register file for each hardware lane, in other word each vector register is composed by hardware lane number of scalar register; in the base configuration the VRF has $64$ vector registers and each vector register is composed of $16$ scalar register of $32$-bits. Each thread has its own register file.  
 
\paragraph{Execution}
Execution stage is divided in different modules, each pipes serves all thread hardware line concurrently:

\begin{itemize}
	\item \texttt{Floating Point Unit}: A multistage floating point instructions, supports all basic FP operation according to the IEEE-754-2008 standard. 
\end{itemize}

\begin{itemize}
	\item \texttt{Branch Unit}: this stage handles conditional and unconditional jumps and traps. It signals to the \texttt{Rollback Handler} when a jump must be taken or not.
	\item \texttt{Integer Pipeline}: A single stage executes simple integer instructions, as integer operations, comparisons and bitwise logical operations.
	\item \texttt{Special Function Unit}: A multistage special function unit instruction pipeline, support many transcendental function as sin, cos, sqrt and others.
\end{itemize}

\paragraph{ScratchPad Memory}
\texttt{Scrathpad Memory} is a high-speed internal memory used for temporary storage of calculations by threads in the same core. It is at L1 cache level and its size and other parameters are customizable. This shared memory has multiple independently-addressable memory banks, and one location per bank can be accessed in parallel. Address conflicts are serialized.

\paragraph{Load/Store Unit}
The \texttt{LDST Unit} is detailed in the next section \ref{sec:mem}.

\paragraph{Writeback}
\texttt{Writeback} stage detects on-the-fly structural hazard on register writeback and extends load signed operations.The integer, memory, and floating point execution pipelines have different lengths, so instructions issued in different cycles could arrive at the \texttt{Writeback} in the same cycle. Furthermore, due to collisions, a load/store to the scartchpad memory can have variable latency which is unknown at compile time, and this can result in an unpredictable structural hazard on \texttt{Writeback}. The \texttt{Writeback} module can resolves collision on itself on-the-fly without losing effectiveness.

\paragraph{Thread Controller}
\texttt{Thread Controller} handles eligible thread pool. This module blocks threads that cannot proceed due cache misses or scoreboarding. Dually, \texttt{Thread Controller} handles threads wake-up when the blocking conditions are no more trues. 

Furthermore, the \texttt{Thread Controller} interfaces core instruction cache and the higher level in the memory hierarchy. A load/store miss blocks the corresponding thread until data is gather from main memory. Instruction miss requests are directly forwarded to the memory controller through the network on chip.

\paragraph{Rollback Handler}
\texttt{Rollback Handler} restores PCs and scoreboards of the threads that issued a rollback. In case of jump or trap, the \texttt{Brach} module in the \texttt{Execution} pipeline issues a rollback request to this stage, and passes to the \texttt{Rollback Handler} the thread ID that issued the rollback, the old scoreboard and the PC to restore. Furthermore, the \texttt{Rollback Handler} flushes all issued requests from the thread still in the pipeline.

\clearpage
\section{Memory System}\label{sec:mem}
\nuplus has two cache levels, each core as is own L1 cache shared among all its thread, all cores share the same L2 cache equally distributed among all tiles. Coherence is supported through a directory-based mechanism, each tile has a \texttt{Directory Controller} that handles read and write requests from different cores. 

Memory model supports a nVIDIA-like Shared Memory mechanism (here called \texttt{Scratchpad Memory}) that will allows different threads, in the same core, to share data without coherence mechanism overhead. 

In this section are detailed the memory organization in the \nuplus system and the main actors that ensures coherence among different cores in the NoC. Our main focus is to decouple the coherence protocol from the core datapath. There are two main actors, as Figure \ref{fig:cc} depicts: 

\begin{enumerate}
	\item \texttt{Load Store Unit}: core side (above). It holds cache L1 data and tags, schedules different thread memory requests, and brings data in \texttt{Writeback}. 
	\item \texttt{Cache Controller}: memory side (below). It handles core and network requests. 
\end{enumerate}

\begin{wrapfigure}{R}{0.75\textwidth}
	\centering
	\includegraphics[width=0.75\textwidth]{Images/LDST_CC.eps}
	\caption{Cache Controller Architecture.}
	\label{fig:cc}
\end{wrapfigure}

The \texttt{Load Store Unit} does not store specific coherence protocol information (as stable states) but it stores \textit{privileges} for all cached addresses. Each cache line has two privileges: \textit{can read} and \textit{can write}. Those privileges determine cache misses/hits and are updated only by the \texttt{Cache Controller}. 

The other main actor is the \texttt{Cache Controller}. As the previous one, we kept its design independent from a specific coherence protocol as possible.

\subsection{Cache Controller}
\nuplus system supports a shared memory system 
Our aim is to decouple the control unit from the coherence protocol. The \texttt{Cache Controller} main components are:

\begin{enumerate}
	\item \texttt{Core Interface}: it holds core FIFOs, one for each type of request.
	\item \texttt{Protocol ROM}: it stores the specific coherence protocol. It receives the current state and the scheduled request and decodes the next atomic actions that the \texttt{Fetch Unit} and the \texttt{Protocol Handler} have to take. For now it is hardcoded, in the future it will be made programmable.
	\item \texttt{Fetch Unit}: it checks all the pending requests from \texttt{Network Interface} (NI) and \texttt{Core Interface} (CI), checks all the pending memory requests (in MSHR), and issues the next request. This module queries the \texttt{Protocol ROM} that asserts if a request with a given state should stall or not.
	\item \texttt{Cache Snoop and State Lookup}: It reads the scheduled address and issues a tag and privileges snoop request on the \texttt{Tag and Privileges Snoop Request} bus. It also reads the state and checks if there is an already pending request in MSHR. It issues a Data snoop request to L1 data cache if a replacement occurs on \texttt{Data Snoop Request} bus. This information is dispatched to the \texttt{Protocol Handler}. 
	\item \texttt{Protocol Handler}: This module queries the \texttt{Protocol ROM} with the scheduled address current state and request, updates the MSHR entry and \texttt{Coherence State SRAM} if needed. In case of response from the network (after a GetS request, for instance), the \texttt{Protocol Handler} updates the L1 cache tag, the privileges, and the data, issuing an update request on its \texttt{High Priority Update} bus. An update from \texttt{Cache Controller} has the highest priority in the \texttt{Load Store Unit Arbiter}. Furthermore, this module handles block replacement and issues coherence requests.
	\item \texttt{Network Message Composer}: This module translates the coherence request from the \texttt{Protocol Handler} in a network request, and issues it on the right virtual network.  
	\item \texttt{Network Interface}: it handles network messages, it has three virtual networks in order to avoid deadlocks.
\end{enumerate}

The \texttt{Protocol Handler} is the main module of our \texttt{Cache Controller}, it reads coherence state and the protocol table to generate the next action. The \texttt{Protocol ROM} indicates the next step to take in the coherence protocol based on the current state and the scheduled request. The \texttt{Coherence State SRAM} holds a stable state for each cache line, while transient states are kept in MSHR with pending requests.

\paragraph{Assumptions:}
\begin{enumerate}
	\item The \texttt{Fetch Unit} does not check if there is room for a new MSHR entry when a request is scheduled. We suppose that a new request has always room for a new MSHR entry. Our architecture blocks a thread that issues a miss memory request, hence from the core side we can receive a number of requests equal to the number of threads (one request per thread), and only a core request may allocate a new MSHR entry. We keep the number of MSHR entries equal to the number of threads.
	\item When a request is issued (both from NI or CI), it can modify an MSHR entry after two clock cycles, hence the next request may read a non up to date entry. In order to avoid this, the \texttt{Fetch Unit} does not schedule two consecutive requests on the same address. This is done with a scoreboard-like mechanism. 
	\item MSHR structure is a read-first memory, it may cause issues if the \texttt{Protocol Unit} updates an entry and a request reads that entry in the same cycle. In order not to read an non up to date MSHR entry, when \texttt{Protocol Unit} is updating a specific MSHR entry, \texttt{Fetch Unit} does not schedules request on that entry.
\end{enumerate}

\paragraph{Replacement and Evict}
We are implementing the first strategy, but we have a backup solution in case of the first one may cause deadlock. After a response, the \texttt{Protocol Handler} preallocates the MSHR entry for the replacement and forces the \texttt{Load Store Unit} to evict the old data in the Evict Buffer. The \texttt{Protocol Unit} fills this entry with the address to evict and sets the \texttt{waiting\_for\_eviction} bit. All requests to this address are stalled. When \texttt{Fetch Unit} schedules the replacement request from Evict Buffer, the remaining information (such as data and state) are filled in MSHR and the \texttt{Protocol Handler} can issue the PUT request.

\subsection{Directory Controller}
\begin{wrapfigure}{L}{0.8\textwidth}
	\centering
	\includegraphics[width=0.8\textwidth]{Images/Directory_Controller.eps}
\end{wrapfigure}

\clearpage
\section{Instruction Set Architecture} \label{section:ISA}
\nuplus uses a custom MIPS-like load/store instruction set. It has a scalar register file and a vector register file, both are fully configurable. Scalar instructions use scalar registers, vector instructions use vector registers. 

The \nuplus core has 58 general purpose 32-bit scalar registers (\texttt{\$s0-\$s57}), which can be used also to store up to 29 64-bit wide data, and 64 512-bit vector registers (\texttt{\$v0-\$v57}), each of which can store a vector of 16 32-bit elements or 8 64-bit elements. 
There are also six special registers. 

\begin{table}[h]
	\small
	\centering
	\label{tab:mean}
	
	\begin{tabular}{c c l }
		\toprule
		ID & Number & Name            \\ \midrule
		TR & 58     & Trap Register   \\
		RM & 59     & Mask Register   \\
		FP & 60     & Frame Pointer   \\
		SP & 61     & Stack Pointer   \\
		RA & 62     & Return Address  \\
		PC & 63     & Program Counter \\ \bottomrule
	\end{tabular}
\end{table}

These are \texttt{\$tr} register used as trap register, \texttt{\$rm} used to store the mask for operations with vectors, \texttt{\$fp} that is the frame pointer, \texttt{\$sp} that is the stack pointer, \texttt{\$ra} used to store the return address and \texttt{\$pc} that is the program counter.

The \nuplus instructions have a fixed length of 32 bits. The eight most significant bits are used to encode the instruction type and the operation. The \nuplus ISA provides eight types of instructions, shown in figure \ref{figura:nuplusisa}.


\begin{figure}[h]
	\centering\includegraphics[width=\textwidth]{./Images/ISA.eps}
	\caption{\nuplus ISA}
	\label{figura:nuplusisa}
\end{figure}

The \textit{R type} includes logical and arithmetic operations, and also memory operations with based indexed addressing, that is the effective address is calculated by the sum of a base and of an index, both contained in two registers. The R type instructions have three register operands, referred to as \texttt{R-DST}, \texttt{R-SRC0} and \texttt{R-SRC1}. It also has a 3-bit \texttt{fmt} field, an \texttt{M} bit and an \texttt{L} bit.

The \textit{I type} includes the logical and arithmetic operations between a register operand and an immediate operand. The I type instructions have two register operands, referred to as \texttt{R-DST} and \texttt{R-SRC0}, and a 9-bit immediate field \texttt{imm}. It also has a 2-bit \texttt{fmt} field and an \texttt{M} bit.

The \textit{MOVEI type} includes the load operations of an immediate operand in a register. The MOVEI type instructions have the destination register \texttt{R-DST} and a 16-bit immediate field \texttt{imm}. It also has a 1-bit \texttt{fmt} field and an \texttt{M} bit.

The \textit{C type} is used for control operations, like those bound with coherence protocol, and for synchronisation instructions.

The \textit{JR type} includes jump instructions with the based addressing mode. The JR type instructions have a register containing the base \texttt{R-COND/DST} and a 18-bit immediate field \texttt{imm}.

The \textit{J type} includes jump instructions with the relative addressing mode. The J type instructions have only a 24-bit immediate field \texttt{imm}.

The \textit{M type} includes the instructions used to access memory, through the based addressing mode. The M type instructions have two registers, the destination or source register \texttt{R-DST/SRC} and the base register \texttt{R-BASE} and a 9-bit immediate field \texttt{offset}. It also has an \texttt{S} bit, an \texttt{M} bit and an \texttt{L} bit.

The \textit{M-poly type} is used for memory instructions which uses a polyhedral access pattern.

The \textit{NOP type} includes the no-operation instruction.

Note that, at this moment, C and M-poly instructions are not implemented yet.


The \texttt{fmt} field has a bit for each register in the instruction and specifies if a register is scalar or vectorial. The \texttt{M} bit is used to specify if an instruction uses the mask register, to perform a masked operation. The \texttt{L} bit is used to distinguish between 32-bit and 64-bit data. The \texttt{S} bit determines if the scratchpad must be used or not.

\end{document} 